#include "../gen_conv.h"
#include "../reorder_avx2.h"

void gen_conv(M_TYPE * const  __restrict__ output,
M_TYPE const * const __restrict__ input, M_TYPE const * const __restrict__ params,
    IND_TYPE X, IND_TYPE W,
    IND_TYPE Y, IND_TYPE H,
	IND_TYPE C, IND_TYPE F) {
/*
[V f; U (4, f); ULambda y; T (256, c); Hoist_vars [c]; T (17, x);
  Lambda_apply y [((Iter 3), (Arg 4)); ((Iter 1), (Arg 5))]; T (16, f);
  T (4, c); T (1, x); T (1, y)]
*/
IND_TYPE c, cp_0, c12_p_0, cp_1, c12, f, fp_0, x, xp_0, x12_p_0, xp_1, x12, y, yp_0, y12_p_0, yp_1, y12;

assert((Y == 17));
assert((X == 17));
assert((H == 1));
assert((W == 1));
assert((C == 1024));
assert((F == 512));
IND_TYPE y13 = 0;
IND_TYPE x13 = 0;
IND_TYPE h = 0;
IND_TYPE w = 0;
IND_TYPE c13 = 0;
IND_TYPE f8 = 0;
float scal_0 ,scal_1 ,scal_2 ,scal_3 ,scal_4;
__m256 mem_vec_144 ,mem_vec_145 ,mem_vec_146 ,mem_vec_147 ,mem_vec_148 ,mem_vec_149 ,mem_vec_150 ,mem_vec_151 ,mem_vec_152 ,mem_vec_153 ,mem_vec_154 ,mem_vec_155 ,mem_vec_156 ,mem_vec_157 ,mem_vec_158 ,mem_vec_159 ,mem_vec_160 ,mem_vec_161 ,mem_vec_162 ,mem_vec_163 ,mem_vec_164 ,mem_vec_165 ,mem_vec_166 ,mem_vec_167 ,mem_vec_168 ,mem_vec_169 ,mem_vec_170 ,mem_vec_171 ,mem_vec_172 ,mem_vec_173 ,mem_vec_174 ,mem_vec_175 ,mem_vec_176 ,mem_vec_177 ,mem_vec_178 ,mem_vec_179 ,vec_0 ,vec_1 ,vec_10 ,vec_11 ,vec_12 ,vec_13 ,vec_14 ,vec_15 ,vec_16 ,vec_17 ,vec_18 ,vec_19 ,vec_2 ,vec_20 ,vec_21 ,vec_22 ,vec_23 ,vec_24 ,vec_25 ,vec_26 ,vec_27 ,vec_28 ,vec_3 ,vec_4 ,vec_5 ,vec_6 ,vec_7 ,vec_8 ,vec_9;
// y = 17, x = 17, h = 1, w = 1, c = 1024, f = 512
// T (y, 1) (17 / 17)
for (y12 = y13, y12_p_0 = 0;
	y12 < y13 + 17;
	y12 += 17, y12_p_0 += 17){
	// y = 17, x = 17, h = 1, w = 1, c = 1024, f = 512
	// T (x, 1) (17 / 17)
	for (x12 = x13, x12_p_0 = 0;
		x12 < x13 + 17;
		x12 += 17, x12_p_0 += 17){
		// y = 17, x = 17, h = 1, w = 1, c = 1024, f = 512
		// T (c, 4) (1024 / 256)
		for (c12 = c13, c12_p_0 = 0;
			c12 < c13 + 1024;
			c12 += 256, c12_p_0 += 256){
			// y = 17, x = 17, h = 1, w = 1, c = 256, f = 512
			// T (f, 16) (512 / 32)
			for (f = f8, fp_0 = 0;
				f < f8 + 512;
				f += 32, fp_0 += 32){
					for (y = y12, yp_1 = y12_p_0, yp_0 = 0;
						y < y12 + 12;
						y += 4, yp_1 += 4, yp_0 += 4){
						// y = ph_y, x = 17, h = 1, w = 1, c = 256, f = 32
						// T (x, 17) (17 / 1)
						for (x = x12, xp_1 = x12_p_0, xp_0 = 0;
							x < x12 + 17;
							x += 1, xp_1 += 1, xp_0 += 1){
									mem_vec_144 = _mm256_loadu_ps(&output[(F * Y) * x + F * y + f]);
									mem_vec_145 = _mm256_loadu_ps(&output[(F * Y) * x + F * y + f + 8]);
									mem_vec_146 = _mm256_loadu_ps(&output[(F * Y) * x + F * y + f + 16]);
									mem_vec_147 = _mm256_loadu_ps(&output[(F * Y) * x + F * y + f + 24]);
									mem_vec_148 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
									mem_vec_149 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 8]);
									mem_vec_150 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16]);
									mem_vec_151 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 24]);
									mem_vec_152 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
									mem_vec_153 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 8]);
									mem_vec_154 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16]);
									mem_vec_155 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 24]);
									mem_vec_156 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
									mem_vec_157 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 8]);
									mem_vec_158 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16]);
									mem_vec_159 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 24]);
									// y = ph_y, x = 1, h = 1, w = 1, c = 256, f = 32
									// T (c, 256) (256 / 1)
									for (c = c12, cp_1 = c12_p_0, cp_0 = 0;
										c < c12 + 256;
										c += 1, cp_1 += 1, cp_0 += 1){
										scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
										vec_1 = _mm256_set1_ps(scal_0);
										vec_2 = _mm256_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

										vec_0 = _mm256_fmadd_ps(vec_1, vec_2, mem_vec_144);
										mem_vec_144 = vec_0;

										vec_4 = _mm256_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 8]);

										vec_3 = _mm256_fmadd_ps(vec_1, vec_4, mem_vec_145);
										mem_vec_145 = vec_3;

										vec_6 = _mm256_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 16]);

										vec_5 = _mm256_fmadd_ps(vec_1, vec_6, mem_vec_146);
										mem_vec_146 = vec_5;

										vec_8 = _mm256_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 24]);

										vec_7 = _mm256_fmadd_ps(vec_1, vec_8, mem_vec_147);
										mem_vec_147 = vec_7;
										scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
										vec_10 = _mm256_set1_ps(scal_1);


										vec_9 = _mm256_fmadd_ps(vec_10, vec_2, mem_vec_148);
										mem_vec_148 = vec_9;



										vec_11 = _mm256_fmadd_ps(vec_10, vec_4, mem_vec_149);
										mem_vec_149 = vec_11;



										vec_12 = _mm256_fmadd_ps(vec_10, vec_6, mem_vec_150);
										mem_vec_150 = vec_12;



										vec_13 = _mm256_fmadd_ps(vec_10, vec_8, mem_vec_151);
										mem_vec_151 = vec_13;
										scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
										vec_15 = _mm256_set1_ps(scal_2);


										vec_14 = _mm256_fmadd_ps(vec_15, vec_2, mem_vec_152);
										mem_vec_152 = vec_14;



										vec_16 = _mm256_fmadd_ps(vec_15, vec_4, mem_vec_153);
										mem_vec_153 = vec_16;



										vec_17 = _mm256_fmadd_ps(vec_15, vec_6, mem_vec_154);
										mem_vec_154 = vec_17;



										vec_18 = _mm256_fmadd_ps(vec_15, vec_8, mem_vec_155);
										mem_vec_155 = vec_18;
										scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
										vec_20 = _mm256_set1_ps(scal_3);


										vec_19 = _mm256_fmadd_ps(vec_20, vec_2, mem_vec_156);
										mem_vec_156 = vec_19;



										vec_21 = _mm256_fmadd_ps(vec_20, vec_4, mem_vec_157);
										mem_vec_157 = vec_21;



										vec_22 = _mm256_fmadd_ps(vec_20, vec_6, mem_vec_158);
										mem_vec_158 = vec_22;



										vec_23 = _mm256_fmadd_ps(vec_20, vec_8, mem_vec_159);
										mem_vec_159 = vec_23;
									}
								_mm256_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_144);
								_mm256_storeu_ps(&output[(F * Y) * x + F * y + f + 8], mem_vec_145);
								_mm256_storeu_ps(&output[(F * Y) * x + F * y + f + 16], mem_vec_146);
								_mm256_storeu_ps(&output[(F * Y) * x + F * y + f + 24], mem_vec_147);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_148);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 8], mem_vec_149);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16], mem_vec_150);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 24], mem_vec_151);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_152);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 8], mem_vec_153);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16], mem_vec_154);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 24], mem_vec_155);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_156);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 8], mem_vec_157);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16], mem_vec_158);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 24], mem_vec_159);
						}
					}
					for (y = y12 + 12, yp_1 = y12_p_0, yp_0 = 0;
						y < y12 + 12 + 5;
						y += 5, yp_1 += 5, yp_0 += 5){
						// y = ph_y, x = 17, h = 1, w = 1, c = 256, f = 32
						// T (x, 17) (17 / 1)
						for (x = x12, xp_1 = x12_p_0, xp_0 = 0;
							x < x12 + 17;
							x += 1, xp_1 += 1, xp_0 += 1){
									mem_vec_160 = _mm256_loadu_ps(&output[(F * Y) * x + F * y + f]);
									mem_vec_161 = _mm256_loadu_ps(&output[(F * Y) * x + F * y + f + 8]);
									mem_vec_162 = _mm256_loadu_ps(&output[(F * Y) * x + F * y + f + 16]);
									mem_vec_163 = _mm256_loadu_ps(&output[(F * Y) * x + F * y + f + 24]);
									mem_vec_164 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
									mem_vec_165 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 8]);
									mem_vec_166 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16]);
									mem_vec_167 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 24]);
									mem_vec_168 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
									mem_vec_169 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 8]);
									mem_vec_170 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16]);
									mem_vec_171 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 24]);
									mem_vec_172 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
									mem_vec_173 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 8]);
									mem_vec_174 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16]);
									mem_vec_175 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 24]);
									mem_vec_176 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f]);
									mem_vec_177 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 8]);
									mem_vec_178 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16]);
									mem_vec_179 = _mm256_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 24]);
									// y = ph_y, x = 1, h = 1, w = 1, c = 256, f = 32
									// T (c, 256) (256 / 1)
									for (c = c12, cp_1 = c12_p_0, cp_0 = 0;
										c < c12 + 256;
										c += 1, cp_1 += 1, cp_0 += 1){
										scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
										vec_1 = _mm256_set1_ps(scal_0);
										vec_2 = _mm256_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

										vec_0 = _mm256_fmadd_ps(vec_1, vec_2, mem_vec_160);
										mem_vec_160 = vec_0;

										vec_4 = _mm256_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 8]);

										vec_3 = _mm256_fmadd_ps(vec_1, vec_4, mem_vec_161);
										mem_vec_161 = vec_3;

										vec_6 = _mm256_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 16]);

										vec_5 = _mm256_fmadd_ps(vec_1, vec_6, mem_vec_162);
										mem_vec_162 = vec_5;

										vec_8 = _mm256_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 24]);

										vec_7 = _mm256_fmadd_ps(vec_1, vec_8, mem_vec_163);
										mem_vec_163 = vec_7;
										scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
										vec_10 = _mm256_set1_ps(scal_1);


										vec_9 = _mm256_fmadd_ps(vec_10, vec_2, mem_vec_164);
										mem_vec_164 = vec_9;



										vec_11 = _mm256_fmadd_ps(vec_10, vec_4, mem_vec_165);
										mem_vec_165 = vec_11;



										vec_12 = _mm256_fmadd_ps(vec_10, vec_6, mem_vec_166);
										mem_vec_166 = vec_12;



										vec_13 = _mm256_fmadd_ps(vec_10, vec_8, mem_vec_167);
										mem_vec_167 = vec_13;
										scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
										vec_15 = _mm256_set1_ps(scal_2);


										vec_14 = _mm256_fmadd_ps(vec_15, vec_2, mem_vec_168);
										mem_vec_168 = vec_14;



										vec_16 = _mm256_fmadd_ps(vec_15, vec_4, mem_vec_169);
										mem_vec_169 = vec_16;



										vec_17 = _mm256_fmadd_ps(vec_15, vec_6, mem_vec_170);
										mem_vec_170 = vec_17;



										vec_18 = _mm256_fmadd_ps(vec_15, vec_8, mem_vec_171);
										mem_vec_171 = vec_18;
										scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
										vec_20 = _mm256_set1_ps(scal_3);


										vec_19 = _mm256_fmadd_ps(vec_20, vec_2, mem_vec_172);
										mem_vec_172 = vec_19;



										vec_21 = _mm256_fmadd_ps(vec_20, vec_4, mem_vec_173);
										mem_vec_173 = vec_21;



										vec_22 = _mm256_fmadd_ps(vec_20, vec_6, mem_vec_174);
										mem_vec_174 = vec_22;



										vec_23 = _mm256_fmadd_ps(vec_20, vec_8, mem_vec_175);
										mem_vec_175 = vec_23;
										scal_4 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h) + c];
										vec_25 = _mm256_set1_ps(scal_4);


										vec_24 = _mm256_fmadd_ps(vec_25, vec_2, mem_vec_176);
										mem_vec_176 = vec_24;



										vec_26 = _mm256_fmadd_ps(vec_25, vec_4, mem_vec_177);
										mem_vec_177 = vec_26;



										vec_27 = _mm256_fmadd_ps(vec_25, vec_6, mem_vec_178);
										mem_vec_178 = vec_27;



										vec_28 = _mm256_fmadd_ps(vec_25, vec_8, mem_vec_179);
										mem_vec_179 = vec_28;
									}
								_mm256_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_160);
								_mm256_storeu_ps(&output[(F * Y) * x + F * y + f + 8], mem_vec_161);
								_mm256_storeu_ps(&output[(F * Y) * x + F * y + f + 16], mem_vec_162);
								_mm256_storeu_ps(&output[(F * Y) * x + F * y + f + 24], mem_vec_163);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_164);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 8], mem_vec_165);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16], mem_vec_166);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 24], mem_vec_167);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_168);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 8], mem_vec_169);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16], mem_vec_170);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 24], mem_vec_171);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_172);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 8], mem_vec_173);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16], mem_vec_174);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 24], mem_vec_175);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f], mem_vec_176);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 8], mem_vec_177);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16], mem_vec_178);
								_mm256_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 24], mem_vec_179);
						}
					}
			}
		}
	}
}


}