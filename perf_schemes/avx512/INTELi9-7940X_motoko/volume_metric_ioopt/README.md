metric :

on selectionne ratio % des meilleurs sur la taille de la réduction et le volume,
(ou si pas assez de candidats, min_threshold)
on intersecte et on prend les max_threshold meilleurs sur le volume
```ocaml
let select_mixed min_threshold max_threshold ratio implems =    
     let n_candidates = List.length implems in    
     let num_select = Int.min n_candidates                            
         (Int.max min_threshold (ratio * n_candidates / 100)) in                            
     let implemsi = List.mapi (fun i c -> i, c) implems in    
     let best_volumes = Utils.sort_by (fun (_, c) -> data_volume_weighted c)    
         implemsi |> List.take num_select    
     and best_red = Utils.sort_by (fun (_, c) -> Int.neg @@ red_size C c) implemsi    
                  |> List.take num_select in    
     let common_indexes =    
       let best_red_idxs = List.map fst best_red    
       and best_volumes_idxs = List.map fst best_volumes in    
       List.filter (fun idx -> List.exists ((=) idx) best_red_idxs)    
         best_volumes_idxs in    
     List.map (List.at implems) common_indexes ❯ Unbound value List.at    
     |> List.take max_threshold                  
```
