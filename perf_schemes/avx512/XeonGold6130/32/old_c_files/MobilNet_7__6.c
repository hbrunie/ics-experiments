#include "../gen_conv.h"
#include "../reorder_avx2.h"

void gen_conv(M_TYPE * const  __restrict__ output,
M_TYPE const * const __restrict__ input, M_TYPE const * const __restrict__ params,
    IND_TYPE X, IND_TYPE W,
    IND_TYPE Y, IND_TYPE H,
	IND_TYPE C, IND_TYPE F) {
/*
[V f; U (4, f); U (7, y); T (32, c); Hoist_vars [c]; T (3, w); T (3, h);
  T (14, x); T (4, c); T (2, y); T (8, f); T (4, c)]
*/
IND_TYPE c, cp_0, c112_p_0, c113_p_0, cp_1, c112_p_1, cp_2, c112, c113, f, fp_0, h, hp_0, w, wp_0, x, xp_0, y, yp_0;

assert((Y == 14));
assert((X == 14));
assert((H == 3));
assert((W == 3));
assert((C == 512));
assert((F == 512));
IND_TYPE y56 = 0;
IND_TYPE x56 = 0;
IND_TYPE h52 = 0;
IND_TYPE w52 = 0;
IND_TYPE c114 = 0;
IND_TYPE f56 = 0;
float scal_0 ,scal_1 ,scal_2 ,scal_3 ,scal_4 ,scal_5 ,scal_6;
__m512 mem_vec_784 ,mem_vec_785 ,mem_vec_786 ,mem_vec_787 ,mem_vec_788 ,mem_vec_789 ,mem_vec_790 ,mem_vec_791 ,mem_vec_792 ,mem_vec_793 ,mem_vec_794 ,mem_vec_795 ,mem_vec_796 ,mem_vec_797 ,mem_vec_798 ,mem_vec_799 ,mem_vec_800 ,mem_vec_801 ,mem_vec_802 ,mem_vec_803 ,mem_vec_804 ,mem_vec_805 ,mem_vec_806 ,mem_vec_807 ,mem_vec_808 ,mem_vec_809 ,mem_vec_810 ,mem_vec_811 ,vec_0 ,vec_1 ,vec_10 ,vec_11 ,vec_12 ,vec_13 ,vec_14 ,vec_15 ,vec_16 ,vec_17 ,vec_18 ,vec_19 ,vec_2 ,vec_20 ,vec_21 ,vec_22 ,vec_23 ,vec_24 ,vec_25 ,vec_26 ,vec_27 ,vec_28 ,vec_29 ,vec_3 ,vec_30 ,vec_31 ,vec_32 ,vec_33 ,vec_34 ,vec_35 ,vec_36 ,vec_37 ,vec_38 ,vec_4 ,vec_5 ,vec_6 ,vec_7 ,vec_8 ,vec_9;
// y = 14, x = 14, h = 3, w = 3, c = 512, f = 512
// T (c, 4) (512 / 128)
for (c113 = c114, c113_p_0 = 0;
	c113 < c114 + 512;
	c113 += 128, c113_p_0 += 128){
	// y = 14, x = 14, h = 3, w = 3, c = 128, f = 512
	// T (f, 8) (512 / 64)
	for (f = f56, fp_0 = 0;
		f < f56 + 512;
		f += 64, fp_0 += 64){
		// y = 14, x = 14, h = 3, w = 3, c = 128, f = 64
		// T (y, 2) (14 / 7)
		for (y = y56, yp_0 = 0;
			y < y56 + 14;
			y += 7, yp_0 += 7){
			// y = 7, x = 14, h = 3, w = 3, c = 128, f = 64
			// T (c, 4) (128 / 32)
			for (c112 = c113, c112_p_1 = c113_p_0, c112_p_0 = 0;
				c112 < c113 + 128;
				c112 += 32, c112_p_1 += 32, c112_p_0 += 32){
				// y = 7, x = 14, h = 3, w = 3, c = 32, f = 64
				// T (x, 14) (14 / 1)
				for (x = x56, xp_0 = 0;
					x < x56 + 14;
					x += 1, xp_0 += 1){
					// y = 7, x = 1, h = 3, w = 3, c = 32, f = 64
					// T (h, 3) (3 / 1)
					for (h = h52, hp_0 = 0;
						h < h52 + 3;
						h += 1, hp_0 += 1){
						// y = 7, x = 1, h = 1, w = 3, c = 32, f = 64
						// T (w, 3) (3 / 1)
						for (w = w52, wp_0 = 0;
							w < w52 + 3;
							w += 1, wp_0 += 1){
									mem_vec_784 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f]);
									mem_vec_785 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 16]);
									mem_vec_786 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 32]);
									mem_vec_787 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 48]);
									mem_vec_788 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
									mem_vec_789 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16]);
									mem_vec_790 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 32]);
									mem_vec_791 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 48]);
									mem_vec_792 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
									mem_vec_793 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16]);
									mem_vec_794 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 32]);
									mem_vec_795 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 48]);
									mem_vec_796 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
									mem_vec_797 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16]);
									mem_vec_798 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 32]);
									mem_vec_799 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 48]);
									mem_vec_800 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f]);
									mem_vec_801 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16]);
									mem_vec_802 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 32]);
									mem_vec_803 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 48]);
									mem_vec_804 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f]);
									mem_vec_805 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16]);
									mem_vec_806 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f + 32]);
									mem_vec_807 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f + 48]);
									mem_vec_808 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f]);
									mem_vec_809 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16]);
									mem_vec_810 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f + 32]);
									mem_vec_811 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f + 48]);
									// y = 7, x = 1, h = 1, w = 1, c = 32, f = 64
									// T (c, 32) (32 / 1)
									for (c = c112, cp_2 = c112_p_1, cp_1 = c112_p_0, cp_0 = 0;
										c < c112 + 32;
										c += 1, cp_2 += 1, cp_1 += 1, cp_0 += 1){
										scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
										vec_1 = _mm512_set1_ps(scal_0);
										vec_2 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

										vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_784);
										mem_vec_784 = vec_0;

										vec_4 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 16]);

										vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_785);
										mem_vec_785 = vec_3;

										vec_6 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 32]);

										vec_5 = _mm512_fmadd_ps(vec_1, vec_6, mem_vec_786);
										mem_vec_786 = vec_5;

										vec_8 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 48]);

										vec_7 = _mm512_fmadd_ps(vec_1, vec_8, mem_vec_787);
										mem_vec_787 = vec_7;
										scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
										vec_10 = _mm512_set1_ps(scal_1);


										vec_9 = _mm512_fmadd_ps(vec_10, vec_2, mem_vec_788);
										mem_vec_788 = vec_9;



										vec_11 = _mm512_fmadd_ps(vec_10, vec_4, mem_vec_789);
										mem_vec_789 = vec_11;



										vec_12 = _mm512_fmadd_ps(vec_10, vec_6, mem_vec_790);
										mem_vec_790 = vec_12;



										vec_13 = _mm512_fmadd_ps(vec_10, vec_8, mem_vec_791);
										mem_vec_791 = vec_13;
										scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
										vec_15 = _mm512_set1_ps(scal_2);


										vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_792);
										mem_vec_792 = vec_14;



										vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_793);
										mem_vec_793 = vec_16;



										vec_17 = _mm512_fmadd_ps(vec_15, vec_6, mem_vec_794);
										mem_vec_794 = vec_17;



										vec_18 = _mm512_fmadd_ps(vec_15, vec_8, mem_vec_795);
										mem_vec_795 = vec_18;
										scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
										vec_20 = _mm512_set1_ps(scal_3);


										vec_19 = _mm512_fmadd_ps(vec_20, vec_2, mem_vec_796);
										mem_vec_796 = vec_19;



										vec_21 = _mm512_fmadd_ps(vec_20, vec_4, mem_vec_797);
										mem_vec_797 = vec_21;



										vec_22 = _mm512_fmadd_ps(vec_20, vec_6, mem_vec_798);
										mem_vec_798 = vec_22;



										vec_23 = _mm512_fmadd_ps(vec_20, vec_8, mem_vec_799);
										mem_vec_799 = vec_23;
										scal_4 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h) + c];
										vec_25 = _mm512_set1_ps(scal_4);


										vec_24 = _mm512_fmadd_ps(vec_25, vec_2, mem_vec_800);
										mem_vec_800 = vec_24;



										vec_26 = _mm512_fmadd_ps(vec_25, vec_4, mem_vec_801);
										mem_vec_801 = vec_26;



										vec_27 = _mm512_fmadd_ps(vec_25, vec_6, mem_vec_802);
										mem_vec_802 = vec_27;



										vec_28 = _mm512_fmadd_ps(vec_25, vec_8, mem_vec_803);
										mem_vec_803 = vec_28;
										scal_5 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h) + c];
										vec_30 = _mm512_set1_ps(scal_5);


										vec_29 = _mm512_fmadd_ps(vec_30, vec_2, mem_vec_804);
										mem_vec_804 = vec_29;



										vec_31 = _mm512_fmadd_ps(vec_30, vec_4, mem_vec_805);
										mem_vec_805 = vec_31;



										vec_32 = _mm512_fmadd_ps(vec_30, vec_6, mem_vec_806);
										mem_vec_806 = vec_32;



										vec_33 = _mm512_fmadd_ps(vec_30, vec_8, mem_vec_807);
										mem_vec_807 = vec_33;
										scal_6 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h) + c];
										vec_35 = _mm512_set1_ps(scal_6);


										vec_34 = _mm512_fmadd_ps(vec_35, vec_2, mem_vec_808);
										mem_vec_808 = vec_34;



										vec_36 = _mm512_fmadd_ps(vec_35, vec_4, mem_vec_809);
										mem_vec_809 = vec_36;



										vec_37 = _mm512_fmadd_ps(vec_35, vec_6, mem_vec_810);
										mem_vec_810 = vec_37;



										vec_38 = _mm512_fmadd_ps(vec_35, vec_8, mem_vec_811);
										mem_vec_811 = vec_38;
									}
								_mm512_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_784);
								_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 16], mem_vec_785);
								_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 32], mem_vec_786);
								_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 48], mem_vec_787);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_788);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16], mem_vec_789);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 32], mem_vec_790);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 48], mem_vec_791);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_792);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16], mem_vec_793);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 32], mem_vec_794);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 48], mem_vec_795);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_796);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16], mem_vec_797);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 32], mem_vec_798);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 48], mem_vec_799);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f], mem_vec_800);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16], mem_vec_801);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 32], mem_vec_802);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 48], mem_vec_803);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f], mem_vec_804);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16], mem_vec_805);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f + 32], mem_vec_806);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f + 48], mem_vec_807);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f], mem_vec_808);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16], mem_vec_809);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f + 32], mem_vec_810);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f + 48], mem_vec_811);
						}
					}
				}
			}
		}
	}
}


}