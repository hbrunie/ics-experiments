#include "../gen_conv.h"
#include "../reorder_avx2.h"

void gen_conv(M_TYPE * const  __restrict__ output,
M_TYPE const * const __restrict__ input, M_TYPE const * const __restrict__ params,
    IND_TYPE X, IND_TYPE W,
    IND_TYPE Y, IND_TYPE H,
	IND_TYPE C, IND_TYPE F) {
/*
[V f; U (2, f); ULambda y; T (32, c); Hoist_vars [c]; T (17, x); T (32, c);
  Lambda_apply y [((Iter 1), (Arg 8)); ((Iter 1), (Arg 9))]; T (16, f);
  T (1, y)]
*/
IND_TYPE c, cp_0, c6_p_0, cp_1, c6, f, fp_0, x, xp_0, y, yp_0, y6_p_0, yp_1, y6;

assert((Y == 17));
assert((X == 17));
assert((H == 1));
assert((W == 1));
assert((C == 1024));
assert((F == 512));
IND_TYPE y7 = 0;
IND_TYPE x4 = 0;
IND_TYPE h = 0;
IND_TYPE w = 0;
IND_TYPE c7 = 0;
IND_TYPE f4 = 0;
float scal_0 ,scal_1 ,scal_2 ,scal_3 ,scal_4 ,scal_5 ,scal_6 ,scal_7 ,scal_8;
__m512 mem_vec_100 ,mem_vec_101 ,mem_vec_68 ,mem_vec_69 ,mem_vec_70 ,mem_vec_71 ,mem_vec_72 ,mem_vec_73 ,mem_vec_74 ,mem_vec_75 ,mem_vec_76 ,mem_vec_77 ,mem_vec_78 ,mem_vec_79 ,mem_vec_80 ,mem_vec_81 ,mem_vec_82 ,mem_vec_83 ,mem_vec_84 ,mem_vec_85 ,mem_vec_86 ,mem_vec_87 ,mem_vec_88 ,mem_vec_89 ,mem_vec_90 ,mem_vec_91 ,mem_vec_92 ,mem_vec_93 ,mem_vec_94 ,mem_vec_95 ,mem_vec_96 ,mem_vec_97 ,mem_vec_98 ,mem_vec_99 ,vec_0 ,vec_1 ,vec_10 ,vec_11 ,vec_12 ,vec_13 ,vec_14 ,vec_15 ,vec_16 ,vec_17 ,vec_18 ,vec_19 ,vec_2 ,vec_20 ,vec_21 ,vec_22 ,vec_23 ,vec_24 ,vec_25 ,vec_26 ,vec_27 ,vec_28 ,vec_3 ,vec_4 ,vec_5 ,vec_6 ,vec_7 ,vec_8 ,vec_9;
// y = 17, x = 17, h = 1, w = 1, c = 1024, f = 512
// T (y, 1) (17 / 17)
for (y6 = y7, y6_p_0 = 0;
	y6 < y7 + 17;
	y6 += 17, y6_p_0 += 17){
	// y = 17, x = 17, h = 1, w = 1, c = 1024, f = 512
	// T (f, 16) (512 / 32)
	for (f = f4, fp_0 = 0;
		f < f4 + 512;
		f += 32, fp_0 += 32){
			for (y = y6, yp_1 = y6_p_0, yp_0 = 0;
				y < y6 + 8;
				y += 8, yp_1 += 8, yp_0 += 8){
				// y = ph_y, x = 17, h = 1, w = 1, c = 1024, f = 32
				// T (c, 32) (1024 / 32)
				for (c6 = c7, c6_p_0 = 0;
					c6 < c7 + 1024;
					c6 += 32, c6_p_0 += 32){
					// y = ph_y, x = 17, h = 1, w = 1, c = 32, f = 32
					// T (x, 17) (17 / 1)
					for (x = x4, xp_0 = 0;
						x < x4 + 17;
						x += 1, xp_0 += 1){
								mem_vec_68 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f]);
								mem_vec_69 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 16]);
								mem_vec_70 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
								mem_vec_71 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16]);
								mem_vec_72 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
								mem_vec_73 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16]);
								mem_vec_74 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
								mem_vec_75 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16]);
								mem_vec_76 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f]);
								mem_vec_77 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16]);
								mem_vec_78 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f]);
								mem_vec_79 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16]);
								mem_vec_80 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f]);
								mem_vec_81 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16]);
								mem_vec_82 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 7) + f]);
								mem_vec_83 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 7) + f + 16]);
								// y = ph_y, x = 1, h = 1, w = 1, c = 32, f = 32
								// T (c, 32) (32 / 1)
								for (c = c6, cp_1 = c6_p_0, cp_0 = 0;
									c < c6 + 32;
									c += 1, cp_1 += 1, cp_0 += 1){
									scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
									vec_1 = _mm512_set1_ps(scal_0);
									vec_2 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

									vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_68);
									mem_vec_68 = vec_0;

									vec_4 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 16]);

									vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_69);
									mem_vec_69 = vec_3;
									scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
									vec_6 = _mm512_set1_ps(scal_1);


									vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_70);
									mem_vec_70 = vec_5;



									vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_71);
									mem_vec_71 = vec_7;
									scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
									vec_9 = _mm512_set1_ps(scal_2);


									vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_72);
									mem_vec_72 = vec_8;



									vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_73);
									mem_vec_73 = vec_10;
									scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
									vec_12 = _mm512_set1_ps(scal_3);


									vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_74);
									mem_vec_74 = vec_11;



									vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_75);
									mem_vec_75 = vec_13;
									scal_4 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h) + c];
									vec_15 = _mm512_set1_ps(scal_4);


									vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_76);
									mem_vec_76 = vec_14;



									vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_77);
									mem_vec_77 = vec_16;
									scal_5 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h) + c];
									vec_18 = _mm512_set1_ps(scal_5);


									vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_78);
									mem_vec_78 = vec_17;



									vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_79);
									mem_vec_79 = vec_19;
									scal_6 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h) + c];
									vec_21 = _mm512_set1_ps(scal_6);


									vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_80);
									mem_vec_80 = vec_20;



									vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_81);
									mem_vec_81 = vec_22;
									scal_7 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h) + c];
									vec_24 = _mm512_set1_ps(scal_7);


									vec_23 = _mm512_fmadd_ps(vec_24, vec_2, mem_vec_82);
									mem_vec_82 = vec_23;



									vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_83);
									mem_vec_83 = vec_25;
								}
							_mm512_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_68);
							_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 16], mem_vec_69);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_70);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16], mem_vec_71);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_72);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16], mem_vec_73);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_74);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16], mem_vec_75);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f], mem_vec_76);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16], mem_vec_77);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f], mem_vec_78);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16], mem_vec_79);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f], mem_vec_80);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16], mem_vec_81);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 7) + f], mem_vec_82);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 7) + f + 16], mem_vec_83);
					}
				}
			}
			for (y = y6 + 8, yp_1 = y6_p_0, yp_0 = 0;
				y < y6 + 8 + 9;
				y += 9, yp_1 += 9, yp_0 += 9){
				// y = ph_y, x = 17, h = 1, w = 1, c = 1024, f = 32
				// T (c, 32) (1024 / 32)
				for (c6 = c7, c6_p_0 = 0;
					c6 < c7 + 1024;
					c6 += 32, c6_p_0 += 32){
					// y = ph_y, x = 17, h = 1, w = 1, c = 32, f = 32
					// T (x, 17) (17 / 1)
					for (x = x4, xp_0 = 0;
						x < x4 + 17;
						x += 1, xp_0 += 1){
								mem_vec_84 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f]);
								mem_vec_85 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 16]);
								mem_vec_86 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
								mem_vec_87 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16]);
								mem_vec_88 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
								mem_vec_89 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16]);
								mem_vec_90 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
								mem_vec_91 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16]);
								mem_vec_92 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f]);
								mem_vec_93 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16]);
								mem_vec_94 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f]);
								mem_vec_95 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16]);
								mem_vec_96 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f]);
								mem_vec_97 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16]);
								mem_vec_98 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 7) + f]);
								mem_vec_99 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 7) + f + 16]);
								mem_vec_100 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 8) + f]);
								mem_vec_101 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 8) + f + 16]);
								// y = ph_y, x = 1, h = 1, w = 1, c = 32, f = 32
								// T (c, 32) (32 / 1)
								for (c = c6, cp_1 = c6_p_0, cp_0 = 0;
									c < c6 + 32;
									c += 1, cp_1 += 1, cp_0 += 1){
									scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
									vec_1 = _mm512_set1_ps(scal_0);
									vec_2 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

									vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_84);
									mem_vec_84 = vec_0;

									vec_4 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 16]);

									vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_85);
									mem_vec_85 = vec_3;
									scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
									vec_6 = _mm512_set1_ps(scal_1);


									vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_86);
									mem_vec_86 = vec_5;



									vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_87);
									mem_vec_87 = vec_7;
									scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
									vec_9 = _mm512_set1_ps(scal_2);


									vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_88);
									mem_vec_88 = vec_8;



									vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_89);
									mem_vec_89 = vec_10;
									scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
									vec_12 = _mm512_set1_ps(scal_3);


									vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_90);
									mem_vec_90 = vec_11;



									vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_91);
									mem_vec_91 = vec_13;
									scal_4 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h) + c];
									vec_15 = _mm512_set1_ps(scal_4);


									vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_92);
									mem_vec_92 = vec_14;



									vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_93);
									mem_vec_93 = vec_16;
									scal_5 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h) + c];
									vec_18 = _mm512_set1_ps(scal_5);


									vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_94);
									mem_vec_94 = vec_17;



									vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_95);
									mem_vec_95 = vec_19;
									scal_6 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h) + c];
									vec_21 = _mm512_set1_ps(scal_6);


									vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_96);
									mem_vec_96 = vec_20;



									vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_97);
									mem_vec_97 = vec_22;
									scal_7 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h) + c];
									vec_24 = _mm512_set1_ps(scal_7);


									vec_23 = _mm512_fmadd_ps(vec_24, vec_2, mem_vec_98);
									mem_vec_98 = vec_23;



									vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_99);
									mem_vec_99 = vec_25;
									scal_8 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 8 + h) + c];
									vec_27 = _mm512_set1_ps(scal_8);


									vec_26 = _mm512_fmadd_ps(vec_27, vec_2, mem_vec_100);
									mem_vec_100 = vec_26;



									vec_28 = _mm512_fmadd_ps(vec_27, vec_4, mem_vec_101);
									mem_vec_101 = vec_28;
								}
							_mm512_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_84);
							_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 16], mem_vec_85);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_86);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16], mem_vec_87);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_88);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16], mem_vec_89);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_90);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16], mem_vec_91);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f], mem_vec_92);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16], mem_vec_93);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f], mem_vec_94);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16], mem_vec_95);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f], mem_vec_96);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16], mem_vec_97);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 7) + f], mem_vec_98);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 7) + f + 16], mem_vec_99);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 8) + f], mem_vec_100);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 8) + f + 16], mem_vec_101);
					}
				}
			}
	}
}


}