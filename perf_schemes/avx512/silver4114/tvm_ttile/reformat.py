import re


defaultCsvList=["result_MobilNet_1.csv","result_MobilNet_5.csv","result_MobilNet_9.csv","result_ResNet18_1.csv","result_ResNet18_5.csv","result_ResNet18_9.csv","result_Yolo9000_18.csv","result_Yolo9000_5.csv","result_MobilNet_2.csv","result_MobilNet_6.csv","result_ResNet18_10.csv","result_ResNet18_2.csv","result_ResNet18_6.csv","result_Yolo9000_0.csv","result_Yolo9000_19.csv","result_Yolo9000_8.csv","result_MobilNet_3.csv","result_MobilNet_7.csv","result_ResNet18_11.csv","result_ResNet18_3.csv","result_ResNet18_7.csv","result_Yolo9000_12.csv","result_Yolo9000_2.csv","result_Yolo9000_9.csv","result_MobilNet_4.csv","result_MobilNet_8.csv","result_ResNet18_12.csv","result_ResNet18_4.csv","result_ResNet18_8.csv","result_Yolo9000_13.csv","result_Yolo9000_4.csv","result_Yolo9000_23.csv"]
for fname in defaultCsvList:
    fout = "formatted/"
    fout += fname
    print(fname)
    with open(fout, "w")  as ouf:
        with open(fname, "r") as inf:
            first = True
            for l in inf.readlines():
                if first:
                    first = False
                    ls = l.split(";")
                    last = ls[-1]
                    slast = last.split("(")
                    print(l)
                    keep = slast[0]
                    slast = str(slast[1])
                    n = re.sub("\)\n","",slast)
                    p = ls[:-1]+[keep]
                    ouf.write(";".join(p)+"\n")
                    second = [-1,"ttile",n,0,0,0,0,0,n]
                    ouf.write(";".join([str(x) for x in second])+"\n")
                else:
                    ouf.write(l)
