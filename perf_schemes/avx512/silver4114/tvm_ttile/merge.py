defaultCsvList=["result_MobilNet_1.csv","result_MobilNet_5.csv","result_MobilNet_9.csv","result_ResNet18_1.csv","result_ResNet18_5.csv","result_ResNet18_9.csv","result_Yolo9000_18.csv","result_Yolo9000_5.csv","result_MobilNet_2.csv","result_MobilNet_6.csv","result_ResNet18_10.csv","result_ResNet18_2.csv","result_ResNet18_6.csv","result_Yolo9000_0.csv","result_Yolo9000_19.csv","result_Yolo9000_8.csv","result_MobilNet_3.csv","result_MobilNet_7.csv","result_ResNet18_11.csv","result_ResNet18_3.csv","result_ResNet18_7.csv","result_Yolo9000_12.csv","result_Yolo9000_2.csv","result_Yolo9000_9.csv","result_MobilNet_4.csv","result_MobilNet_8.csv","result_ResNet18_12.csv","result_ResNet18_4.csv","result_ResNet18_8.csv","result_Yolo9000_13.csv","result_Yolo9000_4.csv","result_Yolo9000_23.csv"]

import pandas as pd
dfs = []
for fname in defaultCsvList:
    print(fname)
    df = pd.read_csv(fname,sep=";")
    dfs.append(df)

big_df = pd.concat(dfs)
big_df.to_csv("fulldata.csv")
