
#include <immintrin.h>
typedef int IND_TYPE;
typedef float M_TYPE;
        void gen_conv1(M_TYPE * const  __restrict__ output,
M_TYPE const * const __restrict__ input, M_TYPE const * const __restrict__ params,
    IND_TYPE X, IND_TYPE W,
    IND_TYPE Y, IND_TYPE H,
	IND_TYPE C, IND_TYPE F, int strideO1, int strideO2, int strideA1, int strideA2, int strideW1, int strideW2, int strideW3) {
/*
[V f; U (2, f); ULambda y; U (3, h); U (3, w); T (64, c); Hoist_vars [c];
  T (68, x); T (2, c); T (2, f);
  Lambda_apply y [((Iter 6), (Arg 9)); ((Iter 1), (Arg 14))]; T (1, x);
  T (1, f); T (4, f)]
*/
IND_TYPE c, cp_0, c345_p_0, cp_1, c345, f, fp_0, f460_p_0, f461_p_0, fp_1, f460_p_1, fp_2, f460, f461, x, xp_0, x345_p_0, xp_1, x345, y, yp_0;
IND_TYPE y230 = 0;
IND_TYPE x346 = 0;
IND_TYPE h = 0;
IND_TYPE w = 0;
IND_TYPE c346 = 0;
IND_TYPE f462 = 0;
float scal_0 ,scal_1 ,scal_10 ,scal_100 ,scal_101 ,scal_102 ,scal_103 ,scal_104 ,scal_105 ,scal_106 ,scal_107 ,scal_108 ,scal_109 ,scal_11 ,scal_110 ,scal_111 ,scal_112 ,scal_113 ,scal_114 ,scal_115 ,scal_116 ,scal_117 ,scal_118 ,scal_119 ,scal_12 ,scal_120 ,scal_121 ,scal_122 ,scal_123 ,scal_124 ,scal_125 ,scal_13 ,scal_14 ,scal_15 ,scal_16 ,scal_17 ,scal_18 ,scal_19 ,scal_2 ,scal_20 ,scal_21 ,scal_22 ,scal_23 ,scal_24 ,scal_25 ,scal_26 ,scal_27 ,scal_28 ,scal_29 ,scal_3 ,scal_30 ,scal_31 ,scal_32 ,scal_33 ,scal_34 ,scal_35 ,scal_36 ,scal_37 ,scal_38 ,scal_39 ,scal_4 ,scal_40 ,scal_41 ,scal_42 ,scal_43 ,scal_44 ,scal_45 ,scal_46 ,scal_47 ,scal_48 ,scal_49 ,scal_5 ,scal_50 ,scal_51 ,scal_52 ,scal_53 ,scal_54 ,scal_55 ,scal_56 ,scal_57 ,scal_58 ,scal_59 ,scal_6 ,scal_60 ,scal_61 ,scal_62 ,scal_63 ,scal_64 ,scal_65 ,scal_66 ,scal_67 ,scal_68 ,scal_69 ,scal_7 ,scal_70 ,scal_71 ,scal_72 ,scal_73 ,scal_74 ,scal_75 ,scal_76 ,scal_77 ,scal_78 ,scal_79 ,scal_8 ,scal_80 ,scal_81 ,scal_82 ,scal_83 ,scal_84 ,scal_85 ,scal_86 ,scal_87 ,scal_88 ,scal_89 ,scal_9 ,scal_90 ,scal_91 ,scal_92 ,scal_93 ,scal_94 ,scal_95 ,scal_96 ,scal_97 ,scal_98 ,scal_99;
__m512 mem_vec_5026 ,mem_vec_5027 ,mem_vec_5028 ,mem_vec_5029 ,mem_vec_5030 ,mem_vec_5031 ,mem_vec_5032 ,mem_vec_5033 ,mem_vec_5034 ,mem_vec_5035 ,mem_vec_5036 ,mem_vec_5037 ,mem_vec_5038 ,mem_vec_5039 ,mem_vec_5040 ,mem_vec_5041 ,mem_vec_5042 ,mem_vec_5043 ,mem_vec_5044 ,mem_vec_5045 ,mem_vec_5046 ,mem_vec_5047 ,mem_vec_5048 ,mem_vec_5049 ,mem_vec_5050 ,mem_vec_5051 ,mem_vec_5052 ,mem_vec_5053 ,mem_vec_5054 ,mem_vec_5055 ,mem_vec_5056 ,mem_vec_5057 ,mem_vec_5058 ,mem_vec_5059 ,mem_vec_5060 ,mem_vec_5061 ,mem_vec_5062 ,mem_vec_5063 ,mem_vec_5064 ,mem_vec_5065 ,mem_vec_5066 ,mem_vec_5067 ,mem_vec_5068 ,mem_vec_5069 ,mem_vec_5070 ,mem_vec_5071 ,vec_0 ,vec_1 ,vec_10 ,vec_100 ,vec_101 ,vec_102 ,vec_103 ,vec_104 ,vec_105 ,vec_106 ,vec_107 ,vec_108 ,vec_109 ,vec_11 ,vec_110 ,vec_111 ,vec_112 ,vec_113 ,vec_114 ,vec_115 ,vec_116 ,vec_117 ,vec_118 ,vec_119 ,vec_12 ,vec_120 ,vec_121 ,vec_122 ,vec_123 ,vec_124 ,vec_125 ,vec_126 ,vec_127 ,vec_128 ,vec_129 ,vec_13 ,vec_130 ,vec_131 ,vec_132 ,vec_133 ,vec_134 ,vec_135 ,vec_136 ,vec_137 ,vec_138 ,vec_139 ,vec_14 ,vec_140 ,vec_141 ,vec_142 ,vec_143 ,vec_144 ,vec_145 ,vec_146 ,vec_147 ,vec_148 ,vec_149 ,vec_15 ,vec_150 ,vec_151 ,vec_152 ,vec_153 ,vec_154 ,vec_155 ,vec_156 ,vec_157 ,vec_158 ,vec_159 ,vec_16 ,vec_160 ,vec_161 ,vec_162 ,vec_163 ,vec_164 ,vec_165 ,vec_166 ,vec_167 ,vec_168 ,vec_169 ,vec_17 ,vec_170 ,vec_171 ,vec_172 ,vec_173 ,vec_174 ,vec_175 ,vec_176 ,vec_177 ,vec_178 ,vec_179 ,vec_18 ,vec_180 ,vec_181 ,vec_182 ,vec_183 ,vec_184 ,vec_185 ,vec_186 ,vec_187 ,vec_188 ,vec_189 ,vec_19 ,vec_190 ,vec_191 ,vec_192 ,vec_193 ,vec_194 ,vec_195 ,vec_196 ,vec_197 ,vec_198 ,vec_199 ,vec_2 ,vec_20 ,vec_200 ,vec_201 ,vec_202 ,vec_203 ,vec_204 ,vec_205 ,vec_206 ,vec_207 ,vec_208 ,vec_209 ,vec_21 ,vec_210 ,vec_211 ,vec_212 ,vec_213 ,vec_214 ,vec_215 ,vec_216 ,vec_217 ,vec_218 ,vec_219 ,vec_22 ,vec_220 ,vec_221 ,vec_222 ,vec_223 ,vec_224 ,vec_225 ,vec_226 ,vec_227 ,vec_228 ,vec_229 ,vec_23 ,vec_230 ,vec_231 ,vec_232 ,vec_233 ,vec_234 ,vec_235 ,vec_236 ,vec_237 ,vec_238 ,vec_239 ,vec_24 ,vec_240 ,vec_241 ,vec_242 ,vec_243 ,vec_244 ,vec_245 ,vec_246 ,vec_247 ,vec_248 ,vec_249 ,vec_25 ,vec_250 ,vec_251 ,vec_252 ,vec_253 ,vec_254 ,vec_255 ,vec_256 ,vec_257 ,vec_258 ,vec_259 ,vec_26 ,vec_260 ,vec_261 ,vec_262 ,vec_263 ,vec_264 ,vec_265 ,vec_266 ,vec_267 ,vec_268 ,vec_269 ,vec_27 ,vec_270 ,vec_271 ,vec_272 ,vec_273 ,vec_274 ,vec_275 ,vec_276 ,vec_277 ,vec_278 ,vec_279 ,vec_28 ,vec_280 ,vec_281 ,vec_282 ,vec_283 ,vec_284 ,vec_285 ,vec_286 ,vec_287 ,vec_288 ,vec_289 ,vec_29 ,vec_290 ,vec_291 ,vec_292 ,vec_293 ,vec_294 ,vec_295 ,vec_296 ,vec_297 ,vec_298 ,vec_299 ,vec_3 ,vec_30 ,vec_300 ,vec_301 ,vec_302 ,vec_303 ,vec_304 ,vec_305 ,vec_306 ,vec_307 ,vec_308 ,vec_309 ,vec_31 ,vec_310 ,vec_311 ,vec_312 ,vec_313 ,vec_314 ,vec_315 ,vec_316 ,vec_317 ,vec_318 ,vec_319 ,vec_32 ,vec_320 ,vec_321 ,vec_322 ,vec_323 ,vec_324 ,vec_325 ,vec_326 ,vec_327 ,vec_328 ,vec_329 ,vec_33 ,vec_330 ,vec_331 ,vec_332 ,vec_333 ,vec_334 ,vec_335 ,vec_336 ,vec_337 ,vec_338 ,vec_339 ,vec_34 ,vec_340 ,vec_341 ,vec_342 ,vec_343 ,vec_344 ,vec_345 ,vec_346 ,vec_347 ,vec_348 ,vec_349 ,vec_35 ,vec_350 ,vec_351 ,vec_352 ,vec_353 ,vec_354 ,vec_355 ,vec_356 ,vec_357 ,vec_358 ,vec_359 ,vec_36 ,vec_360 ,vec_361 ,vec_362 ,vec_363 ,vec_364 ,vec_365 ,vec_366 ,vec_367 ,vec_368 ,vec_369 ,vec_37 ,vec_370 ,vec_371 ,vec_372 ,vec_373 ,vec_374 ,vec_375 ,vec_376 ,vec_377 ,vec_378 ,vec_379 ,vec_38 ,vec_380 ,vec_381 ,vec_382 ,vec_383 ,vec_384 ,vec_385 ,vec_386 ,vec_387 ,vec_388 ,vec_389 ,vec_39 ,vec_390 ,vec_391 ,vec_392 ,vec_393 ,vec_394 ,vec_395 ,vec_4 ,vec_40 ,vec_41 ,vec_42 ,vec_43 ,vec_44 ,vec_45 ,vec_46 ,vec_47 ,vec_48 ,vec_49 ,vec_5 ,vec_50 ,vec_51 ,vec_52 ,vec_53 ,vec_54 ,vec_55 ,vec_56 ,vec_57 ,vec_58 ,vec_59 ,vec_6 ,vec_60 ,vec_61 ,vec_62 ,vec_63 ,vec_64 ,vec_65 ,vec_66 ,vec_67 ,vec_68 ,vec_69 ,vec_7 ,vec_70 ,vec_71 ,vec_72 ,vec_73 ,vec_74 ,vec_75 ,vec_76 ,vec_77 ,vec_78 ,vec_79 ,vec_8 ,vec_80 ,vec_81 ,vec_82 ,vec_83 ,vec_84 ,vec_85 ,vec_86 ,vec_87 ,vec_88 ,vec_89 ,vec_9 ,vec_90 ,vec_91 ,vec_92 ,vec_93 ,vec_94 ,vec_95 ,vec_96 ,vec_97 ,vec_98 ,vec_99;
// y = 68, x = 68, h = 3, w = 3, c = 128, f = 256
// T (f, 4) (256 / 64)
f461 = 0;
f461_p_0 = 0;
f460 = 0;
f460_p_1 = 0;
x345 = 0;
x345_p_0 = 0;
y = 0;
yp_0 = 0;
f = 0;
fp_2 = 0;
c345 = 0;
c345_p_0 = 0;
							for (x = x345, xp_1 = x345_p_0, xp_0 = 0;x < x345 + 68;x += 1, xp_1 += 1, xp_0 += 1){
										mem_vec_5026 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * y + f]);
										mem_vec_5027 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * y + f + 16]);
										mem_vec_5028 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 1) + f]);
										mem_vec_5029 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 1) + f + 16]);
										mem_vec_5030 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 2) + f]);
										mem_vec_5031 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 2) + f + 16]);
										mem_vec_5032 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 3) + f]);
										mem_vec_5033 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 3) + f + 16]);
										mem_vec_5034 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 4) + f]);
										mem_vec_5035 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 4) + f + 16]);
										mem_vec_5036 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 5) + f]);
										mem_vec_5037 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 5) + f + 16]);
										mem_vec_5038 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 6) + f]);
										mem_vec_5039 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 6) + f + 16]);
										mem_vec_5040 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 7) + f]);
										mem_vec_5041 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 7) + f + 16]);
										mem_vec_5042 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 8) + f]);
										mem_vec_5043 = _mm512_loadu_ps(&output[strideO1 * x + strideO2 * (y + 8) + f + 16]);
										// y = ph_y, x = 1, h = 3, w = 3, c = 64, f = 32
										// T (c, 64) (64 / 1)
										for (c = c345, cp_1 = c345_p_0, cp_0 = 0;c < c345 + 64;c += 1, cp_1 += 1, cp_0 += 1){
											scal_0 = input[strideA1 * (x + w) + strideA2 * (y + h) + c];
											vec_1 = _mm512_set1_ps(scal_0);
											vec_2 = _mm512_loadu_ps(&params[strideW1 * w + strideW2 * h + strideW3 * c + f]);
											vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_5026);
											mem_vec_5026 = vec_0;
											vec_4 = _mm512_loadu_ps(&params[strideW1 * w + strideW2 * h + strideW3 * c + f + 16]);
											vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_5027);
											mem_vec_5027 = vec_3;
											scal_1 = input[strideA1 * (x + w) + strideA2 * (y + 1 + h) + c];
											vec_6 = _mm512_set1_ps(scal_1);
											vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_5028);
											mem_vec_5028 = vec_5;
											vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_5029);
											mem_vec_5029 = vec_7;
											scal_2 = input[strideA1 * (x + w) + strideA2 * (y + 2 + h) + c];
											vec_9 = _mm512_set1_ps(scal_2);
											vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_5030);
											mem_vec_5030 = vec_8;
											vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_5031);
											mem_vec_5031 = vec_10;
											scal_3 = input[strideA1 * (x + w) + strideA2 * (y + 3 + h) + c];
											vec_12 = _mm512_set1_ps(scal_3);
											vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_5032);
											mem_vec_5032 = vec_11;
											vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_5033);
											mem_vec_5033 = vec_13;
											scal_4 = input[strideA1 * (x + w) + strideA2 * (y + 4 + h) + c];
											vec_15 = _mm512_set1_ps(scal_4);
											vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_5034);
											mem_vec_5034 = vec_14;
											vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_5035);
											mem_vec_5035 = vec_16;
											scal_5 = input[strideA1 * (x + w) + strideA2 * (y + 5 + h) + c];
											vec_18 = _mm512_set1_ps(scal_5);
											vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_5036);
											mem_vec_5036 = vec_17;
											vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_5037);
											mem_vec_5037 = vec_19;
											scal_6 = input[strideA1 * (x + w) + strideA2 * (y + 6 + h) + c];
											vec_21 = _mm512_set1_ps(scal_6);
											vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_5038);
											mem_vec_5038 = vec_20;
											vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_5039);
											mem_vec_5039 = vec_22;
											scal_7 = input[strideA1 * (x + w) + strideA2 * (y + 7 + h) + c];
											vec_24 = _mm512_set1_ps(scal_7);
											vec_23 = _mm512_fmadd_ps(vec_24, vec_2, mem_vec_5040);
											mem_vec_5040 = vec_23;
											vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_5041);
											mem_vec_5041 = vec_25;
											scal_8 = input[strideA1 * (x + w) + strideA2 * (y + 8 + h) + c];
											vec_27 = _mm512_set1_ps(scal_8);
											vec_26 = _mm512_fmadd_ps(vec_27, vec_2, mem_vec_5042);
											mem_vec_5042 = vec_26;
											vec_28 = _mm512_fmadd_ps(vec_27, vec_4, mem_vec_5043);
											mem_vec_5043 = vec_28;
											scal_9 = input[strideA1 * (x + w) + strideA2 * (y + h + 1) + c];
											vec_30 = _mm512_set1_ps(scal_9);
											vec_31 = _mm512_loadu_ps(&params[strideW1 * w + strideW2 * (h + 1) + strideW3 * c + f]);
											vec_29 = _mm512_fmadd_ps(vec_30, vec_31, mem_vec_5026);
											mem_vec_5026 = vec_29;
											vec_33 = _mm512_loadu_ps(&params[strideW1 * w + strideW2 * (h + 1) + strideW3 * c + f + 16]);
											vec_32 = _mm512_fmadd_ps(vec_30, vec_33, mem_vec_5027);
											mem_vec_5027 = vec_32;
											scal_10 = input[strideA1 * (x + w) + strideA2 * (y + 1 + h + 1) + c];
											vec_35 = _mm512_set1_ps(scal_10);
											vec_34 = _mm512_fmadd_ps(vec_35, vec_31, mem_vec_5028);
											mem_vec_5028 = vec_34;
											vec_36 = _mm512_fmadd_ps(vec_35, vec_33, mem_vec_5029);
											mem_vec_5029 = vec_36;
											scal_11 = input[strideA1 * (x + w) + strideA2 * (y + 2 + h + 1) + c];
											vec_38 = _mm512_set1_ps(scal_11);
											vec_37 = _mm512_fmadd_ps(vec_38, vec_31, mem_vec_5030);
											mem_vec_5030 = vec_37;
											vec_39 = _mm512_fmadd_ps(vec_38, vec_33, mem_vec_5031);
											mem_vec_5031 = vec_39;
											scal_12 = input[strideA1 * (x + w) + strideA2 * (y + 3 + h + 1) + c];
											vec_41 = _mm512_set1_ps(scal_12);
											vec_40 = _mm512_fmadd_ps(vec_41, vec_31, mem_vec_5032);
											mem_vec_5032 = vec_40;
											vec_42 = _mm512_fmadd_ps(vec_41, vec_33, mem_vec_5033);
											mem_vec_5033 = vec_42;
											scal_13 = input[strideA1 * (x + w) + strideA2 * (y + 4 + h + 1) + c];
											vec_44 = _mm512_set1_ps(scal_13);
											vec_43 = _mm512_fmadd_ps(vec_44, vec_31, mem_vec_5034);
											mem_vec_5034 = vec_43;
											vec_45 = _mm512_fmadd_ps(vec_44, vec_33, mem_vec_5035);
											mem_vec_5035 = vec_45;
											scal_14 = input[strideA1 * (x + w) + strideA2 * (y + 5 + h + 1) + c];
											vec_47 = _mm512_set1_ps(scal_14);
											vec_46 = _mm512_fmadd_ps(vec_47, vec_31, mem_vec_5036);
											mem_vec_5036 = vec_46;
											vec_48 = _mm512_fmadd_ps(vec_47, vec_33, mem_vec_5037);
											mem_vec_5037 = vec_48;
											scal_15 = input[strideA1 * (x + w) + strideA2 * (y + 6 + h + 1) + c];
											vec_50 = _mm512_set1_ps(scal_15);
											vec_49 = _mm512_fmadd_ps(vec_50, vec_31, mem_vec_5038);
											mem_vec_5038 = vec_49;
											vec_51 = _mm512_fmadd_ps(vec_50, vec_33, mem_vec_5039);
											mem_vec_5039 = vec_51;
											scal_16 = input[strideA1 * (x + w) + strideA2 * (y + 7 + h + 1) + c];
											vec_53 = _mm512_set1_ps(scal_16);
											vec_52 = _mm512_fmadd_ps(vec_53, vec_31, mem_vec_5040);
											mem_vec_5040 = vec_52;
											vec_54 = _mm512_fmadd_ps(vec_53, vec_33, mem_vec_5041);
											mem_vec_5041 = vec_54;
											scal_17 = input[strideA1 * (x + w) + strideA2 * (y + 8 + h + 1) + c];
											vec_56 = _mm512_set1_ps(scal_17);
											vec_55 = _mm512_fmadd_ps(vec_56, vec_31, mem_vec_5042);
											mem_vec_5042 = vec_55;
											vec_57 = _mm512_fmadd_ps(vec_56, vec_33, mem_vec_5043);
											mem_vec_5043 = vec_57;
											scal_18 = input[strideA1 * (x + w) + strideA2 * (y + h + 2) + c];
											vec_59 = _mm512_set1_ps(scal_18);
											vec_60 = _mm512_loadu_ps(&params[strideW1 * w + strideW2 * (h + 2) + strideW3 * c + f]);
											vec_58 = _mm512_fmadd_ps(vec_59, vec_60, mem_vec_5026);
											mem_vec_5026 = vec_58;
											vec_62 = _mm512_loadu_ps(&params[strideW1 * w + strideW2 * (h + 2) + strideW3 * c + f + 16]);
											vec_61 = _mm512_fmadd_ps(vec_59, vec_62, mem_vec_5027);
											mem_vec_5027 = vec_61;
											scal_19 = input[strideA1 * (x + w) + strideA2 * (y + 1 + h + 2) + c];
											vec_64 = _mm512_set1_ps(scal_19);
											vec_63 = _mm512_fmadd_ps(vec_64, vec_60, mem_vec_5028);
											mem_vec_5028 = vec_63;
											vec_65 = _mm512_fmadd_ps(vec_64, vec_62, mem_vec_5029);
											mem_vec_5029 = vec_65;
											scal_20 = input[strideA1 * (x + w) + strideA2 * (y + 2 + h + 2) + c];
											vec_67 = _mm512_set1_ps(scal_20);
											vec_66 = _mm512_fmadd_ps(vec_67, vec_60, mem_vec_5030);
											mem_vec_5030 = vec_66;
											vec_68 = _mm512_fmadd_ps(vec_67, vec_62, mem_vec_5031);
											mem_vec_5031 = vec_68;
											scal_21 = input[strideA1 * (x + w) + strideA2 * (y + 3 + h + 2) + c];
											vec_70 = _mm512_set1_ps(scal_21);
											vec_69 = _mm512_fmadd_ps(vec_70, vec_60, mem_vec_5032);
											mem_vec_5032 = vec_69;
											vec_71 = _mm512_fmadd_ps(vec_70, vec_62, mem_vec_5033);
											mem_vec_5033 = vec_71;
											scal_22 = input[strideA1 * (x + w) + strideA2 * (y + 4 + h + 2) + c];
											vec_73 = _mm512_set1_ps(scal_22);
											vec_72 = _mm512_fmadd_ps(vec_73, vec_60, mem_vec_5034);
											mem_vec_5034 = vec_72;
											vec_74 = _mm512_fmadd_ps(vec_73, vec_62, mem_vec_5035);
											mem_vec_5035 = vec_74;
											scal_23 = input[strideA1 * (x + w) + strideA2 * (y + 5 + h + 2) + c];
											vec_76 = _mm512_set1_ps(scal_23);
											vec_75 = _mm512_fmadd_ps(vec_76, vec_60, mem_vec_5036);
											mem_vec_5036 = vec_75;
											vec_77 = _mm512_fmadd_ps(vec_76, vec_62, mem_vec_5037);
											mem_vec_5037 = vec_77;
											scal_24 = input[strideA1 * (x + w) + strideA2 * (y + 6 + h + 2) + c];
											vec_79 = _mm512_set1_ps(scal_24);
											vec_78 = _mm512_fmadd_ps(vec_79, vec_60, mem_vec_5038);
											mem_vec_5038 = vec_78;
											vec_80 = _mm512_fmadd_ps(vec_79, vec_62, mem_vec_5039);
											mem_vec_5039 = vec_80;
											scal_25 = input[strideA1 * (x + w) + strideA2 * (y + 7 + h + 2) + c];
											vec_82 = _mm512_set1_ps(scal_25);
											vec_81 = _mm512_fmadd_ps(vec_82, vec_60, mem_vec_5040);
											mem_vec_5040 = vec_81;
											vec_83 = _mm512_fmadd_ps(vec_82, vec_62, mem_vec_5041);
											mem_vec_5041 = vec_83;
											scal_26 = input[strideA1 * (x + w) + strideA2 * (y + 8 + h + 2) + c];
											vec_85 = _mm512_set1_ps(scal_26);
											vec_84 = _mm512_fmadd_ps(vec_85, vec_60, mem_vec_5042);
											mem_vec_5042 = vec_84;
											vec_86 = _mm512_fmadd_ps(vec_85, vec_62, mem_vec_5043);
											mem_vec_5043 = vec_86;
											scal_27 = input[strideA1 * (x + w + 1) + strideA2 * (y + h) + c];
											vec_88 = _mm512_set1_ps(scal_27);
											vec_89 = _mm512_loadu_ps(&params[strideW1 * (w + 1) + strideW2 * h + strideW3 * c + f]);
											vec_87 = _mm512_fmadd_ps(vec_88, vec_89, mem_vec_5026);
											mem_vec_5026 = vec_87;
											vec_91 = _mm512_loadu_ps(&params[strideW1 * (w + 1) + strideW2 * h + strideW3 * c + f + 16]);
											vec_90 = _mm512_fmadd_ps(vec_88, vec_91, mem_vec_5027);
											mem_vec_5027 = vec_90;
											scal_28 = input[strideA1 * (x + w + 1) + strideA2 * (y + 1 + h) + c];
											vec_93 = _mm512_set1_ps(scal_28);
											vec_92 = _mm512_fmadd_ps(vec_93, vec_89, mem_vec_5028);
											mem_vec_5028 = vec_92;
											vec_94 = _mm512_fmadd_ps(vec_93, vec_91, mem_vec_5029);
											mem_vec_5029 = vec_94;
											scal_29 = input[strideA1 * (x + w + 1) + strideA2 * (y + 2 + h) + c];
											vec_96 = _mm512_set1_ps(scal_29);
											vec_95 = _mm512_fmadd_ps(vec_96, vec_89, mem_vec_5030);
											mem_vec_5030 = vec_95;
											vec_97 = _mm512_fmadd_ps(vec_96, vec_91, mem_vec_5031);
											mem_vec_5031 = vec_97;
											scal_30 = input[strideA1 * (x + w + 1) + strideA2 * (y + 3 + h) + c];
											vec_99 = _mm512_set1_ps(scal_30);
											vec_98 = _mm512_fmadd_ps(vec_99, vec_89, mem_vec_5032);
											mem_vec_5032 = vec_98;
											vec_100 = _mm512_fmadd_ps(vec_99, vec_91, mem_vec_5033);
											mem_vec_5033 = vec_100;
											scal_31 = input[strideA1 * (x + w + 1) + strideA2 * (y + 4 + h) + c];
											vec_102 = _mm512_set1_ps(scal_31);
											vec_101 = _mm512_fmadd_ps(vec_102, vec_89, mem_vec_5034);
											mem_vec_5034 = vec_101;
											vec_103 = _mm512_fmadd_ps(vec_102, vec_91, mem_vec_5035);
											mem_vec_5035 = vec_103;
											scal_32 = input[strideA1 * (x + w + 1) + strideA2 * (y + 5 + h) + c];
											vec_105 = _mm512_set1_ps(scal_32);
											vec_104 = _mm512_fmadd_ps(vec_105, vec_89, mem_vec_5036);
											mem_vec_5036 = vec_104;
											vec_106 = _mm512_fmadd_ps(vec_105, vec_91, mem_vec_5037);
											mem_vec_5037 = vec_106;
											scal_33 = input[strideA1 * (x + w + 1) + strideA2 * (y + 6 + h) + c];
											vec_108 = _mm512_set1_ps(scal_33);
											vec_107 = _mm512_fmadd_ps(vec_108, vec_89, mem_vec_5038);
											mem_vec_5038 = vec_107;
											vec_109 = _mm512_fmadd_ps(vec_108, vec_91, mem_vec_5039);
											mem_vec_5039 = vec_109;
											scal_34 = input[strideA1 * (x + w + 1) + strideA2 * (y + 7 + h) + c];
											vec_111 = _mm512_set1_ps(scal_34);
											vec_110 = _mm512_fmadd_ps(vec_111, vec_89, mem_vec_5040);
											mem_vec_5040 = vec_110;
											vec_112 = _mm512_fmadd_ps(vec_111, vec_91, mem_vec_5041);
											mem_vec_5041 = vec_112;
											scal_35 = input[strideA1 * (x + w + 1) + strideA2 * (y + 8 + h) + c];
											vec_114 = _mm512_set1_ps(scal_35);
											vec_113 = _mm512_fmadd_ps(vec_114, vec_89, mem_vec_5042);
											mem_vec_5042 = vec_113;
											vec_115 = _mm512_fmadd_ps(vec_114, vec_91, mem_vec_5043);
											mem_vec_5043 = vec_115;
											scal_36 = input[strideA1 * (x + w + 1) + strideA2 * (y + h + 1) + c];
											vec_117 = _mm512_set1_ps(scal_36);
											vec_118 = _mm512_loadu_ps(&params[strideW1 * (w + 1) + strideW2 * (h + 1) + strideW3 * c + f]);
											vec_116 = _mm512_fmadd_ps(vec_117, vec_118, mem_vec_5026);
											mem_vec_5026 = vec_116;
											vec_120 = _mm512_loadu_ps(&params[strideW1 * (w + 1) + strideW2 * (h + 1) + strideW3 * c + f + 16]);
											vec_119 = _mm512_fmadd_ps(vec_117, vec_120, mem_vec_5027);
											mem_vec_5027 = vec_119;
											scal_37 = input[strideA1 * (x + w + 1) + strideA2 * (y + 1 + h + 1) + c];
											vec_122 = _mm512_set1_ps(scal_37);
											vec_121 = _mm512_fmadd_ps(vec_122, vec_118, mem_vec_5028);
											mem_vec_5028 = vec_121;
											vec_123 = _mm512_fmadd_ps(vec_122, vec_120, mem_vec_5029);
											mem_vec_5029 = vec_123;
											scal_38 = input[strideA1 * (x + w + 1) + strideA2 * (y + 2 + h + 1) + c];
											vec_125 = _mm512_set1_ps(scal_38);
											vec_124 = _mm512_fmadd_ps(vec_125, vec_118, mem_vec_5030);
											mem_vec_5030 = vec_124;
											vec_126 = _mm512_fmadd_ps(vec_125, vec_120, mem_vec_5031);
											mem_vec_5031 = vec_126;
											scal_39 = input[strideA1 * (x + w + 1) + strideA2 * (y + 3 + h + 1) + c];
											vec_128 = _mm512_set1_ps(scal_39);
											vec_127 = _mm512_fmadd_ps(vec_128, vec_118, mem_vec_5032);
											mem_vec_5032 = vec_127;
											vec_129 = _mm512_fmadd_ps(vec_128, vec_120, mem_vec_5033);
											mem_vec_5033 = vec_129;
											scal_40 = input[strideA1 * (x + w + 1) + strideA2 * (y + 4 + h + 1) + c];
											vec_131 = _mm512_set1_ps(scal_40);
											vec_130 = _mm512_fmadd_ps(vec_131, vec_118, mem_vec_5034);
											mem_vec_5034 = vec_130;
											vec_132 = _mm512_fmadd_ps(vec_131, vec_120, mem_vec_5035);
											mem_vec_5035 = vec_132;
											scal_41 = input[strideA1 * (x + w + 1) + strideA2 * (y + 5 + h + 1) + c];
											vec_134 = _mm512_set1_ps(scal_41);
											vec_133 = _mm512_fmadd_ps(vec_134, vec_118, mem_vec_5036);
											mem_vec_5036 = vec_133;
											vec_135 = _mm512_fmadd_ps(vec_134, vec_120, mem_vec_5037);
											mem_vec_5037 = vec_135;
											scal_42 = input[strideA1 * (x + w + 1) + strideA2 * (y + 6 + h + 1) + c];
											vec_137 = _mm512_set1_ps(scal_42);
											vec_136 = _mm512_fmadd_ps(vec_137, vec_118, mem_vec_5038);
											mem_vec_5038 = vec_136;
											vec_138 = _mm512_fmadd_ps(vec_137, vec_120, mem_vec_5039);
											mem_vec_5039 = vec_138;
											scal_43 = input[strideA1 * (x + w + 1) + strideA2 * (y + 7 + h + 1) + c];
											vec_140 = _mm512_set1_ps(scal_43);
											vec_139 = _mm512_fmadd_ps(vec_140, vec_118, mem_vec_5040);
											mem_vec_5040 = vec_139;
											vec_141 = _mm512_fmadd_ps(vec_140, vec_120, mem_vec_5041);
											mem_vec_5041 = vec_141;
											scal_44 = input[strideA1 * (x + w + 1) + strideA2 * (y + 8 + h + 1) + c];
											vec_143 = _mm512_set1_ps(scal_44);
											vec_142 = _mm512_fmadd_ps(vec_143, vec_118, mem_vec_5042);
											mem_vec_5042 = vec_142;
											vec_144 = _mm512_fmadd_ps(vec_143, vec_120, mem_vec_5043);
											mem_vec_5043 = vec_144;
											scal_45 = input[strideA1 * (x + w + 1) + strideA2 * (y + h + 2) + c];
											vec_146 = _mm512_set1_ps(scal_45);
											vec_147 = _mm512_loadu_ps(&params[strideW1 * (w + 1) + strideW2 * (h + 2) + strideW3 * c + f]);
											vec_145 = _mm512_fmadd_ps(vec_146, vec_147, mem_vec_5026);
											mem_vec_5026 = vec_145;
											vec_149 = _mm512_loadu_ps(&params[strideW1 * (w + 1) + strideW2 * (h + 2) + strideW3 * c + f + 16]);
											vec_148 = _mm512_fmadd_ps(vec_146, vec_149, mem_vec_5027);
											mem_vec_5027 = vec_148;
											scal_46 = input[strideA1 * (x + w + 1) + strideA2 * (y + 1 + h + 2) + c];
											vec_151 = _mm512_set1_ps(scal_46);
											vec_150 = _mm512_fmadd_ps(vec_151, vec_147, mem_vec_5028);
											mem_vec_5028 = vec_150;
											vec_152 = _mm512_fmadd_ps(vec_151, vec_149, mem_vec_5029);
											mem_vec_5029 = vec_152;
											scal_47 = input[strideA1 * (x + w + 1) + strideA2 * (y + 2 + h + 2) + c];
											vec_154 = _mm512_set1_ps(scal_47);
											vec_153 = _mm512_fmadd_ps(vec_154, vec_147, mem_vec_5030);
											mem_vec_5030 = vec_153;
											vec_155 = _mm512_fmadd_ps(vec_154, vec_149, mem_vec_5031);
											mem_vec_5031 = vec_155;
											scal_48 = input[strideA1 * (x + w + 1) + strideA2 * (y + 3 + h + 2) + c];
											vec_157 = _mm512_set1_ps(scal_48);
											vec_156 = _mm512_fmadd_ps(vec_157, vec_147, mem_vec_5032);
											mem_vec_5032 = vec_156;
											vec_158 = _mm512_fmadd_ps(vec_157, vec_149, mem_vec_5033);
											mem_vec_5033 = vec_158;
											scal_49 = input[strideA1 * (x + w + 1) + strideA2 * (y + 4 + h + 2) + c];
											vec_160 = _mm512_set1_ps(scal_49);
											vec_159 = _mm512_fmadd_ps(vec_160, vec_147, mem_vec_5034);
											mem_vec_5034 = vec_159;
											vec_161 = _mm512_fmadd_ps(vec_160, vec_149, mem_vec_5035);
											mem_vec_5035 = vec_161;
											scal_50 = input[strideA1 * (x + w + 1) + strideA2 * (y + 5 + h + 2) + c];
											vec_163 = _mm512_set1_ps(scal_50);
											vec_162 = _mm512_fmadd_ps(vec_163, vec_147, mem_vec_5036);
											mem_vec_5036 = vec_162;
											vec_164 = _mm512_fmadd_ps(vec_163, vec_149, mem_vec_5037);
											mem_vec_5037 = vec_164;
											scal_51 = input[strideA1 * (x + w + 1) + strideA2 * (y + 6 + h + 2) + c];
											vec_166 = _mm512_set1_ps(scal_51);
											vec_165 = _mm512_fmadd_ps(vec_166, vec_147, mem_vec_5038);
											mem_vec_5038 = vec_165;
											vec_167 = _mm512_fmadd_ps(vec_166, vec_149, mem_vec_5039);
											mem_vec_5039 = vec_167;
											scal_52 = input[strideA1 * (x + w + 1) + strideA2 * (y + 7 + h + 2) + c];
											vec_169 = _mm512_set1_ps(scal_52);
											vec_168 = _mm512_fmadd_ps(vec_169, vec_147, mem_vec_5040);
											mem_vec_5040 = vec_168;
											vec_170 = _mm512_fmadd_ps(vec_169, vec_149, mem_vec_5041);
											mem_vec_5041 = vec_170;
											scal_53 = input[strideA1 * (x + w + 1) + strideA2 * (y + 8 + h + 2) + c];
											vec_172 = _mm512_set1_ps(scal_53);
											vec_171 = _mm512_fmadd_ps(vec_172, vec_147, mem_vec_5042);
											mem_vec_5042 = vec_171;
											vec_173 = _mm512_fmadd_ps(vec_172, vec_149, mem_vec_5043);
											mem_vec_5043 = vec_173;
											scal_54 = input[strideA1 * (x + w + 2) + strideA2 * (y + h) + c];
											vec_175 = _mm512_set1_ps(scal_54);
											vec_176 = _mm512_loadu_ps(&params[strideW1 * (w + 2) + strideW2 * h + strideW3 * c + f]);
											vec_174 = _mm512_fmadd_ps(vec_175, vec_176, mem_vec_5026);
											mem_vec_5026 = vec_174;
											vec_178 = _mm512_loadu_ps(&params[strideW1 * (w + 2) + strideW2 * h + strideW3 * c + f + 16]);
											vec_177 = _mm512_fmadd_ps(vec_175, vec_178, mem_vec_5027);
											mem_vec_5027 = vec_177;
											scal_55 = input[strideA1 * (x + w + 2) + strideA2 * (y + 1 + h) + c];
											vec_180 = _mm512_set1_ps(scal_55);
											vec_179 = _mm512_fmadd_ps(vec_180, vec_176, mem_vec_5028);
											mem_vec_5028 = vec_179;
											vec_181 = _mm512_fmadd_ps(vec_180, vec_178, mem_vec_5029);
											mem_vec_5029 = vec_181;
											scal_56 = input[strideA1 * (x + w + 2) + strideA2 * (y + 2 + h) + c];
											vec_183 = _mm512_set1_ps(scal_56);
											vec_182 = _mm512_fmadd_ps(vec_183, vec_176, mem_vec_5030);
											mem_vec_5030 = vec_182;
											vec_184 = _mm512_fmadd_ps(vec_183, vec_178, mem_vec_5031);
											mem_vec_5031 = vec_184;
											scal_57 = input[strideA1 * (x + w + 2) + strideA2 * (y + 3 + h) + c];
											vec_186 = _mm512_set1_ps(scal_57);
											vec_185 = _mm512_fmadd_ps(vec_186, vec_176, mem_vec_5032);
											mem_vec_5032 = vec_185;
											vec_187 = _mm512_fmadd_ps(vec_186, vec_178, mem_vec_5033);
											mem_vec_5033 = vec_187;
											scal_58 = input[strideA1 * (x + w + 2) + strideA2 * (y + 4 + h) + c];
											vec_189 = _mm512_set1_ps(scal_58);
											vec_188 = _mm512_fmadd_ps(vec_189, vec_176, mem_vec_5034);
											mem_vec_5034 = vec_188;
											vec_190 = _mm512_fmadd_ps(vec_189, vec_178, mem_vec_5035);
											mem_vec_5035 = vec_190;
											scal_59 = input[strideA1 * (x + w + 2) + strideA2 * (y + 5 + h) + c];
											vec_192 = _mm512_set1_ps(scal_59);
											vec_191 = _mm512_fmadd_ps(vec_192, vec_176, mem_vec_5036);
											mem_vec_5036 = vec_191;
											vec_193 = _mm512_fmadd_ps(vec_192, vec_178, mem_vec_5037);
											mem_vec_5037 = vec_193;
											scal_60 = input[strideA1 * (x + w + 2) + strideA2 * (y + 6 + h) + c];
											vec_195 = _mm512_set1_ps(scal_60);
											vec_194 = _mm512_fmadd_ps(vec_195, vec_176, mem_vec_5038);
											mem_vec_5038 = vec_194;
											vec_196 = _mm512_fmadd_ps(vec_195, vec_178, mem_vec_5039);
											mem_vec_5039 = vec_196;
											scal_61 = input[strideA1 * (x + w + 2) + strideA2 * (y + 7 + h) + c];
											vec_198 = _mm512_set1_ps(scal_61);
											vec_197 = _mm512_fmadd_ps(vec_198, vec_176, mem_vec_5040);
											mem_vec_5040 = vec_197;
											vec_199 = _mm512_fmadd_ps(vec_198, vec_178, mem_vec_5041);
											mem_vec_5041 = vec_199;
											scal_62 = input[strideA1 * (x + w + 2) + strideA2 * (y + 8 + h) + c];
											vec_201 = _mm512_set1_ps(scal_62);
											vec_200 = _mm512_fmadd_ps(vec_201, vec_176, mem_vec_5042);
											mem_vec_5042 = vec_200;
											vec_202 = _mm512_fmadd_ps(vec_201, vec_178, mem_vec_5043);
											mem_vec_5043 = vec_202;
											scal_63 = input[strideA1 * (x + w + 2) + strideA2 * (y + h + 1) + c];
											vec_204 = _mm512_set1_ps(scal_63);
											vec_205 = _mm512_loadu_ps(&params[strideW1 * (w + 2) + strideW2 * (h + 1) + strideW3 * c + f]);
											vec_203 = _mm512_fmadd_ps(vec_204, vec_205, mem_vec_5026);
											mem_vec_5026 = vec_203;
											vec_207 = _mm512_loadu_ps(&params[strideW1 * (w + 2) + strideW2 * (h + 1) + strideW3 * c + f + 16]);
											vec_206 = _mm512_fmadd_ps(vec_204, vec_207, mem_vec_5027);
											mem_vec_5027 = vec_206;
											scal_64 = input[strideA1 * (x + w + 2) + strideA2 * (y + 1 + h + 1) + c];
											vec_209 = _mm512_set1_ps(scal_64);
											vec_208 = _mm512_fmadd_ps(vec_209, vec_205, mem_vec_5028);
											mem_vec_5028 = vec_208;
											vec_210 = _mm512_fmadd_ps(vec_209, vec_207, mem_vec_5029);
											mem_vec_5029 = vec_210;
											scal_65 = input[strideA1 * (x + w + 2) + strideA2 * (y + 2 + h + 1) + c];
											vec_212 = _mm512_set1_ps(scal_65);
											vec_211 = _mm512_fmadd_ps(vec_212, vec_205, mem_vec_5030);
											mem_vec_5030 = vec_211;
											vec_213 = _mm512_fmadd_ps(vec_212, vec_207, mem_vec_5031);
											mem_vec_5031 = vec_213;
											scal_66 = input[strideA1 * (x + w + 2) + strideA2 * (y + 3 + h + 1) + c];
											vec_215 = _mm512_set1_ps(scal_66);
											vec_214 = _mm512_fmadd_ps(vec_215, vec_205, mem_vec_5032);
											mem_vec_5032 = vec_214;
											vec_216 = _mm512_fmadd_ps(vec_215, vec_207, mem_vec_5033);
											mem_vec_5033 = vec_216;
											scal_67 = input[strideA1 * (x + w + 2) + strideA2 * (y + 4 + h + 1) + c];
											vec_218 = _mm512_set1_ps(scal_67);
											vec_217 = _mm512_fmadd_ps(vec_218, vec_205, mem_vec_5034);
											mem_vec_5034 = vec_217;
											vec_219 = _mm512_fmadd_ps(vec_218, vec_207, mem_vec_5035);
											mem_vec_5035 = vec_219;
											scal_68 = input[strideA1 * (x + w + 2) + strideA2 * (y + 5 + h + 1) + c];
											vec_221 = _mm512_set1_ps(scal_68);
											vec_220 = _mm512_fmadd_ps(vec_221, vec_205, mem_vec_5036);
											mem_vec_5036 = vec_220;
											vec_222 = _mm512_fmadd_ps(vec_221, vec_207, mem_vec_5037);
											mem_vec_5037 = vec_222;
											scal_69 = input[strideA1 * (x + w + 2) + strideA2 * (y + 6 + h + 1) + c];
											vec_224 = _mm512_set1_ps(scal_69);
											vec_223 = _mm512_fmadd_ps(vec_224, vec_205, mem_vec_5038);
											mem_vec_5038 = vec_223;
											vec_225 = _mm512_fmadd_ps(vec_224, vec_207, mem_vec_5039);
											mem_vec_5039 = vec_225;
											scal_70 = input[strideA1 * (x + w + 2) + strideA2 * (y + 7 + h + 1) + c];
											vec_227 = _mm512_set1_ps(scal_70);
											vec_226 = _mm512_fmadd_ps(vec_227, vec_205, mem_vec_5040);
											mem_vec_5040 = vec_226;
											vec_228 = _mm512_fmadd_ps(vec_227, vec_207, mem_vec_5041);
											mem_vec_5041 = vec_228;
											scal_71 = input[strideA1 * (x + w + 2) + strideA2 * (y + 8 + h + 1) + c];
											vec_230 = _mm512_set1_ps(scal_71);
											vec_229 = _mm512_fmadd_ps(vec_230, vec_205, mem_vec_5042);
											mem_vec_5042 = vec_229;
											vec_231 = _mm512_fmadd_ps(vec_230, vec_207, mem_vec_5043);
											mem_vec_5043 = vec_231;
											scal_72 = input[strideA1 * (x + w + 2) + strideA2 * (y + h + 2) + c];
											vec_233 = _mm512_set1_ps(scal_72);
											vec_234 = _mm512_loadu_ps(&params[strideW1 * (w + 2) + strideW2 * (h + 2) + strideW3 * c + f]);
											vec_232 = _mm512_fmadd_ps(vec_233, vec_234, mem_vec_5026);
											mem_vec_5026 = vec_232;
											vec_236 = _mm512_loadu_ps(&params[strideW1 * (w + 2) + strideW2 * (h + 2) + strideW3 * c + f + 16]);
											vec_235 = _mm512_fmadd_ps(vec_233, vec_236, mem_vec_5027);
											mem_vec_5027 = vec_235;
											scal_73 = input[strideA1 * (x + w + 2) + strideA2 * (y + 1 + h + 2) + c];
											vec_238 = _mm512_set1_ps(scal_73);
											vec_237 = _mm512_fmadd_ps(vec_238, vec_234, mem_vec_5028);
											mem_vec_5028 = vec_237;
											vec_239 = _mm512_fmadd_ps(vec_238, vec_236, mem_vec_5029);
											mem_vec_5029 = vec_239;
											scal_74 = input[strideA1 * (x + w + 2) + strideA2 * (y + 2 + h + 2) + c];
											vec_241 = _mm512_set1_ps(scal_74);
											vec_240 = _mm512_fmadd_ps(vec_241, vec_234, mem_vec_5030);
											mem_vec_5030 = vec_240;
											vec_242 = _mm512_fmadd_ps(vec_241, vec_236, mem_vec_5031);
											mem_vec_5031 = vec_242;
											scal_75 = input[strideA1 * (x + w + 2) + strideA2 * (y + 3 + h + 2) + c];
											vec_244 = _mm512_set1_ps(scal_75);
											vec_243 = _mm512_fmadd_ps(vec_244, vec_234, mem_vec_5032);
											mem_vec_5032 = vec_243;
											vec_245 = _mm512_fmadd_ps(vec_244, vec_236, mem_vec_5033);
											mem_vec_5033 = vec_245;
											scal_76 = input[strideA1 * (x + w + 2) + strideA2 * (y + 4 + h + 2) + c];
											vec_247 = _mm512_set1_ps(scal_76);
											vec_246 = _mm512_fmadd_ps(vec_247, vec_234, mem_vec_5034);
											mem_vec_5034 = vec_246;
											vec_248 = _mm512_fmadd_ps(vec_247, vec_236, mem_vec_5035);
											mem_vec_5035 = vec_248;
											scal_77 = input[strideA1 * (x + w + 2) + strideA2 * (y + 5 + h + 2) + c];
											vec_250 = _mm512_set1_ps(scal_77);
											vec_249 = _mm512_fmadd_ps(vec_250, vec_234, mem_vec_5036);
											mem_vec_5036 = vec_249;
											vec_251 = _mm512_fmadd_ps(vec_250, vec_236, mem_vec_5037);
											mem_vec_5037 = vec_251;
											scal_78 = input[strideA1 * (x + w + 2) + strideA2 * (y + 6 + h + 2) + c];
											vec_253 = _mm512_set1_ps(scal_78);
											vec_252 = _mm512_fmadd_ps(vec_253, vec_234, mem_vec_5038);
											mem_vec_5038 = vec_252;
											vec_254 = _mm512_fmadd_ps(vec_253, vec_236, mem_vec_5039);
											mem_vec_5039 = vec_254;
											scal_79 = input[strideA1 * (x + w + 2) + strideA2 * (y + 7 + h + 2) + c];
											vec_256 = _mm512_set1_ps(scal_79);
											vec_255 = _mm512_fmadd_ps(vec_256, vec_234, mem_vec_5040);
											mem_vec_5040 = vec_255;
											vec_257 = _mm512_fmadd_ps(vec_256, vec_236, mem_vec_5041);
											mem_vec_5041 = vec_257;
											scal_80 = input[strideA1 * (x + w + 2) + strideA2 * (y + 8 + h + 2) + c];
											vec_259 = _mm512_set1_ps(scal_80);
											vec_258 = _mm512_fmadd_ps(vec_259, vec_234, mem_vec_5042);
											mem_vec_5042 = vec_258;
											vec_260 = _mm512_fmadd_ps(vec_259, vec_236, mem_vec_5043);
											mem_vec_5043 = vec_260;
										}
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * y + f], mem_vec_5026);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * y + f + 16], mem_vec_5027);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 1) + f], mem_vec_5028);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 1) + f + 16], mem_vec_5029);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 2) + f], mem_vec_5030);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 2) + f + 16], mem_vec_5031);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 3) + f], mem_vec_5032);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 3) + f + 16], mem_vec_5033);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 4) + f], mem_vec_5034);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 4) + f + 16], mem_vec_5035);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 5) + f], mem_vec_5036);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 5) + f + 16], mem_vec_5037);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 6) + f], mem_vec_5038);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 6) + f + 16], mem_vec_5039);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 7) + f], mem_vec_5040);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 7) + f + 16], mem_vec_5041);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 8) + f], mem_vec_5042);
									_mm512_storeu_ps(&output[strideO1 * x + strideO2 * (y + 8) + f + 16], mem_vec_5043);
							}
}
