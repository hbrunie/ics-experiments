#include "../gen_conv.h"
#include "../reorder_avx2.h"

void gen_conv(M_TYPE * const  __restrict__ output,
M_TYPE const * const __restrict__ input, M_TYPE const * const __restrict__ params,
    IND_TYPE X, IND_TYPE W,
    IND_TYPE Y, IND_TYPE H,
	IND_TYPE C, IND_TYPE F) {
/*
[V f; U (4, f); U (7, y); T (16, c); Hoist_vars [c]; T (3, h); T (28, x);
  T (3, w); T (4, c); T (2, x); T (8, y); T (1, x)]
*/
IND_TYPE c, cp_0, c96_p_0, cp_1, c96, h, hp_0, w, wp_0, x, xp_0, x114_p_0, x115_p_0, xp_1, x114_p_1, xp_2, x114, x115, y, yp_0;

assert((Y == 56));
assert((X == 56));
assert((H == 3));
assert((W == 3));
assert((C == 64));
assert((F == 64));
IND_TYPE y78 = 0;
IND_TYPE x116 = 0;
IND_TYPE h47 = 0;
IND_TYPE w45 = 0;
IND_TYPE c97 = 0;
IND_TYPE f = 0;
float scal_0 ,scal_1 ,scal_2 ,scal_3 ,scal_4 ,scal_5 ,scal_6;
__m512 mem_vec_604 ,mem_vec_605 ,mem_vec_606 ,mem_vec_607 ,mem_vec_608 ,mem_vec_609 ,mem_vec_610 ,mem_vec_611 ,mem_vec_612 ,mem_vec_613 ,mem_vec_614 ,mem_vec_615 ,mem_vec_616 ,mem_vec_617 ,mem_vec_618 ,mem_vec_619 ,mem_vec_620 ,mem_vec_621 ,mem_vec_622 ,mem_vec_623 ,mem_vec_624 ,mem_vec_625 ,mem_vec_626 ,mem_vec_627 ,mem_vec_628 ,mem_vec_629 ,mem_vec_630 ,mem_vec_631 ,vec_0 ,vec_1 ,vec_10 ,vec_11 ,vec_12 ,vec_13 ,vec_14 ,vec_15 ,vec_16 ,vec_17 ,vec_18 ,vec_19 ,vec_2 ,vec_20 ,vec_21 ,vec_22 ,vec_23 ,vec_24 ,vec_25 ,vec_26 ,vec_27 ,vec_28 ,vec_29 ,vec_3 ,vec_30 ,vec_31 ,vec_32 ,vec_33 ,vec_34 ,vec_35 ,vec_36 ,vec_37 ,vec_38 ,vec_4 ,vec_5 ,vec_6 ,vec_7 ,vec_8 ,vec_9;
// y = 56, x = 56, h = 3, w = 3, c = 64, f = 64
// T (x, 1) (56 / 56)
for (x115 = x116, x115_p_0 = 0;
	x115 < x116 + 56;
	x115 += 56, x115_p_0 += 56){
	// y = 56, x = 56, h = 3, w = 3, c = 64, f = 64
	// T (y, 8) (56 / 7)
	for (y = y78, yp_0 = 0;
		y < y78 + 56;
		y += 7, yp_0 += 7){
		// y = 7, x = 56, h = 3, w = 3, c = 64, f = 64
		// T (x, 2) (56 / 28)
		for (x114 = x115, x114_p_1 = x115_p_0, x114_p_0 = 0;
			x114 < x115 + 56;
			x114 += 28, x114_p_1 += 28, x114_p_0 += 28){
			// y = 7, x = 28, h = 3, w = 3, c = 64, f = 64
			// T (c, 4) (64 / 16)
			for (c96 = c97, c96_p_0 = 0;
				c96 < c97 + 64;
				c96 += 16, c96_p_0 += 16){
				// y = 7, x = 28, h = 3, w = 3, c = 16, f = 64
				// T (w, 3) (3 / 1)
				for (w = w45, wp_0 = 0;
					w < w45 + 3;
					w += 1, wp_0 += 1){
					// y = 7, x = 28, h = 3, w = 1, c = 16, f = 64
					// T (x, 28) (28 / 1)
					for (x = x114, xp_2 = x114_p_1, xp_1 = x114_p_0, xp_0 = 0;
						x < x114 + 28;
						x += 1, xp_2 += 1, xp_1 += 1, xp_0 += 1){
						// y = 7, x = 1, h = 3, w = 1, c = 16, f = 64
						// T (h, 3) (3 / 1)
						for (h = h47, hp_0 = 0;
							h < h47 + 3;
							h += 1, hp_0 += 1){
									mem_vec_604 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f]);
									mem_vec_605 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 16]);
									mem_vec_606 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 32]);
									mem_vec_607 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 48]);
									mem_vec_608 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
									mem_vec_609 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16]);
									mem_vec_610 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 32]);
									mem_vec_611 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 48]);
									mem_vec_612 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
									mem_vec_613 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16]);
									mem_vec_614 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 32]);
									mem_vec_615 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 48]);
									mem_vec_616 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
									mem_vec_617 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16]);
									mem_vec_618 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 32]);
									mem_vec_619 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 48]);
									mem_vec_620 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f]);
									mem_vec_621 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16]);
									mem_vec_622 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 32]);
									mem_vec_623 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 48]);
									mem_vec_624 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f]);
									mem_vec_625 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16]);
									mem_vec_626 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f + 32]);
									mem_vec_627 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f + 48]);
									mem_vec_628 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f]);
									mem_vec_629 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16]);
									mem_vec_630 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f + 32]);
									mem_vec_631 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f + 48]);
									// y = 7, x = 1, h = 1, w = 1, c = 16, f = 64
									// T (c, 16) (16 / 1)
									for (c = c96, cp_1 = c96_p_0, cp_0 = 0;
										c < c96 + 16;
										c += 1, cp_1 += 1, cp_0 += 1){
										scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
										vec_1 = _mm512_set1_ps(scal_0);
										vec_2 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

										vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_604);
										mem_vec_604 = vec_0;

										vec_4 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 16]);

										vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_605);
										mem_vec_605 = vec_3;

										vec_6 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 32]);

										vec_5 = _mm512_fmadd_ps(vec_1, vec_6, mem_vec_606);
										mem_vec_606 = vec_5;

										vec_8 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 48]);

										vec_7 = _mm512_fmadd_ps(vec_1, vec_8, mem_vec_607);
										mem_vec_607 = vec_7;
										scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
										vec_10 = _mm512_set1_ps(scal_1);


										vec_9 = _mm512_fmadd_ps(vec_10, vec_2, mem_vec_608);
										mem_vec_608 = vec_9;



										vec_11 = _mm512_fmadd_ps(vec_10, vec_4, mem_vec_609);
										mem_vec_609 = vec_11;



										vec_12 = _mm512_fmadd_ps(vec_10, vec_6, mem_vec_610);
										mem_vec_610 = vec_12;



										vec_13 = _mm512_fmadd_ps(vec_10, vec_8, mem_vec_611);
										mem_vec_611 = vec_13;
										scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
										vec_15 = _mm512_set1_ps(scal_2);


										vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_612);
										mem_vec_612 = vec_14;



										vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_613);
										mem_vec_613 = vec_16;



										vec_17 = _mm512_fmadd_ps(vec_15, vec_6, mem_vec_614);
										mem_vec_614 = vec_17;



										vec_18 = _mm512_fmadd_ps(vec_15, vec_8, mem_vec_615);
										mem_vec_615 = vec_18;
										scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
										vec_20 = _mm512_set1_ps(scal_3);


										vec_19 = _mm512_fmadd_ps(vec_20, vec_2, mem_vec_616);
										mem_vec_616 = vec_19;



										vec_21 = _mm512_fmadd_ps(vec_20, vec_4, mem_vec_617);
										mem_vec_617 = vec_21;



										vec_22 = _mm512_fmadd_ps(vec_20, vec_6, mem_vec_618);
										mem_vec_618 = vec_22;



										vec_23 = _mm512_fmadd_ps(vec_20, vec_8, mem_vec_619);
										mem_vec_619 = vec_23;
										scal_4 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h) + c];
										vec_25 = _mm512_set1_ps(scal_4);


										vec_24 = _mm512_fmadd_ps(vec_25, vec_2, mem_vec_620);
										mem_vec_620 = vec_24;



										vec_26 = _mm512_fmadd_ps(vec_25, vec_4, mem_vec_621);
										mem_vec_621 = vec_26;



										vec_27 = _mm512_fmadd_ps(vec_25, vec_6, mem_vec_622);
										mem_vec_622 = vec_27;



										vec_28 = _mm512_fmadd_ps(vec_25, vec_8, mem_vec_623);
										mem_vec_623 = vec_28;
										scal_5 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h) + c];
										vec_30 = _mm512_set1_ps(scal_5);


										vec_29 = _mm512_fmadd_ps(vec_30, vec_2, mem_vec_624);
										mem_vec_624 = vec_29;



										vec_31 = _mm512_fmadd_ps(vec_30, vec_4, mem_vec_625);
										mem_vec_625 = vec_31;



										vec_32 = _mm512_fmadd_ps(vec_30, vec_6, mem_vec_626);
										mem_vec_626 = vec_32;



										vec_33 = _mm512_fmadd_ps(vec_30, vec_8, mem_vec_627);
										mem_vec_627 = vec_33;
										scal_6 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h) + c];
										vec_35 = _mm512_set1_ps(scal_6);


										vec_34 = _mm512_fmadd_ps(vec_35, vec_2, mem_vec_628);
										mem_vec_628 = vec_34;



										vec_36 = _mm512_fmadd_ps(vec_35, vec_4, mem_vec_629);
										mem_vec_629 = vec_36;



										vec_37 = _mm512_fmadd_ps(vec_35, vec_6, mem_vec_630);
										mem_vec_630 = vec_37;



										vec_38 = _mm512_fmadd_ps(vec_35, vec_8, mem_vec_631);
										mem_vec_631 = vec_38;
									}
								_mm512_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_604);
								_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 16], mem_vec_605);
								_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 32], mem_vec_606);
								_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 48], mem_vec_607);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_608);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16], mem_vec_609);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 32], mem_vec_610);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 48], mem_vec_611);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_612);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16], mem_vec_613);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 32], mem_vec_614);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 48], mem_vec_615);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_616);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16], mem_vec_617);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 32], mem_vec_618);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 48], mem_vec_619);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f], mem_vec_620);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16], mem_vec_621);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 32], mem_vec_622);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 48], mem_vec_623);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f], mem_vec_624);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16], mem_vec_625);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f + 32], mem_vec_626);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f + 48], mem_vec_627);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f], mem_vec_628);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16], mem_vec_629);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f + 32], mem_vec_630);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f + 48], mem_vec_631);
						}
					}
				}
			}
		}
	}
}


}