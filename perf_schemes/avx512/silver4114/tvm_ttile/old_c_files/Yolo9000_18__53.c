#include "../gen_conv.h"
#include "../reorder_avx2.h"

void gen_conv(M_TYPE * const  __restrict__ output,
M_TYPE const * const __restrict__ input, M_TYPE const * const __restrict__ params,
    IND_TYPE X, IND_TYPE W,
    IND_TYPE Y, IND_TYPE H,
	IND_TYPE C, IND_TYPE F) {
/*
[V f; U (1, f); ULambda y; U (3, h); U (3, w); T (64, c); Hoist_vars [c];
  T (17, x); T (1, c);
  Lambda_apply y [((Iter 1), (Arg 5)); ((Iter 1), (Arg 12))]; T (64, f);
  T (8, c); T (1, f)]
*/
IND_TYPE c, cp_0, c392_p_0, c393_p_0, cp_1, c392_p_1, cp_2, c392, c393, f, fp_0, f294_p_0, fp_1, f294, x, xp_0, y, yp_0;

assert((Y == 17));
assert((X == 17));
assert((H == 3));
assert((W == 3));
assert((C == 512));
assert((F == 1024));
IND_TYPE y196 = 0;
IND_TYPE x196 = 0;
IND_TYPE h = 0;
IND_TYPE w = 0;
IND_TYPE c394 = 0;
IND_TYPE f295 = 0;
float scal_0 ,scal_1 ,scal_10 ,scal_100 ,scal_101 ,scal_102 ,scal_103 ,scal_104 ,scal_105 ,scal_106 ,scal_107 ,scal_11 ,scal_12 ,scal_13 ,scal_14 ,scal_15 ,scal_16 ,scal_17 ,scal_18 ,scal_19 ,scal_2 ,scal_20 ,scal_21 ,scal_22 ,scal_23 ,scal_24 ,scal_25 ,scal_26 ,scal_27 ,scal_28 ,scal_29 ,scal_3 ,scal_30 ,scal_31 ,scal_32 ,scal_33 ,scal_34 ,scal_35 ,scal_36 ,scal_37 ,scal_38 ,scal_39 ,scal_4 ,scal_40 ,scal_41 ,scal_42 ,scal_43 ,scal_44 ,scal_45 ,scal_46 ,scal_47 ,scal_48 ,scal_49 ,scal_5 ,scal_50 ,scal_51 ,scal_52 ,scal_53 ,scal_54 ,scal_55 ,scal_56 ,scal_57 ,scal_58 ,scal_59 ,scal_6 ,scal_60 ,scal_61 ,scal_62 ,scal_63 ,scal_64 ,scal_65 ,scal_66 ,scal_67 ,scal_68 ,scal_69 ,scal_7 ,scal_70 ,scal_71 ,scal_72 ,scal_73 ,scal_74 ,scal_75 ,scal_76 ,scal_77 ,scal_78 ,scal_79 ,scal_8 ,scal_80 ,scal_81 ,scal_82 ,scal_83 ,scal_84 ,scal_85 ,scal_86 ,scal_87 ,scal_88 ,scal_89 ,scal_9 ,scal_90 ,scal_91 ,scal_92 ,scal_93 ,scal_94 ,scal_95 ,scal_96 ,scal_97 ,scal_98 ,scal_99;
__m512 mem_vec_1890 ,mem_vec_1891 ,mem_vec_1892 ,mem_vec_1893 ,mem_vec_1894 ,mem_vec_1895 ,mem_vec_1896 ,mem_vec_1897 ,mem_vec_1898 ,mem_vec_1899 ,mem_vec_1900 ,mem_vec_1901 ,mem_vec_1902 ,mem_vec_1903 ,mem_vec_1904 ,mem_vec_1905 ,mem_vec_1906 ,vec_0 ,vec_1 ,vec_10 ,vec_100 ,vec_101 ,vec_102 ,vec_103 ,vec_104 ,vec_105 ,vec_106 ,vec_107 ,vec_108 ,vec_109 ,vec_11 ,vec_110 ,vec_111 ,vec_112 ,vec_113 ,vec_114 ,vec_115 ,vec_116 ,vec_117 ,vec_118 ,vec_119 ,vec_12 ,vec_120 ,vec_121 ,vec_122 ,vec_123 ,vec_124 ,vec_125 ,vec_126 ,vec_127 ,vec_128 ,vec_129 ,vec_13 ,vec_130 ,vec_131 ,vec_132 ,vec_133 ,vec_134 ,vec_135 ,vec_136 ,vec_137 ,vec_138 ,vec_139 ,vec_14 ,vec_140 ,vec_141 ,vec_142 ,vec_143 ,vec_144 ,vec_145 ,vec_146 ,vec_147 ,vec_148 ,vec_149 ,vec_15 ,vec_150 ,vec_151 ,vec_152 ,vec_153 ,vec_154 ,vec_155 ,vec_156 ,vec_157 ,vec_158 ,vec_159 ,vec_16 ,vec_160 ,vec_161 ,vec_162 ,vec_163 ,vec_164 ,vec_165 ,vec_166 ,vec_167 ,vec_168 ,vec_169 ,vec_17 ,vec_170 ,vec_171 ,vec_172 ,vec_173 ,vec_174 ,vec_175 ,vec_176 ,vec_177 ,vec_178 ,vec_179 ,vec_18 ,vec_180 ,vec_181 ,vec_182 ,vec_183 ,vec_184 ,vec_185 ,vec_186 ,vec_187 ,vec_188 ,vec_189 ,vec_19 ,vec_190 ,vec_191 ,vec_192 ,vec_193 ,vec_194 ,vec_195 ,vec_196 ,vec_197 ,vec_198 ,vec_199 ,vec_2 ,vec_20 ,vec_200 ,vec_201 ,vec_202 ,vec_203 ,vec_204 ,vec_205 ,vec_206 ,vec_207 ,vec_208 ,vec_209 ,vec_21 ,vec_210 ,vec_211 ,vec_212 ,vec_213 ,vec_214 ,vec_215 ,vec_216 ,vec_217 ,vec_218 ,vec_219 ,vec_22 ,vec_220 ,vec_221 ,vec_222 ,vec_223 ,vec_224 ,vec_23 ,vec_24 ,vec_25 ,vec_26 ,vec_27 ,vec_28 ,vec_29 ,vec_3 ,vec_30 ,vec_31 ,vec_32 ,vec_33 ,vec_34 ,vec_35 ,vec_36 ,vec_37 ,vec_38 ,vec_39 ,vec_4 ,vec_40 ,vec_41 ,vec_42 ,vec_43 ,vec_44 ,vec_45 ,vec_46 ,vec_47 ,vec_48 ,vec_49 ,vec_5 ,vec_50 ,vec_51 ,vec_52 ,vec_53 ,vec_54 ,vec_55 ,vec_56 ,vec_57 ,vec_58 ,vec_59 ,vec_6 ,vec_60 ,vec_61 ,vec_62 ,vec_63 ,vec_64 ,vec_65 ,vec_66 ,vec_67 ,vec_68 ,vec_69 ,vec_7 ,vec_70 ,vec_71 ,vec_72 ,vec_73 ,vec_74 ,vec_75 ,vec_76 ,vec_77 ,vec_78 ,vec_79 ,vec_8 ,vec_80 ,vec_81 ,vec_82 ,vec_83 ,vec_84 ,vec_85 ,vec_86 ,vec_87 ,vec_88 ,vec_89 ,vec_9 ,vec_90 ,vec_91 ,vec_92 ,vec_93 ,vec_94 ,vec_95 ,vec_96 ,vec_97 ,vec_98 ,vec_99;
// y = 17, x = 17, h = 3, w = 3, c = 512, f = 1024
// T (f, 1) (1024 / 1024)
for (f294 = f295, f294_p_0 = 0;
	f294 < f295 + 1024;
	f294 += 1024, f294_p_0 += 1024){
	// y = 17, x = 17, h = 3, w = 3, c = 512, f = 1024
	// T (c, 8) (512 / 64)
	for (c393 = c394, c393_p_0 = 0;
		c393 < c394 + 512;
		c393 += 64, c393_p_0 += 64){
		// y = 17, x = 17, h = 3, w = 3, c = 64, f = 1024
		// T (f, 64) (1024 / 16)
		for (f = f294, fp_1 = f294_p_0, fp_0 = 0;
			f < f294 + 1024;
			f += 16, fp_1 += 16, fp_0 += 16){
				for (y = y196, yp_0 = 0;
					y < y196 + 5;
					y += 5, yp_0 += 5){
					// y = ph_y, x = 17, h = 3, w = 3, c = 64, f = 16
					// T (c, 1) (64 / 64)
					for (c392 = c393, c392_p_1 = c393_p_0, c392_p_0 = 0;
						c392 < c393 + 64;
						c392 += 64, c392_p_1 += 64, c392_p_0 += 64){
						// y = ph_y, x = 17, h = 3, w = 3, c = 64, f = 16
						// T (x, 17) (17 / 1)
						for (x = x196, xp_0 = 0;
							x < x196 + 17;
							x += 1, xp_0 += 1){
									mem_vec_1890 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f]);
									mem_vec_1891 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
									mem_vec_1892 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
									mem_vec_1893 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
									mem_vec_1894 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f]);
									// y = ph_y, x = 1, h = 3, w = 3, c = 64, f = 16
									// T (c, 64) (64 / 1)
									for (c = c392, cp_2 = c392_p_1, cp_1 = c392_p_0, cp_0 = 0;
										c < c392 + 64;
										c += 1, cp_2 += 1, cp_1 += 1, cp_0 += 1){
										scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
										vec_1 = _mm512_set1_ps(scal_0);
										vec_2 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

										vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_1890);
										mem_vec_1890 = vec_0;
										scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
										vec_4 = _mm512_set1_ps(scal_1);


										vec_3 = _mm512_fmadd_ps(vec_4, vec_2, mem_vec_1891);
										mem_vec_1891 = vec_3;
										scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
										vec_6 = _mm512_set1_ps(scal_2);


										vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_1892);
										mem_vec_1892 = vec_5;
										scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
										vec_8 = _mm512_set1_ps(scal_3);


										vec_7 = _mm512_fmadd_ps(vec_8, vec_2, mem_vec_1893);
										mem_vec_1893 = vec_7;
										scal_4 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h) + c];
										vec_10 = _mm512_set1_ps(scal_4);


										vec_9 = _mm512_fmadd_ps(vec_10, vec_2, mem_vec_1894);
										mem_vec_1894 = vec_9;
										scal_5 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h + 1) + c];
										vec_12 = _mm512_set1_ps(scal_5);
										vec_13 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 1) + F * c + f]);

										vec_11 = _mm512_fmadd_ps(vec_12, vec_13, mem_vec_1890);
										mem_vec_1890 = vec_11;
										scal_6 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h + 1) + c];
										vec_15 = _mm512_set1_ps(scal_6);


										vec_14 = _mm512_fmadd_ps(vec_15, vec_13, mem_vec_1891);
										mem_vec_1891 = vec_14;
										scal_7 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h + 1) + c];
										vec_17 = _mm512_set1_ps(scal_7);


										vec_16 = _mm512_fmadd_ps(vec_17, vec_13, mem_vec_1892);
										mem_vec_1892 = vec_16;
										scal_8 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h + 1) + c];
										vec_19 = _mm512_set1_ps(scal_8);


										vec_18 = _mm512_fmadd_ps(vec_19, vec_13, mem_vec_1893);
										mem_vec_1893 = vec_18;
										scal_9 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h + 1) + c];
										vec_21 = _mm512_set1_ps(scal_9);


										vec_20 = _mm512_fmadd_ps(vec_21, vec_13, mem_vec_1894);
										mem_vec_1894 = vec_20;
										scal_10 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h + 2) + c];
										vec_23 = _mm512_set1_ps(scal_10);
										vec_24 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 2) + F * c + f]);

										vec_22 = _mm512_fmadd_ps(vec_23, vec_24, mem_vec_1890);
										mem_vec_1890 = vec_22;
										scal_11 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h + 2) + c];
										vec_26 = _mm512_set1_ps(scal_11);


										vec_25 = _mm512_fmadd_ps(vec_26, vec_24, mem_vec_1891);
										mem_vec_1891 = vec_25;
										scal_12 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h + 2) + c];
										vec_28 = _mm512_set1_ps(scal_12);


										vec_27 = _mm512_fmadd_ps(vec_28, vec_24, mem_vec_1892);
										mem_vec_1892 = vec_27;
										scal_13 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h + 2) + c];
										vec_30 = _mm512_set1_ps(scal_13);


										vec_29 = _mm512_fmadd_ps(vec_30, vec_24, mem_vec_1893);
										mem_vec_1893 = vec_29;
										scal_14 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h + 2) + c];
										vec_32 = _mm512_set1_ps(scal_14);


										vec_31 = _mm512_fmadd_ps(vec_32, vec_24, mem_vec_1894);
										mem_vec_1894 = vec_31;
										scal_15 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + h) + c];
										vec_34 = _mm512_set1_ps(scal_15);
										vec_35 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 1) + (F * C) * h + F * c + f]);

										vec_33 = _mm512_fmadd_ps(vec_34, vec_35, mem_vec_1890);
										mem_vec_1890 = vec_33;
										scal_16 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 1 + h) + c];
										vec_37 = _mm512_set1_ps(scal_16);


										vec_36 = _mm512_fmadd_ps(vec_37, vec_35, mem_vec_1891);
										mem_vec_1891 = vec_36;
										scal_17 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 2 + h) + c];
										vec_39 = _mm512_set1_ps(scal_17);


										vec_38 = _mm512_fmadd_ps(vec_39, vec_35, mem_vec_1892);
										mem_vec_1892 = vec_38;
										scal_18 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 3 + h) + c];
										vec_41 = _mm512_set1_ps(scal_18);


										vec_40 = _mm512_fmadd_ps(vec_41, vec_35, mem_vec_1893);
										mem_vec_1893 = vec_40;
										scal_19 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 4 + h) + c];
										vec_43 = _mm512_set1_ps(scal_19);


										vec_42 = _mm512_fmadd_ps(vec_43, vec_35, mem_vec_1894);
										mem_vec_1894 = vec_42;
										scal_20 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + h + 1) + c];
										vec_45 = _mm512_set1_ps(scal_20);
										vec_46 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 1) + (F * C) * (h + 1) + F * c + f]);

										vec_44 = _mm512_fmadd_ps(vec_45, vec_46, mem_vec_1890);
										mem_vec_1890 = vec_44;
										scal_21 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 1 + h + 1) + c];
										vec_48 = _mm512_set1_ps(scal_21);


										vec_47 = _mm512_fmadd_ps(vec_48, vec_46, mem_vec_1891);
										mem_vec_1891 = vec_47;
										scal_22 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 2 + h + 1) + c];
										vec_50 = _mm512_set1_ps(scal_22);


										vec_49 = _mm512_fmadd_ps(vec_50, vec_46, mem_vec_1892);
										mem_vec_1892 = vec_49;
										scal_23 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 3 + h + 1) + c];
										vec_52 = _mm512_set1_ps(scal_23);


										vec_51 = _mm512_fmadd_ps(vec_52, vec_46, mem_vec_1893);
										mem_vec_1893 = vec_51;
										scal_24 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 4 + h + 1) + c];
										vec_54 = _mm512_set1_ps(scal_24);


										vec_53 = _mm512_fmadd_ps(vec_54, vec_46, mem_vec_1894);
										mem_vec_1894 = vec_53;
										scal_25 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + h + 2) + c];
										vec_56 = _mm512_set1_ps(scal_25);
										vec_57 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 1) + (F * C) * (h + 2) + F * c + f]);

										vec_55 = _mm512_fmadd_ps(vec_56, vec_57, mem_vec_1890);
										mem_vec_1890 = vec_55;
										scal_26 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 1 + h + 2) + c];
										vec_59 = _mm512_set1_ps(scal_26);


										vec_58 = _mm512_fmadd_ps(vec_59, vec_57, mem_vec_1891);
										mem_vec_1891 = vec_58;
										scal_27 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 2 + h + 2) + c];
										vec_61 = _mm512_set1_ps(scal_27);


										vec_60 = _mm512_fmadd_ps(vec_61, vec_57, mem_vec_1892);
										mem_vec_1892 = vec_60;
										scal_28 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 3 + h + 2) + c];
										vec_63 = _mm512_set1_ps(scal_28);


										vec_62 = _mm512_fmadd_ps(vec_63, vec_57, mem_vec_1893);
										mem_vec_1893 = vec_62;
										scal_29 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 4 + h + 2) + c];
										vec_65 = _mm512_set1_ps(scal_29);


										vec_64 = _mm512_fmadd_ps(vec_65, vec_57, mem_vec_1894);
										mem_vec_1894 = vec_64;
										scal_30 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + h) + c];
										vec_67 = _mm512_set1_ps(scal_30);
										vec_68 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 2) + (F * C) * h + F * c + f]);

										vec_66 = _mm512_fmadd_ps(vec_67, vec_68, mem_vec_1890);
										mem_vec_1890 = vec_66;
										scal_31 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 1 + h) + c];
										vec_70 = _mm512_set1_ps(scal_31);


										vec_69 = _mm512_fmadd_ps(vec_70, vec_68, mem_vec_1891);
										mem_vec_1891 = vec_69;
										scal_32 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 2 + h) + c];
										vec_72 = _mm512_set1_ps(scal_32);


										vec_71 = _mm512_fmadd_ps(vec_72, vec_68, mem_vec_1892);
										mem_vec_1892 = vec_71;
										scal_33 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 3 + h) + c];
										vec_74 = _mm512_set1_ps(scal_33);


										vec_73 = _mm512_fmadd_ps(vec_74, vec_68, mem_vec_1893);
										mem_vec_1893 = vec_73;
										scal_34 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 4 + h) + c];
										vec_76 = _mm512_set1_ps(scal_34);


										vec_75 = _mm512_fmadd_ps(vec_76, vec_68, mem_vec_1894);
										mem_vec_1894 = vec_75;
										scal_35 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + h + 1) + c];
										vec_78 = _mm512_set1_ps(scal_35);
										vec_79 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 2) + (F * C) * (h + 1) + F * c + f]);

										vec_77 = _mm512_fmadd_ps(vec_78, vec_79, mem_vec_1890);
										mem_vec_1890 = vec_77;
										scal_36 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 1 + h + 1) + c];
										vec_81 = _mm512_set1_ps(scal_36);


										vec_80 = _mm512_fmadd_ps(vec_81, vec_79, mem_vec_1891);
										mem_vec_1891 = vec_80;
										scal_37 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 2 + h + 1) + c];
										vec_83 = _mm512_set1_ps(scal_37);


										vec_82 = _mm512_fmadd_ps(vec_83, vec_79, mem_vec_1892);
										mem_vec_1892 = vec_82;
										scal_38 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 3 + h + 1) + c];
										vec_85 = _mm512_set1_ps(scal_38);


										vec_84 = _mm512_fmadd_ps(vec_85, vec_79, mem_vec_1893);
										mem_vec_1893 = vec_84;
										scal_39 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 4 + h + 1) + c];
										vec_87 = _mm512_set1_ps(scal_39);


										vec_86 = _mm512_fmadd_ps(vec_87, vec_79, mem_vec_1894);
										mem_vec_1894 = vec_86;
										scal_40 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + h + 2) + c];
										vec_89 = _mm512_set1_ps(scal_40);
										vec_90 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 2) + (F * C) * (h + 2) + F * c + f]);

										vec_88 = _mm512_fmadd_ps(vec_89, vec_90, mem_vec_1890);
										mem_vec_1890 = vec_88;
										scal_41 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 1 + h + 2) + c];
										vec_92 = _mm512_set1_ps(scal_41);


										vec_91 = _mm512_fmadd_ps(vec_92, vec_90, mem_vec_1891);
										mem_vec_1891 = vec_91;
										scal_42 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 2 + h + 2) + c];
										vec_94 = _mm512_set1_ps(scal_42);


										vec_93 = _mm512_fmadd_ps(vec_94, vec_90, mem_vec_1892);
										mem_vec_1892 = vec_93;
										scal_43 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 3 + h + 2) + c];
										vec_96 = _mm512_set1_ps(scal_43);


										vec_95 = _mm512_fmadd_ps(vec_96, vec_90, mem_vec_1893);
										mem_vec_1893 = vec_95;
										scal_44 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 4 + h + 2) + c];
										vec_98 = _mm512_set1_ps(scal_44);


										vec_97 = _mm512_fmadd_ps(vec_98, vec_90, mem_vec_1894);
										mem_vec_1894 = vec_97;
									}
								_mm512_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_1890);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_1891);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_1892);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_1893);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f], mem_vec_1894);
						}
					}
				}
				for (y = y196 + 5, yp_0 = 0;
					y < y196 + 5 + 12;
					y += 12, yp_0 += 12){
					// y = ph_y, x = 17, h = 3, w = 3, c = 64, f = 16
					// T (c, 1) (64 / 64)
					for (c392 = c393, c392_p_1 = c393_p_0, c392_p_0 = 0;
						c392 < c393 + 64;
						c392 += 64, c392_p_1 += 64, c392_p_0 += 64){
						// y = ph_y, x = 17, h = 3, w = 3, c = 64, f = 16
						// T (x, 17) (17 / 1)
						for (x = x196, xp_0 = 0;
							x < x196 + 17;
							x += 1, xp_0 += 1){
									mem_vec_1895 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f]);
									mem_vec_1896 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
									mem_vec_1897 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
									mem_vec_1898 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
									mem_vec_1899 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f]);
									mem_vec_1900 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f]);
									mem_vec_1901 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f]);
									mem_vec_1902 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 7) + f]);
									mem_vec_1903 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 8) + f]);
									mem_vec_1904 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 9) + f]);
									mem_vec_1905 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 10) + f]);
									mem_vec_1906 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 11) + f]);
									// y = ph_y, x = 1, h = 3, w = 3, c = 64, f = 16
									// T (c, 64) (64 / 1)
									for (c = c392, cp_2 = c392_p_1, cp_1 = c392_p_0, cp_0 = 0;
										c < c392 + 64;
										c += 1, cp_2 += 1, cp_1 += 1, cp_0 += 1){
										scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
										vec_1 = _mm512_set1_ps(scal_0);
										vec_2 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

										vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_1895);
										mem_vec_1895 = vec_0;
										scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
										vec_4 = _mm512_set1_ps(scal_1);


										vec_3 = _mm512_fmadd_ps(vec_4, vec_2, mem_vec_1896);
										mem_vec_1896 = vec_3;
										scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
										vec_6 = _mm512_set1_ps(scal_2);


										vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_1897);
										mem_vec_1897 = vec_5;
										scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
										vec_8 = _mm512_set1_ps(scal_3);


										vec_7 = _mm512_fmadd_ps(vec_8, vec_2, mem_vec_1898);
										mem_vec_1898 = vec_7;
										scal_4 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h) + c];
										vec_10 = _mm512_set1_ps(scal_4);


										vec_9 = _mm512_fmadd_ps(vec_10, vec_2, mem_vec_1899);
										mem_vec_1899 = vec_9;
										scal_5 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h) + c];
										vec_12 = _mm512_set1_ps(scal_5);


										vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_1900);
										mem_vec_1900 = vec_11;
										scal_6 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h) + c];
										vec_14 = _mm512_set1_ps(scal_6);


										vec_13 = _mm512_fmadd_ps(vec_14, vec_2, mem_vec_1901);
										mem_vec_1901 = vec_13;
										scal_7 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h) + c];
										vec_16 = _mm512_set1_ps(scal_7);


										vec_15 = _mm512_fmadd_ps(vec_16, vec_2, mem_vec_1902);
										mem_vec_1902 = vec_15;
										scal_8 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 8 + h) + c];
										vec_18 = _mm512_set1_ps(scal_8);


										vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_1903);
										mem_vec_1903 = vec_17;
										scal_9 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 9 + h) + c];
										vec_20 = _mm512_set1_ps(scal_9);


										vec_19 = _mm512_fmadd_ps(vec_20, vec_2, mem_vec_1904);
										mem_vec_1904 = vec_19;
										scal_10 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 10 + h) + c];
										vec_22 = _mm512_set1_ps(scal_10);


										vec_21 = _mm512_fmadd_ps(vec_22, vec_2, mem_vec_1905);
										mem_vec_1905 = vec_21;
										scal_11 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 11 + h) + c];
										vec_24 = _mm512_set1_ps(scal_11);


										vec_23 = _mm512_fmadd_ps(vec_24, vec_2, mem_vec_1906);
										mem_vec_1906 = vec_23;
										scal_12 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h + 1) + c];
										vec_26 = _mm512_set1_ps(scal_12);
										vec_27 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 1) + F * c + f]);

										vec_25 = _mm512_fmadd_ps(vec_26, vec_27, mem_vec_1895);
										mem_vec_1895 = vec_25;
										scal_13 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h + 1) + c];
										vec_29 = _mm512_set1_ps(scal_13);


										vec_28 = _mm512_fmadd_ps(vec_29, vec_27, mem_vec_1896);
										mem_vec_1896 = vec_28;
										scal_14 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h + 1) + c];
										vec_31 = _mm512_set1_ps(scal_14);


										vec_30 = _mm512_fmadd_ps(vec_31, vec_27, mem_vec_1897);
										mem_vec_1897 = vec_30;
										scal_15 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h + 1) + c];
										vec_33 = _mm512_set1_ps(scal_15);


										vec_32 = _mm512_fmadd_ps(vec_33, vec_27, mem_vec_1898);
										mem_vec_1898 = vec_32;
										scal_16 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h + 1) + c];
										vec_35 = _mm512_set1_ps(scal_16);


										vec_34 = _mm512_fmadd_ps(vec_35, vec_27, mem_vec_1899);
										mem_vec_1899 = vec_34;
										scal_17 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h + 1) + c];
										vec_37 = _mm512_set1_ps(scal_17);


										vec_36 = _mm512_fmadd_ps(vec_37, vec_27, mem_vec_1900);
										mem_vec_1900 = vec_36;
										scal_18 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h + 1) + c];
										vec_39 = _mm512_set1_ps(scal_18);


										vec_38 = _mm512_fmadd_ps(vec_39, vec_27, mem_vec_1901);
										mem_vec_1901 = vec_38;
										scal_19 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h + 1) + c];
										vec_41 = _mm512_set1_ps(scal_19);


										vec_40 = _mm512_fmadd_ps(vec_41, vec_27, mem_vec_1902);
										mem_vec_1902 = vec_40;
										scal_20 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 8 + h + 1) + c];
										vec_43 = _mm512_set1_ps(scal_20);


										vec_42 = _mm512_fmadd_ps(vec_43, vec_27, mem_vec_1903);
										mem_vec_1903 = vec_42;
										scal_21 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 9 + h + 1) + c];
										vec_45 = _mm512_set1_ps(scal_21);


										vec_44 = _mm512_fmadd_ps(vec_45, vec_27, mem_vec_1904);
										mem_vec_1904 = vec_44;
										scal_22 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 10 + h + 1) + c];
										vec_47 = _mm512_set1_ps(scal_22);


										vec_46 = _mm512_fmadd_ps(vec_47, vec_27, mem_vec_1905);
										mem_vec_1905 = vec_46;
										scal_23 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 11 + h + 1) + c];
										vec_49 = _mm512_set1_ps(scal_23);


										vec_48 = _mm512_fmadd_ps(vec_49, vec_27, mem_vec_1906);
										mem_vec_1906 = vec_48;
										scal_24 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h + 2) + c];
										vec_51 = _mm512_set1_ps(scal_24);
										vec_52 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 2) + F * c + f]);

										vec_50 = _mm512_fmadd_ps(vec_51, vec_52, mem_vec_1895);
										mem_vec_1895 = vec_50;
										scal_25 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h + 2) + c];
										vec_54 = _mm512_set1_ps(scal_25);


										vec_53 = _mm512_fmadd_ps(vec_54, vec_52, mem_vec_1896);
										mem_vec_1896 = vec_53;
										scal_26 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h + 2) + c];
										vec_56 = _mm512_set1_ps(scal_26);


										vec_55 = _mm512_fmadd_ps(vec_56, vec_52, mem_vec_1897);
										mem_vec_1897 = vec_55;
										scal_27 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h + 2) + c];
										vec_58 = _mm512_set1_ps(scal_27);


										vec_57 = _mm512_fmadd_ps(vec_58, vec_52, mem_vec_1898);
										mem_vec_1898 = vec_57;
										scal_28 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h + 2) + c];
										vec_60 = _mm512_set1_ps(scal_28);


										vec_59 = _mm512_fmadd_ps(vec_60, vec_52, mem_vec_1899);
										mem_vec_1899 = vec_59;
										scal_29 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h + 2) + c];
										vec_62 = _mm512_set1_ps(scal_29);


										vec_61 = _mm512_fmadd_ps(vec_62, vec_52, mem_vec_1900);
										mem_vec_1900 = vec_61;
										scal_30 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h + 2) + c];
										vec_64 = _mm512_set1_ps(scal_30);


										vec_63 = _mm512_fmadd_ps(vec_64, vec_52, mem_vec_1901);
										mem_vec_1901 = vec_63;
										scal_31 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h + 2) + c];
										vec_66 = _mm512_set1_ps(scal_31);


										vec_65 = _mm512_fmadd_ps(vec_66, vec_52, mem_vec_1902);
										mem_vec_1902 = vec_65;
										scal_32 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 8 + h + 2) + c];
										vec_68 = _mm512_set1_ps(scal_32);


										vec_67 = _mm512_fmadd_ps(vec_68, vec_52, mem_vec_1903);
										mem_vec_1903 = vec_67;
										scal_33 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 9 + h + 2) + c];
										vec_70 = _mm512_set1_ps(scal_33);


										vec_69 = _mm512_fmadd_ps(vec_70, vec_52, mem_vec_1904);
										mem_vec_1904 = vec_69;
										scal_34 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 10 + h + 2) + c];
										vec_72 = _mm512_set1_ps(scal_34);


										vec_71 = _mm512_fmadd_ps(vec_72, vec_52, mem_vec_1905);
										mem_vec_1905 = vec_71;
										scal_35 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 11 + h + 2) + c];
										vec_74 = _mm512_set1_ps(scal_35);


										vec_73 = _mm512_fmadd_ps(vec_74, vec_52, mem_vec_1906);
										mem_vec_1906 = vec_73;
										scal_36 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + h) + c];
										vec_76 = _mm512_set1_ps(scal_36);
										vec_77 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 1) + (F * C) * h + F * c + f]);

										vec_75 = _mm512_fmadd_ps(vec_76, vec_77, mem_vec_1895);
										mem_vec_1895 = vec_75;
										scal_37 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 1 + h) + c];
										vec_79 = _mm512_set1_ps(scal_37);


										vec_78 = _mm512_fmadd_ps(vec_79, vec_77, mem_vec_1896);
										mem_vec_1896 = vec_78;
										scal_38 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 2 + h) + c];
										vec_81 = _mm512_set1_ps(scal_38);


										vec_80 = _mm512_fmadd_ps(vec_81, vec_77, mem_vec_1897);
										mem_vec_1897 = vec_80;
										scal_39 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 3 + h) + c];
										vec_83 = _mm512_set1_ps(scal_39);


										vec_82 = _mm512_fmadd_ps(vec_83, vec_77, mem_vec_1898);
										mem_vec_1898 = vec_82;
										scal_40 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 4 + h) + c];
										vec_85 = _mm512_set1_ps(scal_40);


										vec_84 = _mm512_fmadd_ps(vec_85, vec_77, mem_vec_1899);
										mem_vec_1899 = vec_84;
										scal_41 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 5 + h) + c];
										vec_87 = _mm512_set1_ps(scal_41);


										vec_86 = _mm512_fmadd_ps(vec_87, vec_77, mem_vec_1900);
										mem_vec_1900 = vec_86;
										scal_42 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 6 + h) + c];
										vec_89 = _mm512_set1_ps(scal_42);


										vec_88 = _mm512_fmadd_ps(vec_89, vec_77, mem_vec_1901);
										mem_vec_1901 = vec_88;
										scal_43 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 7 + h) + c];
										vec_91 = _mm512_set1_ps(scal_43);


										vec_90 = _mm512_fmadd_ps(vec_91, vec_77, mem_vec_1902);
										mem_vec_1902 = vec_90;
										scal_44 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 8 + h) + c];
										vec_93 = _mm512_set1_ps(scal_44);


										vec_92 = _mm512_fmadd_ps(vec_93, vec_77, mem_vec_1903);
										mem_vec_1903 = vec_92;
										scal_45 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 9 + h) + c];
										vec_95 = _mm512_set1_ps(scal_45);


										vec_94 = _mm512_fmadd_ps(vec_95, vec_77, mem_vec_1904);
										mem_vec_1904 = vec_94;
										scal_46 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 10 + h) + c];
										vec_97 = _mm512_set1_ps(scal_46);


										vec_96 = _mm512_fmadd_ps(vec_97, vec_77, mem_vec_1905);
										mem_vec_1905 = vec_96;
										scal_47 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 11 + h) + c];
										vec_99 = _mm512_set1_ps(scal_47);


										vec_98 = _mm512_fmadd_ps(vec_99, vec_77, mem_vec_1906);
										mem_vec_1906 = vec_98;
										scal_48 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + h + 1) + c];
										vec_101 = _mm512_set1_ps(scal_48);
										vec_102 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 1) + (F * C) * (h + 1) + F * c + f]);

										vec_100 = _mm512_fmadd_ps(vec_101, vec_102, mem_vec_1895);
										mem_vec_1895 = vec_100;
										scal_49 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 1 + h + 1) + c];
										vec_104 = _mm512_set1_ps(scal_49);


										vec_103 = _mm512_fmadd_ps(vec_104, vec_102, mem_vec_1896);
										mem_vec_1896 = vec_103;
										scal_50 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 2 + h + 1) + c];
										vec_106 = _mm512_set1_ps(scal_50);


										vec_105 = _mm512_fmadd_ps(vec_106, vec_102, mem_vec_1897);
										mem_vec_1897 = vec_105;
										scal_51 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 3 + h + 1) + c];
										vec_108 = _mm512_set1_ps(scal_51);


										vec_107 = _mm512_fmadd_ps(vec_108, vec_102, mem_vec_1898);
										mem_vec_1898 = vec_107;
										scal_52 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 4 + h + 1) + c];
										vec_110 = _mm512_set1_ps(scal_52);


										vec_109 = _mm512_fmadd_ps(vec_110, vec_102, mem_vec_1899);
										mem_vec_1899 = vec_109;
										scal_53 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 5 + h + 1) + c];
										vec_112 = _mm512_set1_ps(scal_53);


										vec_111 = _mm512_fmadd_ps(vec_112, vec_102, mem_vec_1900);
										mem_vec_1900 = vec_111;
										scal_54 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 6 + h + 1) + c];
										vec_114 = _mm512_set1_ps(scal_54);


										vec_113 = _mm512_fmadd_ps(vec_114, vec_102, mem_vec_1901);
										mem_vec_1901 = vec_113;
										scal_55 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 7 + h + 1) + c];
										vec_116 = _mm512_set1_ps(scal_55);


										vec_115 = _mm512_fmadd_ps(vec_116, vec_102, mem_vec_1902);
										mem_vec_1902 = vec_115;
										scal_56 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 8 + h + 1) + c];
										vec_118 = _mm512_set1_ps(scal_56);


										vec_117 = _mm512_fmadd_ps(vec_118, vec_102, mem_vec_1903);
										mem_vec_1903 = vec_117;
										scal_57 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 9 + h + 1) + c];
										vec_120 = _mm512_set1_ps(scal_57);


										vec_119 = _mm512_fmadd_ps(vec_120, vec_102, mem_vec_1904);
										mem_vec_1904 = vec_119;
										scal_58 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 10 + h + 1) + c];
										vec_122 = _mm512_set1_ps(scal_58);


										vec_121 = _mm512_fmadd_ps(vec_122, vec_102, mem_vec_1905);
										mem_vec_1905 = vec_121;
										scal_59 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 11 + h + 1) + c];
										vec_124 = _mm512_set1_ps(scal_59);


										vec_123 = _mm512_fmadd_ps(vec_124, vec_102, mem_vec_1906);
										mem_vec_1906 = vec_123;
										scal_60 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + h + 2) + c];
										vec_126 = _mm512_set1_ps(scal_60);
										vec_127 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 1) + (F * C) * (h + 2) + F * c + f]);

										vec_125 = _mm512_fmadd_ps(vec_126, vec_127, mem_vec_1895);
										mem_vec_1895 = vec_125;
										scal_61 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 1 + h + 2) + c];
										vec_129 = _mm512_set1_ps(scal_61);


										vec_128 = _mm512_fmadd_ps(vec_129, vec_127, mem_vec_1896);
										mem_vec_1896 = vec_128;
										scal_62 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 2 + h + 2) + c];
										vec_131 = _mm512_set1_ps(scal_62);


										vec_130 = _mm512_fmadd_ps(vec_131, vec_127, mem_vec_1897);
										mem_vec_1897 = vec_130;
										scal_63 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 3 + h + 2) + c];
										vec_133 = _mm512_set1_ps(scal_63);


										vec_132 = _mm512_fmadd_ps(vec_133, vec_127, mem_vec_1898);
										mem_vec_1898 = vec_132;
										scal_64 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 4 + h + 2) + c];
										vec_135 = _mm512_set1_ps(scal_64);


										vec_134 = _mm512_fmadd_ps(vec_135, vec_127, mem_vec_1899);
										mem_vec_1899 = vec_134;
										scal_65 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 5 + h + 2) + c];
										vec_137 = _mm512_set1_ps(scal_65);


										vec_136 = _mm512_fmadd_ps(vec_137, vec_127, mem_vec_1900);
										mem_vec_1900 = vec_136;
										scal_66 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 6 + h + 2) + c];
										vec_139 = _mm512_set1_ps(scal_66);


										vec_138 = _mm512_fmadd_ps(vec_139, vec_127, mem_vec_1901);
										mem_vec_1901 = vec_138;
										scal_67 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 7 + h + 2) + c];
										vec_141 = _mm512_set1_ps(scal_67);


										vec_140 = _mm512_fmadd_ps(vec_141, vec_127, mem_vec_1902);
										mem_vec_1902 = vec_140;
										scal_68 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 8 + h + 2) + c];
										vec_143 = _mm512_set1_ps(scal_68);


										vec_142 = _mm512_fmadd_ps(vec_143, vec_127, mem_vec_1903);
										mem_vec_1903 = vec_142;
										scal_69 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 9 + h + 2) + c];
										vec_145 = _mm512_set1_ps(scal_69);


										vec_144 = _mm512_fmadd_ps(vec_145, vec_127, mem_vec_1904);
										mem_vec_1904 = vec_144;
										scal_70 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 10 + h + 2) + c];
										vec_147 = _mm512_set1_ps(scal_70);


										vec_146 = _mm512_fmadd_ps(vec_147, vec_127, mem_vec_1905);
										mem_vec_1905 = vec_146;
										scal_71 = input[(C * (Y + H - 1)) * (x + w + 1) + C * (y + 11 + h + 2) + c];
										vec_149 = _mm512_set1_ps(scal_71);


										vec_148 = _mm512_fmadd_ps(vec_149, vec_127, mem_vec_1906);
										mem_vec_1906 = vec_148;
										scal_72 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + h) + c];
										vec_151 = _mm512_set1_ps(scal_72);
										vec_152 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 2) + (F * C) * h + F * c + f]);

										vec_150 = _mm512_fmadd_ps(vec_151, vec_152, mem_vec_1895);
										mem_vec_1895 = vec_150;
										scal_73 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 1 + h) + c];
										vec_154 = _mm512_set1_ps(scal_73);


										vec_153 = _mm512_fmadd_ps(vec_154, vec_152, mem_vec_1896);
										mem_vec_1896 = vec_153;
										scal_74 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 2 + h) + c];
										vec_156 = _mm512_set1_ps(scal_74);


										vec_155 = _mm512_fmadd_ps(vec_156, vec_152, mem_vec_1897);
										mem_vec_1897 = vec_155;
										scal_75 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 3 + h) + c];
										vec_158 = _mm512_set1_ps(scal_75);


										vec_157 = _mm512_fmadd_ps(vec_158, vec_152, mem_vec_1898);
										mem_vec_1898 = vec_157;
										scal_76 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 4 + h) + c];
										vec_160 = _mm512_set1_ps(scal_76);


										vec_159 = _mm512_fmadd_ps(vec_160, vec_152, mem_vec_1899);
										mem_vec_1899 = vec_159;
										scal_77 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 5 + h) + c];
										vec_162 = _mm512_set1_ps(scal_77);


										vec_161 = _mm512_fmadd_ps(vec_162, vec_152, mem_vec_1900);
										mem_vec_1900 = vec_161;
										scal_78 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 6 + h) + c];
										vec_164 = _mm512_set1_ps(scal_78);


										vec_163 = _mm512_fmadd_ps(vec_164, vec_152, mem_vec_1901);
										mem_vec_1901 = vec_163;
										scal_79 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 7 + h) + c];
										vec_166 = _mm512_set1_ps(scal_79);


										vec_165 = _mm512_fmadd_ps(vec_166, vec_152, mem_vec_1902);
										mem_vec_1902 = vec_165;
										scal_80 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 8 + h) + c];
										vec_168 = _mm512_set1_ps(scal_80);


										vec_167 = _mm512_fmadd_ps(vec_168, vec_152, mem_vec_1903);
										mem_vec_1903 = vec_167;
										scal_81 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 9 + h) + c];
										vec_170 = _mm512_set1_ps(scal_81);


										vec_169 = _mm512_fmadd_ps(vec_170, vec_152, mem_vec_1904);
										mem_vec_1904 = vec_169;
										scal_82 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 10 + h) + c];
										vec_172 = _mm512_set1_ps(scal_82);


										vec_171 = _mm512_fmadd_ps(vec_172, vec_152, mem_vec_1905);
										mem_vec_1905 = vec_171;
										scal_83 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 11 + h) + c];
										vec_174 = _mm512_set1_ps(scal_83);


										vec_173 = _mm512_fmadd_ps(vec_174, vec_152, mem_vec_1906);
										mem_vec_1906 = vec_173;
										scal_84 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + h + 1) + c];
										vec_176 = _mm512_set1_ps(scal_84);
										vec_177 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 2) + (F * C) * (h + 1) + F * c + f]);

										vec_175 = _mm512_fmadd_ps(vec_176, vec_177, mem_vec_1895);
										mem_vec_1895 = vec_175;
										scal_85 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 1 + h + 1) + c];
										vec_179 = _mm512_set1_ps(scal_85);


										vec_178 = _mm512_fmadd_ps(vec_179, vec_177, mem_vec_1896);
										mem_vec_1896 = vec_178;
										scal_86 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 2 + h + 1) + c];
										vec_181 = _mm512_set1_ps(scal_86);


										vec_180 = _mm512_fmadd_ps(vec_181, vec_177, mem_vec_1897);
										mem_vec_1897 = vec_180;
										scal_87 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 3 + h + 1) + c];
										vec_183 = _mm512_set1_ps(scal_87);


										vec_182 = _mm512_fmadd_ps(vec_183, vec_177, mem_vec_1898);
										mem_vec_1898 = vec_182;
										scal_88 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 4 + h + 1) + c];
										vec_185 = _mm512_set1_ps(scal_88);


										vec_184 = _mm512_fmadd_ps(vec_185, vec_177, mem_vec_1899);
										mem_vec_1899 = vec_184;
										scal_89 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 5 + h + 1) + c];
										vec_187 = _mm512_set1_ps(scal_89);


										vec_186 = _mm512_fmadd_ps(vec_187, vec_177, mem_vec_1900);
										mem_vec_1900 = vec_186;
										scal_90 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 6 + h + 1) + c];
										vec_189 = _mm512_set1_ps(scal_90);


										vec_188 = _mm512_fmadd_ps(vec_189, vec_177, mem_vec_1901);
										mem_vec_1901 = vec_188;
										scal_91 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 7 + h + 1) + c];
										vec_191 = _mm512_set1_ps(scal_91);


										vec_190 = _mm512_fmadd_ps(vec_191, vec_177, mem_vec_1902);
										mem_vec_1902 = vec_190;
										scal_92 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 8 + h + 1) + c];
										vec_193 = _mm512_set1_ps(scal_92);


										vec_192 = _mm512_fmadd_ps(vec_193, vec_177, mem_vec_1903);
										mem_vec_1903 = vec_192;
										scal_93 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 9 + h + 1) + c];
										vec_195 = _mm512_set1_ps(scal_93);


										vec_194 = _mm512_fmadd_ps(vec_195, vec_177, mem_vec_1904);
										mem_vec_1904 = vec_194;
										scal_94 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 10 + h + 1) + c];
										vec_197 = _mm512_set1_ps(scal_94);


										vec_196 = _mm512_fmadd_ps(vec_197, vec_177, mem_vec_1905);
										mem_vec_1905 = vec_196;
										scal_95 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 11 + h + 1) + c];
										vec_199 = _mm512_set1_ps(scal_95);


										vec_198 = _mm512_fmadd_ps(vec_199, vec_177, mem_vec_1906);
										mem_vec_1906 = vec_198;
										scal_96 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + h + 2) + c];
										vec_201 = _mm512_set1_ps(scal_96);
										vec_202 = _mm512_loadu_ps(&params[((F * C) * H) * (w + 2) + (F * C) * (h + 2) + F * c + f]);

										vec_200 = _mm512_fmadd_ps(vec_201, vec_202, mem_vec_1895);
										mem_vec_1895 = vec_200;
										scal_97 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 1 + h + 2) + c];
										vec_204 = _mm512_set1_ps(scal_97);


										vec_203 = _mm512_fmadd_ps(vec_204, vec_202, mem_vec_1896);
										mem_vec_1896 = vec_203;
										scal_98 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 2 + h + 2) + c];
										vec_206 = _mm512_set1_ps(scal_98);


										vec_205 = _mm512_fmadd_ps(vec_206, vec_202, mem_vec_1897);
										mem_vec_1897 = vec_205;
										scal_99 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 3 + h + 2) + c];
										vec_208 = _mm512_set1_ps(scal_99);


										vec_207 = _mm512_fmadd_ps(vec_208, vec_202, mem_vec_1898);
										mem_vec_1898 = vec_207;
										scal_100 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 4 + h + 2) + c];
										vec_210 = _mm512_set1_ps(scal_100);


										vec_209 = _mm512_fmadd_ps(vec_210, vec_202, mem_vec_1899);
										mem_vec_1899 = vec_209;
										scal_101 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 5 + h + 2) + c];
										vec_212 = _mm512_set1_ps(scal_101);


										vec_211 = _mm512_fmadd_ps(vec_212, vec_202, mem_vec_1900);
										mem_vec_1900 = vec_211;
										scal_102 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 6 + h + 2) + c];
										vec_214 = _mm512_set1_ps(scal_102);


										vec_213 = _mm512_fmadd_ps(vec_214, vec_202, mem_vec_1901);
										mem_vec_1901 = vec_213;
										scal_103 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 7 + h + 2) + c];
										vec_216 = _mm512_set1_ps(scal_103);


										vec_215 = _mm512_fmadd_ps(vec_216, vec_202, mem_vec_1902);
										mem_vec_1902 = vec_215;
										scal_104 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 8 + h + 2) + c];
										vec_218 = _mm512_set1_ps(scal_104);


										vec_217 = _mm512_fmadd_ps(vec_218, vec_202, mem_vec_1903);
										mem_vec_1903 = vec_217;
										scal_105 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 9 + h + 2) + c];
										vec_220 = _mm512_set1_ps(scal_105);


										vec_219 = _mm512_fmadd_ps(vec_220, vec_202, mem_vec_1904);
										mem_vec_1904 = vec_219;
										scal_106 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 10 + h + 2) + c];
										vec_222 = _mm512_set1_ps(scal_106);


										vec_221 = _mm512_fmadd_ps(vec_222, vec_202, mem_vec_1905);
										mem_vec_1905 = vec_221;
										scal_107 = input[(C * (Y + H - 1)) * (x + w + 2) + C * (y + 11 + h + 2) + c];
										vec_224 = _mm512_set1_ps(scal_107);


										vec_223 = _mm512_fmadd_ps(vec_224, vec_202, mem_vec_1906);
										mem_vec_1906 = vec_223;
									}
								_mm512_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_1895);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_1896);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_1897);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_1898);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f], mem_vec_1899);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f], mem_vec_1900);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f], mem_vec_1901);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 7) + f], mem_vec_1902);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 8) + f], mem_vec_1903);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 9) + f], mem_vec_1904);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 10) + f], mem_vec_1905);
								_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 11) + f], mem_vec_1906);
						}
					}
				}
		}
	}
}


}