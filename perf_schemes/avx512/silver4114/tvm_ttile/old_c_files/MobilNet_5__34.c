#include "../gen_conv.h"
#include "../reorder_avx2.h"

void gen_conv(M_TYPE * const  __restrict__ output,
M_TYPE const * const __restrict__ input, M_TYPE const * const __restrict__ params,
    IND_TYPE X, IND_TYPE W,
    IND_TYPE Y, IND_TYPE H,
	IND_TYPE C, IND_TYPE F) {
/*
[V f; U (4, f); U (4, y); T (32, c); Hoist_vars [c]; T (3, h); T (28, x);
  T (3, w); T (8, c); T (1, f); T (7, y); T (1, x); T (2, f); T (2, f)]
*/
IND_TYPE c, cp_0, c90_p_0, cp_1, c90, f, fp_0, f68_p_0, f69_p_0, fp_1, f68_p_1, fp_2, f68, f69, h, hp_0, w, wp_0, x, xp_0, x90_p_0, xp_1, x90, y, yp_0;

assert((Y == 28));
assert((X == 28));
assert((H == 3));
assert((W == 3));
assert((C == 256));
assert((F == 256));
IND_TYPE y86 = 0;
IND_TYPE x91 = 0;
IND_TYPE h44 = 0;
IND_TYPE w48 = 0;
IND_TYPE c91 = 0;
IND_TYPE f70 = 0;
float scal_0 ,scal_1 ,scal_2 ,scal_3;
__m512 mem_vec_568 ,mem_vec_569 ,mem_vec_570 ,mem_vec_571 ,mem_vec_572 ,mem_vec_573 ,mem_vec_574 ,mem_vec_575 ,mem_vec_576 ,mem_vec_577 ,mem_vec_578 ,mem_vec_579 ,mem_vec_580 ,mem_vec_581 ,mem_vec_582 ,mem_vec_583 ,vec_0 ,vec_1 ,vec_10 ,vec_11 ,vec_12 ,vec_13 ,vec_14 ,vec_15 ,vec_16 ,vec_17 ,vec_18 ,vec_19 ,vec_2 ,vec_20 ,vec_21 ,vec_22 ,vec_23 ,vec_3 ,vec_4 ,vec_5 ,vec_6 ,vec_7 ,vec_8 ,vec_9;
// y = 28, x = 28, h = 3, w = 3, c = 256, f = 256
// T (f, 2) (256 / 128)
for (f69 = f70, f69_p_0 = 0;
	f69 < f70 + 256;
	f69 += 128, f69_p_0 += 128){
	// y = 28, x = 28, h = 3, w = 3, c = 256, f = 128
	// T (f, 2) (128 / 64)
	for (f68 = f69, f68_p_1 = f69_p_0, f68_p_0 = 0;
		f68 < f69 + 128;
		f68 += 64, f68_p_1 += 64, f68_p_0 += 64){
		// y = 28, x = 28, h = 3, w = 3, c = 256, f = 64
		// T (x, 1) (28 / 28)
		for (x90 = x91, x90_p_0 = 0;
			x90 < x91 + 28;
			x90 += 28, x90_p_0 += 28){
			// y = 28, x = 28, h = 3, w = 3, c = 256, f = 64
			// T (y, 7) (28 / 4)
			for (y = y86, yp_0 = 0;
				y < y86 + 28;
				y += 4, yp_0 += 4){
				// y = 4, x = 28, h = 3, w = 3, c = 256, f = 64
				// T (f, 1) (64 / 64)
				for (f = f68, fp_2 = f68_p_1, fp_1 = f68_p_0, fp_0 = 0;
					f < f68 + 64;
					f += 64, fp_2 += 64, fp_1 += 64, fp_0 += 64){
					// y = 4, x = 28, h = 3, w = 3, c = 256, f = 64
					// T (c, 8) (256 / 32)
					for (c90 = c91, c90_p_0 = 0;
						c90 < c91 + 256;
						c90 += 32, c90_p_0 += 32){
						// y = 4, x = 28, h = 3, w = 3, c = 32, f = 64
						// T (w, 3) (3 / 1)
						for (w = w48, wp_0 = 0;
							w < w48 + 3;
							w += 1, wp_0 += 1){
							// y = 4, x = 28, h = 3, w = 1, c = 32, f = 64
							// T (x, 28) (28 / 1)
							for (x = x90, xp_1 = x90_p_0, xp_0 = 0;
								x < x90 + 28;
								x += 1, xp_1 += 1, xp_0 += 1){
								// y = 4, x = 1, h = 3, w = 1, c = 32, f = 64
								// T (h, 3) (3 / 1)
								for (h = h44, hp_0 = 0;
									h < h44 + 3;
									h += 1, hp_0 += 1){
											mem_vec_568 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f]);
											mem_vec_569 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 16]);
											mem_vec_570 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 32]);
											mem_vec_571 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 48]);
											mem_vec_572 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
											mem_vec_573 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16]);
											mem_vec_574 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 32]);
											mem_vec_575 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 48]);
											mem_vec_576 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
											mem_vec_577 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16]);
											mem_vec_578 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 32]);
											mem_vec_579 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 48]);
											mem_vec_580 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
											mem_vec_581 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16]);
											mem_vec_582 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 32]);
											mem_vec_583 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 48]);
											// y = 4, x = 1, h = 1, w = 1, c = 32, f = 64
											// T (c, 32) (32 / 1)
											for (c = c90, cp_1 = c90_p_0, cp_0 = 0;
												c < c90 + 32;
												c += 1, cp_1 += 1, cp_0 += 1){
												scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
												vec_1 = _mm512_set1_ps(scal_0);
												vec_2 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

												vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_568);
												mem_vec_568 = vec_0;

												vec_4 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 16]);

												vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_569);
												mem_vec_569 = vec_3;

												vec_6 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 32]);

												vec_5 = _mm512_fmadd_ps(vec_1, vec_6, mem_vec_570);
												mem_vec_570 = vec_5;

												vec_8 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 48]);

												vec_7 = _mm512_fmadd_ps(vec_1, vec_8, mem_vec_571);
												mem_vec_571 = vec_7;
												scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
												vec_10 = _mm512_set1_ps(scal_1);


												vec_9 = _mm512_fmadd_ps(vec_10, vec_2, mem_vec_572);
												mem_vec_572 = vec_9;



												vec_11 = _mm512_fmadd_ps(vec_10, vec_4, mem_vec_573);
												mem_vec_573 = vec_11;



												vec_12 = _mm512_fmadd_ps(vec_10, vec_6, mem_vec_574);
												mem_vec_574 = vec_12;



												vec_13 = _mm512_fmadd_ps(vec_10, vec_8, mem_vec_575);
												mem_vec_575 = vec_13;
												scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
												vec_15 = _mm512_set1_ps(scal_2);


												vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_576);
												mem_vec_576 = vec_14;



												vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_577);
												mem_vec_577 = vec_16;



												vec_17 = _mm512_fmadd_ps(vec_15, vec_6, mem_vec_578);
												mem_vec_578 = vec_17;



												vec_18 = _mm512_fmadd_ps(vec_15, vec_8, mem_vec_579);
												mem_vec_579 = vec_18;
												scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
												vec_20 = _mm512_set1_ps(scal_3);


												vec_19 = _mm512_fmadd_ps(vec_20, vec_2, mem_vec_580);
												mem_vec_580 = vec_19;



												vec_21 = _mm512_fmadd_ps(vec_20, vec_4, mem_vec_581);
												mem_vec_581 = vec_21;



												vec_22 = _mm512_fmadd_ps(vec_20, vec_6, mem_vec_582);
												mem_vec_582 = vec_22;



												vec_23 = _mm512_fmadd_ps(vec_20, vec_8, mem_vec_583);
												mem_vec_583 = vec_23;
											}
										_mm512_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_568);
										_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 16], mem_vec_569);
										_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 32], mem_vec_570);
										_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 48], mem_vec_571);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_572);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16], mem_vec_573);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 32], mem_vec_574);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 48], mem_vec_575);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_576);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16], mem_vec_577);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 32], mem_vec_578);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 48], mem_vec_579);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_580);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16], mem_vec_581);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 32], mem_vec_582);
										_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 48], mem_vec_583);
								}
							}
						}
					}
				}
			}
		}
	}
}


}