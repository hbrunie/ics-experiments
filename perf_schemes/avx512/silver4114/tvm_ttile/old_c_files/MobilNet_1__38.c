#include "../gen_conv.h"
#include "../reorder_avx2.h"

void gen_conv(M_TYPE * const  __restrict__ output,
M_TYPE const * const __restrict__ input, M_TYPE const * const __restrict__ params,
    IND_TYPE X, IND_TYPE W,
    IND_TYPE Y, IND_TYPE H,
	IND_TYPE C, IND_TYPE F) {
/*
[V f; U (2, f); ULambda y; U (3, h); T (32, c); Hoist_vars [c]; T (3, w);
  T (112, x); Lambda_apply y [((Iter 9), (Arg 11)); ((Iter 1), (Arg 13))];
  T (1, c); T (1, y)]
*/
IND_TYPE c, cp_0, c447_p_0, cp_1, c447, w, wp_0, x, xp_0, y, yp_0, y408_p_0, yp_1, y408;

assert((Y == 112));
assert((X == 112));
assert((H == 3));
assert((W == 3));
assert((C == 32));
assert((F == 32));
IND_TYPE y409 = 0;
IND_TYPE x376 = 0;
IND_TYPE h = 0;
IND_TYPE w221 = 0;
IND_TYPE c448 = 0;
IND_TYPE f = 0;
float scal_0 ,scal_1 ,scal_10 ,scal_11 ,scal_12 ,scal_13 ,scal_14 ,scal_15 ,scal_16 ,scal_17 ,scal_18 ,scal_19 ,scal_2 ,scal_20 ,scal_21 ,scal_22 ,scal_23 ,scal_24 ,scal_25 ,scal_26 ,scal_27 ,scal_28 ,scal_29 ,scal_3 ,scal_30 ,scal_31 ,scal_32 ,scal_33 ,scal_34 ,scal_35 ,scal_36 ,scal_37 ,scal_38 ,scal_4 ,scal_5 ,scal_6 ,scal_7 ,scal_8 ,scal_9;
__m512 mem_vec_4175 ,mem_vec_4176 ,mem_vec_4177 ,mem_vec_4178 ,mem_vec_4179 ,mem_vec_4180 ,mem_vec_4181 ,mem_vec_4182 ,mem_vec_4183 ,mem_vec_4184 ,mem_vec_4185 ,mem_vec_4186 ,mem_vec_4187 ,mem_vec_4188 ,mem_vec_4189 ,mem_vec_4190 ,mem_vec_4191 ,mem_vec_4192 ,mem_vec_4193 ,mem_vec_4194 ,mem_vec_4195 ,mem_vec_4196 ,mem_vec_4197 ,mem_vec_4198 ,mem_vec_4199 ,mem_vec_4200 ,mem_vec_4201 ,mem_vec_4202 ,mem_vec_4203 ,mem_vec_4204 ,mem_vec_4205 ,mem_vec_4206 ,mem_vec_4207 ,mem_vec_4208 ,mem_vec_4209 ,mem_vec_4210 ,mem_vec_4211 ,mem_vec_4212 ,mem_vec_4213 ,mem_vec_4214 ,mem_vec_4215 ,mem_vec_4216 ,mem_vec_4217 ,mem_vec_4218 ,mem_vec_4219 ,mem_vec_4220 ,mem_vec_4221 ,mem_vec_4222 ,vec_0 ,vec_1 ,vec_10 ,vec_100 ,vec_101 ,vec_102 ,vec_103 ,vec_104 ,vec_105 ,vec_106 ,vec_107 ,vec_108 ,vec_109 ,vec_11 ,vec_110 ,vec_111 ,vec_112 ,vec_113 ,vec_114 ,vec_115 ,vec_116 ,vec_117 ,vec_118 ,vec_119 ,vec_12 ,vec_120 ,vec_121 ,vec_122 ,vec_13 ,vec_14 ,vec_15 ,vec_16 ,vec_17 ,vec_18 ,vec_19 ,vec_2 ,vec_20 ,vec_21 ,vec_22 ,vec_23 ,vec_24 ,vec_25 ,vec_26 ,vec_27 ,vec_28 ,vec_29 ,vec_3 ,vec_30 ,vec_31 ,vec_32 ,vec_33 ,vec_34 ,vec_35 ,vec_36 ,vec_37 ,vec_38 ,vec_39 ,vec_4 ,vec_40 ,vec_41 ,vec_42 ,vec_43 ,vec_44 ,vec_45 ,vec_46 ,vec_47 ,vec_48 ,vec_49 ,vec_5 ,vec_50 ,vec_51 ,vec_52 ,vec_53 ,vec_54 ,vec_55 ,vec_56 ,vec_57 ,vec_58 ,vec_59 ,vec_6 ,vec_60 ,vec_61 ,vec_62 ,vec_63 ,vec_64 ,vec_65 ,vec_66 ,vec_67 ,vec_68 ,vec_69 ,vec_7 ,vec_70 ,vec_71 ,vec_72 ,vec_73 ,vec_74 ,vec_75 ,vec_76 ,vec_77 ,vec_78 ,vec_79 ,vec_8 ,vec_80 ,vec_81 ,vec_82 ,vec_83 ,vec_84 ,vec_85 ,vec_86 ,vec_87 ,vec_88 ,vec_89 ,vec_9 ,vec_90 ,vec_91 ,vec_92 ,vec_93 ,vec_94 ,vec_95 ,vec_96 ,vec_97 ,vec_98 ,vec_99;
// y = 112, x = 112, h = 3, w = 3, c = 32, f = 32
// T (y, 1) (112 / 112)
for (y408 = y409, y408_p_0 = 0;
	y408 < y409 + 112;
	y408 += 112, y408_p_0 += 112){
	// y = 112, x = 112, h = 3, w = 3, c = 32, f = 32
	// T (c, 1) (32 / 32)
	for (c447 = c448, c447_p_0 = 0;
		c447 < c448 + 32;
		c447 += 32, c447_p_0 += 32){
			for (y = y408, yp_1 = y408_p_0, yp_0 = 0;
				y < y408 + 99;
				y += 11, yp_1 += 11, yp_0 += 11){
				// y = ph_y, x = 112, h = 3, w = 3, c = 32, f = 32
				// T (x, 112) (112 / 1)
				for (x = x376, xp_0 = 0;
					x < x376 + 112;
					x += 1, xp_0 += 1){
					// y = ph_y, x = 1, h = 3, w = 3, c = 32, f = 32
					// T (w, 3) (3 / 1)
					for (w = w221, wp_0 = 0;
						w < w221 + 3;
						w += 1, wp_0 += 1){
								mem_vec_4175 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f]);
								mem_vec_4176 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 16]);
								mem_vec_4177 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
								mem_vec_4178 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16]);
								mem_vec_4179 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
								mem_vec_4180 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16]);
								mem_vec_4181 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
								mem_vec_4182 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16]);
								mem_vec_4183 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f]);
								mem_vec_4184 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16]);
								mem_vec_4185 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f]);
								mem_vec_4186 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16]);
								mem_vec_4187 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f]);
								mem_vec_4188 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16]);
								mem_vec_4189 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 7) + f]);
								mem_vec_4190 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 7) + f + 16]);
								mem_vec_4191 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 8) + f]);
								mem_vec_4192 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 8) + f + 16]);
								mem_vec_4193 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 9) + f]);
								mem_vec_4194 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 9) + f + 16]);
								mem_vec_4195 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 10) + f]);
								mem_vec_4196 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 10) + f + 16]);
								// y = ph_y, x = 1, h = 3, w = 1, c = 32, f = 32
								// T (c, 32) (32 / 1)
								for (c = c447, cp_1 = c447_p_0, cp_0 = 0;
									c < c447 + 32;
									c += 1, cp_1 += 1, cp_0 += 1){
									scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
									vec_1 = _mm512_set1_ps(scal_0);
									vec_2 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

									vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_4175);
									mem_vec_4175 = vec_0;

									vec_4 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 16]);

									vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_4176);
									mem_vec_4176 = vec_3;
									scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
									vec_6 = _mm512_set1_ps(scal_1);


									vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_4177);
									mem_vec_4177 = vec_5;



									vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_4178);
									mem_vec_4178 = vec_7;
									scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
									vec_9 = _mm512_set1_ps(scal_2);


									vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_4179);
									mem_vec_4179 = vec_8;



									vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_4180);
									mem_vec_4180 = vec_10;
									scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
									vec_12 = _mm512_set1_ps(scal_3);


									vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_4181);
									mem_vec_4181 = vec_11;



									vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_4182);
									mem_vec_4182 = vec_13;
									scal_4 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h) + c];
									vec_15 = _mm512_set1_ps(scal_4);


									vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_4183);
									mem_vec_4183 = vec_14;



									vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_4184);
									mem_vec_4184 = vec_16;
									scal_5 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h) + c];
									vec_18 = _mm512_set1_ps(scal_5);


									vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_4185);
									mem_vec_4185 = vec_17;



									vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_4186);
									mem_vec_4186 = vec_19;
									scal_6 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h) + c];
									vec_21 = _mm512_set1_ps(scal_6);


									vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_4187);
									mem_vec_4187 = vec_20;



									vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_4188);
									mem_vec_4188 = vec_22;
									scal_7 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h) + c];
									vec_24 = _mm512_set1_ps(scal_7);


									vec_23 = _mm512_fmadd_ps(vec_24, vec_2, mem_vec_4189);
									mem_vec_4189 = vec_23;



									vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_4190);
									mem_vec_4190 = vec_25;
									scal_8 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 8 + h) + c];
									vec_27 = _mm512_set1_ps(scal_8);


									vec_26 = _mm512_fmadd_ps(vec_27, vec_2, mem_vec_4191);
									mem_vec_4191 = vec_26;



									vec_28 = _mm512_fmadd_ps(vec_27, vec_4, mem_vec_4192);
									mem_vec_4192 = vec_28;
									scal_9 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 9 + h) + c];
									vec_30 = _mm512_set1_ps(scal_9);


									vec_29 = _mm512_fmadd_ps(vec_30, vec_2, mem_vec_4193);
									mem_vec_4193 = vec_29;



									vec_31 = _mm512_fmadd_ps(vec_30, vec_4, mem_vec_4194);
									mem_vec_4194 = vec_31;
									scal_10 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 10 + h) + c];
									vec_33 = _mm512_set1_ps(scal_10);


									vec_32 = _mm512_fmadd_ps(vec_33, vec_2, mem_vec_4195);
									mem_vec_4195 = vec_32;



									vec_34 = _mm512_fmadd_ps(vec_33, vec_4, mem_vec_4196);
									mem_vec_4196 = vec_34;
									scal_11 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h + 1) + c];
									vec_36 = _mm512_set1_ps(scal_11);
									vec_37 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 1) + F * c + f]);

									vec_35 = _mm512_fmadd_ps(vec_36, vec_37, mem_vec_4175);
									mem_vec_4175 = vec_35;

									vec_39 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 1) + F * c + f + 16]);

									vec_38 = _mm512_fmadd_ps(vec_36, vec_39, mem_vec_4176);
									mem_vec_4176 = vec_38;
									scal_12 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h + 1) + c];
									vec_41 = _mm512_set1_ps(scal_12);


									vec_40 = _mm512_fmadd_ps(vec_41, vec_37, mem_vec_4177);
									mem_vec_4177 = vec_40;



									vec_42 = _mm512_fmadd_ps(vec_41, vec_39, mem_vec_4178);
									mem_vec_4178 = vec_42;
									scal_13 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h + 1) + c];
									vec_44 = _mm512_set1_ps(scal_13);


									vec_43 = _mm512_fmadd_ps(vec_44, vec_37, mem_vec_4179);
									mem_vec_4179 = vec_43;



									vec_45 = _mm512_fmadd_ps(vec_44, vec_39, mem_vec_4180);
									mem_vec_4180 = vec_45;
									scal_14 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h + 1) + c];
									vec_47 = _mm512_set1_ps(scal_14);


									vec_46 = _mm512_fmadd_ps(vec_47, vec_37, mem_vec_4181);
									mem_vec_4181 = vec_46;



									vec_48 = _mm512_fmadd_ps(vec_47, vec_39, mem_vec_4182);
									mem_vec_4182 = vec_48;
									scal_15 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h + 1) + c];
									vec_50 = _mm512_set1_ps(scal_15);


									vec_49 = _mm512_fmadd_ps(vec_50, vec_37, mem_vec_4183);
									mem_vec_4183 = vec_49;



									vec_51 = _mm512_fmadd_ps(vec_50, vec_39, mem_vec_4184);
									mem_vec_4184 = vec_51;
									scal_16 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h + 1) + c];
									vec_53 = _mm512_set1_ps(scal_16);


									vec_52 = _mm512_fmadd_ps(vec_53, vec_37, mem_vec_4185);
									mem_vec_4185 = vec_52;



									vec_54 = _mm512_fmadd_ps(vec_53, vec_39, mem_vec_4186);
									mem_vec_4186 = vec_54;
									scal_17 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h + 1) + c];
									vec_56 = _mm512_set1_ps(scal_17);


									vec_55 = _mm512_fmadd_ps(vec_56, vec_37, mem_vec_4187);
									mem_vec_4187 = vec_55;



									vec_57 = _mm512_fmadd_ps(vec_56, vec_39, mem_vec_4188);
									mem_vec_4188 = vec_57;
									scal_18 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h + 1) + c];
									vec_59 = _mm512_set1_ps(scal_18);


									vec_58 = _mm512_fmadd_ps(vec_59, vec_37, mem_vec_4189);
									mem_vec_4189 = vec_58;



									vec_60 = _mm512_fmadd_ps(vec_59, vec_39, mem_vec_4190);
									mem_vec_4190 = vec_60;
									scal_19 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 8 + h + 1) + c];
									vec_62 = _mm512_set1_ps(scal_19);


									vec_61 = _mm512_fmadd_ps(vec_62, vec_37, mem_vec_4191);
									mem_vec_4191 = vec_61;



									vec_63 = _mm512_fmadd_ps(vec_62, vec_39, mem_vec_4192);
									mem_vec_4192 = vec_63;
									scal_20 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 9 + h + 1) + c];
									vec_65 = _mm512_set1_ps(scal_20);


									vec_64 = _mm512_fmadd_ps(vec_65, vec_37, mem_vec_4193);
									mem_vec_4193 = vec_64;



									vec_66 = _mm512_fmadd_ps(vec_65, vec_39, mem_vec_4194);
									mem_vec_4194 = vec_66;
									scal_21 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 10 + h + 1) + c];
									vec_68 = _mm512_set1_ps(scal_21);


									vec_67 = _mm512_fmadd_ps(vec_68, vec_37, mem_vec_4195);
									mem_vec_4195 = vec_67;



									vec_69 = _mm512_fmadd_ps(vec_68, vec_39, mem_vec_4196);
									mem_vec_4196 = vec_69;
									scal_22 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h + 2) + c];
									vec_71 = _mm512_set1_ps(scal_22);
									vec_72 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 2) + F * c + f]);

									vec_70 = _mm512_fmadd_ps(vec_71, vec_72, mem_vec_4175);
									mem_vec_4175 = vec_70;

									vec_74 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 2) + F * c + f + 16]);

									vec_73 = _mm512_fmadd_ps(vec_71, vec_74, mem_vec_4176);
									mem_vec_4176 = vec_73;
									scal_23 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h + 2) + c];
									vec_76 = _mm512_set1_ps(scal_23);


									vec_75 = _mm512_fmadd_ps(vec_76, vec_72, mem_vec_4177);
									mem_vec_4177 = vec_75;



									vec_77 = _mm512_fmadd_ps(vec_76, vec_74, mem_vec_4178);
									mem_vec_4178 = vec_77;
									scal_24 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h + 2) + c];
									vec_79 = _mm512_set1_ps(scal_24);


									vec_78 = _mm512_fmadd_ps(vec_79, vec_72, mem_vec_4179);
									mem_vec_4179 = vec_78;



									vec_80 = _mm512_fmadd_ps(vec_79, vec_74, mem_vec_4180);
									mem_vec_4180 = vec_80;
									scal_25 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h + 2) + c];
									vec_82 = _mm512_set1_ps(scal_25);


									vec_81 = _mm512_fmadd_ps(vec_82, vec_72, mem_vec_4181);
									mem_vec_4181 = vec_81;



									vec_83 = _mm512_fmadd_ps(vec_82, vec_74, mem_vec_4182);
									mem_vec_4182 = vec_83;
									scal_26 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h + 2) + c];
									vec_85 = _mm512_set1_ps(scal_26);


									vec_84 = _mm512_fmadd_ps(vec_85, vec_72, mem_vec_4183);
									mem_vec_4183 = vec_84;



									vec_86 = _mm512_fmadd_ps(vec_85, vec_74, mem_vec_4184);
									mem_vec_4184 = vec_86;
									scal_27 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h + 2) + c];
									vec_88 = _mm512_set1_ps(scal_27);


									vec_87 = _mm512_fmadd_ps(vec_88, vec_72, mem_vec_4185);
									mem_vec_4185 = vec_87;



									vec_89 = _mm512_fmadd_ps(vec_88, vec_74, mem_vec_4186);
									mem_vec_4186 = vec_89;
									scal_28 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h + 2) + c];
									vec_91 = _mm512_set1_ps(scal_28);


									vec_90 = _mm512_fmadd_ps(vec_91, vec_72, mem_vec_4187);
									mem_vec_4187 = vec_90;



									vec_92 = _mm512_fmadd_ps(vec_91, vec_74, mem_vec_4188);
									mem_vec_4188 = vec_92;
									scal_29 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h + 2) + c];
									vec_94 = _mm512_set1_ps(scal_29);


									vec_93 = _mm512_fmadd_ps(vec_94, vec_72, mem_vec_4189);
									mem_vec_4189 = vec_93;



									vec_95 = _mm512_fmadd_ps(vec_94, vec_74, mem_vec_4190);
									mem_vec_4190 = vec_95;
									scal_30 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 8 + h + 2) + c];
									vec_97 = _mm512_set1_ps(scal_30);


									vec_96 = _mm512_fmadd_ps(vec_97, vec_72, mem_vec_4191);
									mem_vec_4191 = vec_96;



									vec_98 = _mm512_fmadd_ps(vec_97, vec_74, mem_vec_4192);
									mem_vec_4192 = vec_98;
									scal_31 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 9 + h + 2) + c];
									vec_100 = _mm512_set1_ps(scal_31);


									vec_99 = _mm512_fmadd_ps(vec_100, vec_72, mem_vec_4193);
									mem_vec_4193 = vec_99;



									vec_101 = _mm512_fmadd_ps(vec_100, vec_74, mem_vec_4194);
									mem_vec_4194 = vec_101;
									scal_32 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 10 + h + 2) + c];
									vec_103 = _mm512_set1_ps(scal_32);


									vec_102 = _mm512_fmadd_ps(vec_103, vec_72, mem_vec_4195);
									mem_vec_4195 = vec_102;



									vec_104 = _mm512_fmadd_ps(vec_103, vec_74, mem_vec_4196);
									mem_vec_4196 = vec_104;
								}
							_mm512_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_4175);
							_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 16], mem_vec_4176);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_4177);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16], mem_vec_4178);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_4179);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16], mem_vec_4180);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_4181);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16], mem_vec_4182);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f], mem_vec_4183);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16], mem_vec_4184);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f], mem_vec_4185);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16], mem_vec_4186);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f], mem_vec_4187);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16], mem_vec_4188);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 7) + f], mem_vec_4189);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 7) + f + 16], mem_vec_4190);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 8) + f], mem_vec_4191);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 8) + f + 16], mem_vec_4192);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 9) + f], mem_vec_4193);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 9) + f + 16], mem_vec_4194);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 10) + f], mem_vec_4195);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 10) + f + 16], mem_vec_4196);
					}
				}
			}
			for (y = y408 + 99, yp_1 = y408_p_0, yp_0 = 0;
				y < y408 + 99 + 13;
				y += 13, yp_1 += 13, yp_0 += 13){
				// y = ph_y, x = 112, h = 3, w = 3, c = 32, f = 32
				// T (x, 112) (112 / 1)
				for (x = x376, xp_0 = 0;
					x < x376 + 112;
					x += 1, xp_0 += 1){
					// y = ph_y, x = 1, h = 3, w = 3, c = 32, f = 32
					// T (w, 3) (3 / 1)
					for (w = w221, wp_0 = 0;
						w < w221 + 3;
						w += 1, wp_0 += 1){
								mem_vec_4197 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f]);
								mem_vec_4198 = _mm512_loadu_ps(&output[(F * Y) * x + F * y + f + 16]);
								mem_vec_4199 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f]);
								mem_vec_4200 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16]);
								mem_vec_4201 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f]);
								mem_vec_4202 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16]);
								mem_vec_4203 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f]);
								mem_vec_4204 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16]);
								mem_vec_4205 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f]);
								mem_vec_4206 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16]);
								mem_vec_4207 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f]);
								mem_vec_4208 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16]);
								mem_vec_4209 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f]);
								mem_vec_4210 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16]);
								mem_vec_4211 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 7) + f]);
								mem_vec_4212 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 7) + f + 16]);
								mem_vec_4213 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 8) + f]);
								mem_vec_4214 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 8) + f + 16]);
								mem_vec_4215 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 9) + f]);
								mem_vec_4216 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 9) + f + 16]);
								mem_vec_4217 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 10) + f]);
								mem_vec_4218 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 10) + f + 16]);
								mem_vec_4219 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 11) + f]);
								mem_vec_4220 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 11) + f + 16]);
								mem_vec_4221 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 12) + f]);
								mem_vec_4222 = _mm512_loadu_ps(&output[(F * Y) * x + F * (y + 12) + f + 16]);
								// y = ph_y, x = 1, h = 3, w = 1, c = 32, f = 32
								// T (c, 32) (32 / 1)
								for (c = c447, cp_1 = c447_p_0, cp_0 = 0;
									c < c447 + 32;
									c += 1, cp_1 += 1, cp_0 += 1){
									scal_0 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h) + c];
									vec_1 = _mm512_set1_ps(scal_0);
									vec_2 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f]);

									vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_4197);
									mem_vec_4197 = vec_0;

									vec_4 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * h + F * c + f + 16]);

									vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_4198);
									mem_vec_4198 = vec_3;
									scal_1 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h) + c];
									vec_6 = _mm512_set1_ps(scal_1);


									vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_4199);
									mem_vec_4199 = vec_5;



									vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_4200);
									mem_vec_4200 = vec_7;
									scal_2 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h) + c];
									vec_9 = _mm512_set1_ps(scal_2);


									vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_4201);
									mem_vec_4201 = vec_8;



									vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_4202);
									mem_vec_4202 = vec_10;
									scal_3 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h) + c];
									vec_12 = _mm512_set1_ps(scal_3);


									vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_4203);
									mem_vec_4203 = vec_11;



									vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_4204);
									mem_vec_4204 = vec_13;
									scal_4 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h) + c];
									vec_15 = _mm512_set1_ps(scal_4);


									vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_4205);
									mem_vec_4205 = vec_14;



									vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_4206);
									mem_vec_4206 = vec_16;
									scal_5 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h) + c];
									vec_18 = _mm512_set1_ps(scal_5);


									vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_4207);
									mem_vec_4207 = vec_17;



									vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_4208);
									mem_vec_4208 = vec_19;
									scal_6 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h) + c];
									vec_21 = _mm512_set1_ps(scal_6);


									vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_4209);
									mem_vec_4209 = vec_20;



									vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_4210);
									mem_vec_4210 = vec_22;
									scal_7 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h) + c];
									vec_24 = _mm512_set1_ps(scal_7);


									vec_23 = _mm512_fmadd_ps(vec_24, vec_2, mem_vec_4211);
									mem_vec_4211 = vec_23;



									vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_4212);
									mem_vec_4212 = vec_25;
									scal_8 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 8 + h) + c];
									vec_27 = _mm512_set1_ps(scal_8);


									vec_26 = _mm512_fmadd_ps(vec_27, vec_2, mem_vec_4213);
									mem_vec_4213 = vec_26;



									vec_28 = _mm512_fmadd_ps(vec_27, vec_4, mem_vec_4214);
									mem_vec_4214 = vec_28;
									scal_9 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 9 + h) + c];
									vec_30 = _mm512_set1_ps(scal_9);


									vec_29 = _mm512_fmadd_ps(vec_30, vec_2, mem_vec_4215);
									mem_vec_4215 = vec_29;



									vec_31 = _mm512_fmadd_ps(vec_30, vec_4, mem_vec_4216);
									mem_vec_4216 = vec_31;
									scal_10 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 10 + h) + c];
									vec_33 = _mm512_set1_ps(scal_10);


									vec_32 = _mm512_fmadd_ps(vec_33, vec_2, mem_vec_4217);
									mem_vec_4217 = vec_32;



									vec_34 = _mm512_fmadd_ps(vec_33, vec_4, mem_vec_4218);
									mem_vec_4218 = vec_34;
									scal_11 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 11 + h) + c];
									vec_36 = _mm512_set1_ps(scal_11);


									vec_35 = _mm512_fmadd_ps(vec_36, vec_2, mem_vec_4219);
									mem_vec_4219 = vec_35;



									vec_37 = _mm512_fmadd_ps(vec_36, vec_4, mem_vec_4220);
									mem_vec_4220 = vec_37;
									scal_12 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 12 + h) + c];
									vec_39 = _mm512_set1_ps(scal_12);


									vec_38 = _mm512_fmadd_ps(vec_39, vec_2, mem_vec_4221);
									mem_vec_4221 = vec_38;



									vec_40 = _mm512_fmadd_ps(vec_39, vec_4, mem_vec_4222);
									mem_vec_4222 = vec_40;
									scal_13 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h + 1) + c];
									vec_42 = _mm512_set1_ps(scal_13);
									vec_43 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 1) + F * c + f]);

									vec_41 = _mm512_fmadd_ps(vec_42, vec_43, mem_vec_4197);
									mem_vec_4197 = vec_41;

									vec_45 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 1) + F * c + f + 16]);

									vec_44 = _mm512_fmadd_ps(vec_42, vec_45, mem_vec_4198);
									mem_vec_4198 = vec_44;
									scal_14 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h + 1) + c];
									vec_47 = _mm512_set1_ps(scal_14);


									vec_46 = _mm512_fmadd_ps(vec_47, vec_43, mem_vec_4199);
									mem_vec_4199 = vec_46;



									vec_48 = _mm512_fmadd_ps(vec_47, vec_45, mem_vec_4200);
									mem_vec_4200 = vec_48;
									scal_15 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h + 1) + c];
									vec_50 = _mm512_set1_ps(scal_15);


									vec_49 = _mm512_fmadd_ps(vec_50, vec_43, mem_vec_4201);
									mem_vec_4201 = vec_49;



									vec_51 = _mm512_fmadd_ps(vec_50, vec_45, mem_vec_4202);
									mem_vec_4202 = vec_51;
									scal_16 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h + 1) + c];
									vec_53 = _mm512_set1_ps(scal_16);


									vec_52 = _mm512_fmadd_ps(vec_53, vec_43, mem_vec_4203);
									mem_vec_4203 = vec_52;



									vec_54 = _mm512_fmadd_ps(vec_53, vec_45, mem_vec_4204);
									mem_vec_4204 = vec_54;
									scal_17 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h + 1) + c];
									vec_56 = _mm512_set1_ps(scal_17);


									vec_55 = _mm512_fmadd_ps(vec_56, vec_43, mem_vec_4205);
									mem_vec_4205 = vec_55;



									vec_57 = _mm512_fmadd_ps(vec_56, vec_45, mem_vec_4206);
									mem_vec_4206 = vec_57;
									scal_18 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h + 1) + c];
									vec_59 = _mm512_set1_ps(scal_18);


									vec_58 = _mm512_fmadd_ps(vec_59, vec_43, mem_vec_4207);
									mem_vec_4207 = vec_58;



									vec_60 = _mm512_fmadd_ps(vec_59, vec_45, mem_vec_4208);
									mem_vec_4208 = vec_60;
									scal_19 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h + 1) + c];
									vec_62 = _mm512_set1_ps(scal_19);


									vec_61 = _mm512_fmadd_ps(vec_62, vec_43, mem_vec_4209);
									mem_vec_4209 = vec_61;



									vec_63 = _mm512_fmadd_ps(vec_62, vec_45, mem_vec_4210);
									mem_vec_4210 = vec_63;
									scal_20 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h + 1) + c];
									vec_65 = _mm512_set1_ps(scal_20);


									vec_64 = _mm512_fmadd_ps(vec_65, vec_43, mem_vec_4211);
									mem_vec_4211 = vec_64;



									vec_66 = _mm512_fmadd_ps(vec_65, vec_45, mem_vec_4212);
									mem_vec_4212 = vec_66;
									scal_21 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 8 + h + 1) + c];
									vec_68 = _mm512_set1_ps(scal_21);


									vec_67 = _mm512_fmadd_ps(vec_68, vec_43, mem_vec_4213);
									mem_vec_4213 = vec_67;



									vec_69 = _mm512_fmadd_ps(vec_68, vec_45, mem_vec_4214);
									mem_vec_4214 = vec_69;
									scal_22 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 9 + h + 1) + c];
									vec_71 = _mm512_set1_ps(scal_22);


									vec_70 = _mm512_fmadd_ps(vec_71, vec_43, mem_vec_4215);
									mem_vec_4215 = vec_70;



									vec_72 = _mm512_fmadd_ps(vec_71, vec_45, mem_vec_4216);
									mem_vec_4216 = vec_72;
									scal_23 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 10 + h + 1) + c];
									vec_74 = _mm512_set1_ps(scal_23);


									vec_73 = _mm512_fmadd_ps(vec_74, vec_43, mem_vec_4217);
									mem_vec_4217 = vec_73;



									vec_75 = _mm512_fmadd_ps(vec_74, vec_45, mem_vec_4218);
									mem_vec_4218 = vec_75;
									scal_24 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 11 + h + 1) + c];
									vec_77 = _mm512_set1_ps(scal_24);


									vec_76 = _mm512_fmadd_ps(vec_77, vec_43, mem_vec_4219);
									mem_vec_4219 = vec_76;



									vec_78 = _mm512_fmadd_ps(vec_77, vec_45, mem_vec_4220);
									mem_vec_4220 = vec_78;
									scal_25 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 12 + h + 1) + c];
									vec_80 = _mm512_set1_ps(scal_25);


									vec_79 = _mm512_fmadd_ps(vec_80, vec_43, mem_vec_4221);
									mem_vec_4221 = vec_79;



									vec_81 = _mm512_fmadd_ps(vec_80, vec_45, mem_vec_4222);
									mem_vec_4222 = vec_81;
									scal_26 = input[(C * (Y + H - 1)) * (x + w) + C * (y + h + 2) + c];
									vec_83 = _mm512_set1_ps(scal_26);
									vec_84 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 2) + F * c + f]);

									vec_82 = _mm512_fmadd_ps(vec_83, vec_84, mem_vec_4197);
									mem_vec_4197 = vec_82;

									vec_86 = _mm512_loadu_ps(&params[((F * C) * H) * w + (F * C) * (h + 2) + F * c + f + 16]);

									vec_85 = _mm512_fmadd_ps(vec_83, vec_86, mem_vec_4198);
									mem_vec_4198 = vec_85;
									scal_27 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 1 + h + 2) + c];
									vec_88 = _mm512_set1_ps(scal_27);


									vec_87 = _mm512_fmadd_ps(vec_88, vec_84, mem_vec_4199);
									mem_vec_4199 = vec_87;



									vec_89 = _mm512_fmadd_ps(vec_88, vec_86, mem_vec_4200);
									mem_vec_4200 = vec_89;
									scal_28 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 2 + h + 2) + c];
									vec_91 = _mm512_set1_ps(scal_28);


									vec_90 = _mm512_fmadd_ps(vec_91, vec_84, mem_vec_4201);
									mem_vec_4201 = vec_90;



									vec_92 = _mm512_fmadd_ps(vec_91, vec_86, mem_vec_4202);
									mem_vec_4202 = vec_92;
									scal_29 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 3 + h + 2) + c];
									vec_94 = _mm512_set1_ps(scal_29);


									vec_93 = _mm512_fmadd_ps(vec_94, vec_84, mem_vec_4203);
									mem_vec_4203 = vec_93;



									vec_95 = _mm512_fmadd_ps(vec_94, vec_86, mem_vec_4204);
									mem_vec_4204 = vec_95;
									scal_30 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 4 + h + 2) + c];
									vec_97 = _mm512_set1_ps(scal_30);


									vec_96 = _mm512_fmadd_ps(vec_97, vec_84, mem_vec_4205);
									mem_vec_4205 = vec_96;



									vec_98 = _mm512_fmadd_ps(vec_97, vec_86, mem_vec_4206);
									mem_vec_4206 = vec_98;
									scal_31 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 5 + h + 2) + c];
									vec_100 = _mm512_set1_ps(scal_31);


									vec_99 = _mm512_fmadd_ps(vec_100, vec_84, mem_vec_4207);
									mem_vec_4207 = vec_99;



									vec_101 = _mm512_fmadd_ps(vec_100, vec_86, mem_vec_4208);
									mem_vec_4208 = vec_101;
									scal_32 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 6 + h + 2) + c];
									vec_103 = _mm512_set1_ps(scal_32);


									vec_102 = _mm512_fmadd_ps(vec_103, vec_84, mem_vec_4209);
									mem_vec_4209 = vec_102;



									vec_104 = _mm512_fmadd_ps(vec_103, vec_86, mem_vec_4210);
									mem_vec_4210 = vec_104;
									scal_33 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 7 + h + 2) + c];
									vec_106 = _mm512_set1_ps(scal_33);


									vec_105 = _mm512_fmadd_ps(vec_106, vec_84, mem_vec_4211);
									mem_vec_4211 = vec_105;



									vec_107 = _mm512_fmadd_ps(vec_106, vec_86, mem_vec_4212);
									mem_vec_4212 = vec_107;
									scal_34 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 8 + h + 2) + c];
									vec_109 = _mm512_set1_ps(scal_34);


									vec_108 = _mm512_fmadd_ps(vec_109, vec_84, mem_vec_4213);
									mem_vec_4213 = vec_108;



									vec_110 = _mm512_fmadd_ps(vec_109, vec_86, mem_vec_4214);
									mem_vec_4214 = vec_110;
									scal_35 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 9 + h + 2) + c];
									vec_112 = _mm512_set1_ps(scal_35);


									vec_111 = _mm512_fmadd_ps(vec_112, vec_84, mem_vec_4215);
									mem_vec_4215 = vec_111;



									vec_113 = _mm512_fmadd_ps(vec_112, vec_86, mem_vec_4216);
									mem_vec_4216 = vec_113;
									scal_36 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 10 + h + 2) + c];
									vec_115 = _mm512_set1_ps(scal_36);


									vec_114 = _mm512_fmadd_ps(vec_115, vec_84, mem_vec_4217);
									mem_vec_4217 = vec_114;



									vec_116 = _mm512_fmadd_ps(vec_115, vec_86, mem_vec_4218);
									mem_vec_4218 = vec_116;
									scal_37 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 11 + h + 2) + c];
									vec_118 = _mm512_set1_ps(scal_37);


									vec_117 = _mm512_fmadd_ps(vec_118, vec_84, mem_vec_4219);
									mem_vec_4219 = vec_117;



									vec_119 = _mm512_fmadd_ps(vec_118, vec_86, mem_vec_4220);
									mem_vec_4220 = vec_119;
									scal_38 = input[(C * (Y + H - 1)) * (x + w) + C * (y + 12 + h + 2) + c];
									vec_121 = _mm512_set1_ps(scal_38);


									vec_120 = _mm512_fmadd_ps(vec_121, vec_84, mem_vec_4221);
									mem_vec_4221 = vec_120;



									vec_122 = _mm512_fmadd_ps(vec_121, vec_86, mem_vec_4222);
									mem_vec_4222 = vec_122;
								}
							_mm512_storeu_ps(&output[(F * Y) * x + F * y + f], mem_vec_4197);
							_mm512_storeu_ps(&output[(F * Y) * x + F * y + f + 16], mem_vec_4198);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f], mem_vec_4199);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 1) + f + 16], mem_vec_4200);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f], mem_vec_4201);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 2) + f + 16], mem_vec_4202);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f], mem_vec_4203);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 3) + f + 16], mem_vec_4204);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f], mem_vec_4205);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 4) + f + 16], mem_vec_4206);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f], mem_vec_4207);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 5) + f + 16], mem_vec_4208);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f], mem_vec_4209);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 6) + f + 16], mem_vec_4210);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 7) + f], mem_vec_4211);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 7) + f + 16], mem_vec_4212);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 8) + f], mem_vec_4213);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 8) + f + 16], mem_vec_4214);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 9) + f], mem_vec_4215);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 9) + f + 16], mem_vec_4216);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 10) + f], mem_vec_4217);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 10) + f + 16], mem_vec_4218);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 11) + f], mem_vec_4219);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 11) + f + 16], mem_vec_4220);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 12) + f], mem_vec_4221);
							_mm512_storeu_ps(&output[(F * Y) * x + F * (y + 12) + f + 16], mem_vec_4222);
					}
				}
			}
	}
}


}