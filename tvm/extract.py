
result = {
    "MobilNet_01": {"autotvm": {}, "tvmttile": {}},
    "MobilNet_02": {"autotvm": {}, "tvmttile": {}},
    "MobilNet_03": {"autotvm": {}, "tvmttile": {}},
    "MobilNet_04": {"autotvm": {}, "tvmttile": {}},
    "MobilNet_05": {"autotvm": {}, "tvmttile": {}},
    "MobilNet_06": {"autotvm": {}, "tvmttile": {}},
    "MobilNet_07": {"autotvm": {}, "tvmttile": {}},
    "MobilNet_08": {"autotvm": {}, "tvmttile": {}},
    "MobilNet_09": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_01": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_02": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_03": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_04": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_05": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_06": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_07": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_08": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_09": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_10": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_11": {"autotvm": {}, "tvmttile": {}},
    "ResNet18_12": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_00": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_02": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_04": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_05": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_08": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_09": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_12": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_13": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_18": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_19": {"autotvm": {}, "tvmttile": {}},
    "Yolo9000_23": {"autotvm": {}, "tvmttile": {}}
}

for name_archi in ["XeonGold6130"]:
    for nbthread in [1, 2, 4, 8, 16, 32]:
        file_ = open(f"autotvm/results/result_1_{nbthread}_{"avx512"}_{name_archi}.csv", "r")
        for line in file_:
            l = line.split(";")
            result[l[0]]["autotvm"][nbthread] = float(l[1])
        file_.close()
