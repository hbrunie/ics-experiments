open Batteries
open Spec
module Ag = Angstrom
open Parse_common


let vec vec_size = let open Ag in
  string "V " *> alpha
  >>| fun d -> UTV (vec_size, dim_from_str d)

let tile_with_prefix prefix = let open Ag in
  string prefix
  *> both (integer <* whitespace <* char ',' <* whitespace)
  (alpha <* char ')')
  >>| fun (s, c) -> UTV (s, dim_from_str c)

let tile = tile_with_prefix "T ("

let unroll = tile_with_prefix "U ("

let ulambda = let open Ag in 
  string "ULambda " *> alpha
  >>| fun d -> ULambda (dim_from_str d)


let p_list value = let open Ag in
  char '[' *> whitespace *>
  sep_by (whitespace <* char ';' <* whitespace) (enclose_ws value)
  <* whitespace <* ((char ']') <|> (char ';' <* whitespace <* char ']'))

let enclose_par value =let open Ag in
char '(' *> value <* char ')'

let iter = enclose_par Ag.(string "Iter " *> integer)
let arg = enclose_par Ag.(string "Arg " *> integer)

let iter_arg = Ag.(enclose_par (both iter (char ',' *> whitespace *> arg)))

let lambda_apply = Ag.(
    string "Lambda_apply " *> alpha
    >>= fun c ->
    (whitespace *> p_list iter_arg
     >>| fun l_arg -> Apply (dim_from_str c, l_arg))
  )

let hoist_var = let open Ag in
  string "Hoist_vars [" *> alpha *> char ']'
  >>| Fun.const Other

let list_spec vec_size =
  p_list (Ag.choice [tile; unroll; vec vec_size; hoist_var; ulambda; lambda_apply])

let parse_single_scheme vec_size = let open Ag in
  (*Result.get_ok %*) parse_string ~consume:All (list_spec vec_size)
    %> function Ok a -> a | Error s -> failwith s

let all vec_size =
  Ag.(enclose_ws (both (integer <* string " : " ) (consumed @@ list_spec vec_size) ))

let parse_scheme vec_size filename =
  File.with_file_in filename
    IO.read_all
  |> Ag.(parse_string ~consume:All (many (all vec_size))
         %> Result.map
              (* Reparse s to get a spec list. This is a waste of time, but do the job for now *)
           ( List.map (fun (i, s) -> i, s, parse_string ~consume:All (list_spec vec_size) s |> Result.get_ok))
        )
  |> Utils.tee (function Error e -> print_endline e | _ -> ())
  |> Result.get_ok
