open Batteries

(* Way too complicated for such a simple thing.
 * Take a list of list
 * and returns the list of (list of first elements, list of second element, list of third...) *)
let zip_all l =
  let take_heads ll =
    List.fold_right (fun next acc ->
        Option.bind acc
          (fun (hs, ts) -> match next with
               [] -> None
             | h'::t' -> Some (h'::hs, t'::ts)
          ))
      ll (Some ([], [])) in
  let rec loop acc ll =
    match take_heads ll with None -> List.rev acc
                           | Some (heads, tails) -> loop (heads::acc) tails in
  loop [] l

let split_list p =
  let rec loop acc = function
    | h::t  ->
      (match p h with Some x -> List.rev acc, Some (x, t)
                    | None ->loop (h::acc) t)
    | [] -> List.rev acc, None in
  loop []


let tee f x = f x; x

let min_max_by ?(cmp=Stdlib.compare) metric l =
  let cmp a1 a2 = cmp (metric a1) (metric a2) in
  List.min_max ~cmp l

let min_by ?(cmp=Stdlib.compare) metric = min_max_by ~cmp metric %> fst

let max_by ?(cmp=Stdlib.compare) metric = min_max_by ~cmp metric %> snd

let max_idx cmp l =
  List.mapi (fun i a -> i, a) l
  |> List.reduce
    (fun (imax, vmax) (i2, v2) ->
      if cmp v2 vmax >= 0 then i2, v2 else imax, vmax)

let bimap f g (x, y) = f x, g y
let map_fst f  = bimap f Fun.id
let map_snd f  = bimap Fun.id f
