open Spec
open Batteries
open Either

let cache_line_size = 16
let cache_line_sizef = 16.
let rec scanl f init = function h::t -> let acc = f init h in acc :: (scanl f acc t) | [] -> []

let enum_dim = [F; C; Y; X; H; W;] 

let init_list = List.map (fun d -> d, 1) enum_dim

let input_footprint _f c y x h w  =
  let c' = int_of_float @@ Float.ceil (float_of_int c /. cache_line_sizef) in
  (x + w - 1) * (y + h - 1) * c'

let param_footprint f c _y _x h w   =
  let f' = Int.max 1 (f / cache_line_size) in
  w * h * c * f'

let output_footprint f _c y x _h _w  =
  let f' = Int.max 1 (f / cache_line_size) in
  x * y * f'

type footprint = {input : int; params: int;
                  output : int; global: int;
                  volume: int;
                 } [@@deriving show {with_path = false}]

let get fp_list dim = List.assoc dim fp_list

let fp_map f dim fp_list = List.modify dim f fp_list

let get_all_dim fp_list =
  let get = get fp_list in
  get F, get C, get Y, get X, get H, get W

let compute_fp fp_list =
  let f, c, y, x, h, w = get_all_dim fp_list in
  let apply fn = fn f c y x h w in
  let map3 f g h = apply f, apply g, apply h in
  let input, params, output = map3 input_footprint param_footprint output_footprint in
  {input; params; output;
   global = input + params + output;
   volume = input + params + 2 * output;}

let embed l = List.map (fun x -> [x]) l

type dim_footprint = (dim * int) list [@@deriving show {with_path = false}]

type dim_fp_iter = dim_footprint list [@@deriving show {with_path = false}]

let show_either pa pb = Either.(function Left a -> "Left " ^ (pa a)
                               | Right b -> "Right " ^ (pb b))

type branch_fp = (dim_fp_iter, dim_fp_iter * dim_fp_iter)  Either.t

let show_either_fp = show_either [%show: dim_fp_iter] [%show: dim_fp_iter * dim_fp_iter]

(* Get footprint list *)

let footprint_either l : (dim_fp_iter, dim_fp_iter * dim_fp_iter)  Either.t =
  let level_sizes lambda_value init =
    scanl (fun fp -> function UTV (s, d) -> fp_map (( * ) s) d fp
                            | ULambda d -> let value = Option.get lambda_value in
                              fp_map (( * ) value) d fp
                            | Other -> fp
                            | Apply _ -> failwith "Unreachable") init  in
  match Utils.split_list (function Apply (d, l) -> Some (d, l) | _ -> None) l with
    l, Some ((d, iter_args), remaining) ->
    let iter_left, arg_left, iter_right, arg_right =
      match iter_args with [i1, a1; i2, a2] -> i1, a1, i2, a2 | _ -> failwith "Unreachable" in
    let inner_level_left, inner_level_right  =
      let f arg = level_sizes (Some arg) init_list l in
      f arg_left, f arg_right in
    let last_left = List.last inner_level_left
    and last_right = List.last inner_level_right in
    let compute_remaining next = level_sizes None next in 
    let gather inner iter last =
      let next =fp_map (( * ) iter) d last in
      inner @ [next] @ compute_remaining next remaining in
    Right (gather inner_level_left iter_left last_left,
           gather inner_level_right iter_right last_right)
  | l, None ->
    Left (level_sizes None init_list l)

let spec_fp_either l  = 
  let dispatch = List.map2 (fun x y -> x, y) l in
  match (footprint_either l) with
    Left loops -> Left (dispatch loops )
 | Right  (lleft, lright) -> Right (dispatch lleft, dispatch lright)

let show_spec_fp_either =
  show_either [%show: (spec * dim_footprint) list] [%show: (spec * dim_footprint) list * (spec * dim_footprint) list]

let forget_spec = function Left l -> Left (List.map snd l)
                         | Right (l1 , l2 ) -> Right (List.map snd l1, List.map snd l2)

let arithmetic_intensity fp_list =
  let f, c, y, x, h, w = get_all_dim fp_list in
  f * c * y * x * h * w

let reduction_size = List.find_map (function UTV (s, C)  -> Some s | _ -> None)

let out_of_cache cache_size fp =
  compute_fp fp 
  |> fun {global;_} -> global >= cache_size

let level_dim_mul level1 level2 =
    List.fold_left2 (fun prod (d1, l1) (d2, l2) ->
      assert (equal_dim d1 d2);  prod * (l2 / l1))
      1 level1 level2

let data_volume_cache_level cache_size levels = let open Either in
  let volume x = let {volume;_} = compute_fp x in volume in
  let compute_list loops =
    let l_in_cache, l_out_cache = List.span (Bool.not % out_of_cache cache_size) loops in
    print_endline @@ [%show: (dim * int) list list] l_in_cache;
    print_newline ();
    print_endline "After Cache";
    print_endline @@ [%show: (dim * int) list list] l_out_cache;
    match l_out_cache with
      | [] ->  List.last l_in_cache  |> volume
      | (first_out :: nexts) ->
        match List.Exceptionless.last nexts     
          with
            Some last_level -> level_dim_mul first_out last_level * volume first_out
          | None  ->   volume first_out in
  match levels with 
    Left loops -> compute_list loops
  | Right (loops_left, loops_right) ->
    compute_list loops_left + compute_list loops_right
