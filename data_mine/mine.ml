open Batteries
open Parse_scheme
open Parse_csv

let bench_names csv_location =
  let open Shexp_process in
  let open Shexp_process.Infix in
  let module List = Batteries.List in
  eval (readdir csv_location
        >>| List.filter_map
          (fun s ->
             Re.(let re = compile @@ seq [group (rep1 any); str ".csv";rep space; eol] in
                 exec_opt re s
                 |> Option.map (fun g -> Group.get g 1)
                )
          )
       )

let get_tiles_values csv_location  bench_name =
      let csv = bench_name ^ ".csv"
      and tiles = bench_name ^ "_tiles.log" in
      let prefix file = csv_location  ^ file in
       parse_scheme 8 (prefix tiles), parse_csv (prefix csv)

let best_scheme tiles values =
      let perfs = List.assoc "perf" values |> List.map (Option.get % Either.find_left) in
      let best_idx, best = Utils.max_idx Float.compare perfs in
      best_idx, best, List.at tiles best_idx

let rank_cache tiles values =
  let best_idx, _, (_, _, tile) = best_scheme tiles values in
  let red_size = Loop_levels.reduction_size tile in
  let best_l_miss_size misses =  List.at misses best_idx in
  let l1_misses = List.assoc "CACHE_MISS_L1" values |> List.map (Option.get % Either.find_right)
  and l2_misses = List.assoc "CACHE_MISS_L2" values |> List.map (Option.get % Either.find_right) in
  let best_l1_miss, best_l2_miss =
    best_l_miss_size l1_misses, best_l_miss_size l2_misses in 
  let same_red_values misses =
    List.fold_right2
      (fun (_, _, t) miss_line misses ->
      if Loop_levels.reduction_size t = red_size then miss_line::misses else misses)
      tiles misses [] in
  let same_red_l1_misses = same_red_values l1_misses
  and same_red_l2_misses = same_red_values l2_misses in
  let count v l = List.fold_left
      (fun (less, more) v' -> if (v < v') then (less, more + 1) else (less + 1, more))
      (0, 0) l in
  let better_l1, worse_l1 = count best_l1_miss l1_misses
  and better_l2, worse_l2 = count  best_l2_miss l2_misses
  and better_l1_same_red, worse_l1_same_red = count  best_l1_miss same_red_l1_misses
  and better_l2_same_red, worse_l2_same_red = count  best_l2_miss same_red_l2_misses in
  better_l1, worse_l1,
  better_l2, worse_l2,
  better_l1_same_red, worse_l1_same_red,
  better_l2_same_red, worse_l2_same_red

let l1_size_float = 8192
let l1_size = l1_size_float / 16
let l2_size_float = 262144
let l2_size = l2_size_float / 16

let data_volumes tile =
  let fp_list = Loop_levels.footprint_either tile in
  let l1_volume = Loop_levels.data_volume_cache_level l1_size fp_list in
  let l2_volume = Loop_levels.data_volume_cache_level l2_size fp_list in
  l1_volume, l2_volume


let write_csv_volumes csv_location =
  List.iter ( fun name ->
      let csv_name = name ^ "_volumes.csv" in
      File.with_file_out csv_name @@ fun f ->
      Printf.fprintf f "L1 volume, L2 volume\n";
      let tiles, _ = get_tiles_values csv_location name in List.iter (fun (_, _, scheme) ->
          let l1_volume, l2_volume = data_volumes scheme in
          Printf.fprintf f "%d, %d\n" l1_volume l2_volume) tiles
    ) (bench_names csv_location)

let print_best csv_location =
List.iter (fun name ->
      let schemes, values = get_tiles_values csv_location name in
      let perfs = List.assoc "perf" values |> List.map (Option.get % Either.find_left) in
      let best_idx, best = Utils.max_idx Float.compare perfs in
      let _, scheme, _ = List.at schemes best_idx in
      Printf.printf "%s, %.3f\n%s\n" name
        best scheme
    ) (bench_names csv_location)

let rank_misses csv_location =
  List.iter (fun name ->
      let tiles, values = get_tiles_values csv_location name in
      let better_l1, worse_l1,
          better_l2, worse_l2,
          better_l1_same_red, worse_l1_same_red,
          better_l2_same_red, worse_l2_same_red =
        rank_cache tiles values in
      Printf.printf "%s\nGlobal:\n\tL1 : %d / %d,\n\tL2 : %d / %d\n"
        name
        better_l1 (better_l1 + worse_l1 + 1)
        better_l2 (better_l2 + worse_l2 + 1);
      Printf.printf "Same reduction :\n\tL1  : %d / %d,\n\tL2 : %d / %d\n\n"
        better_l1_same_red (better_l1_same_red + worse_l1_same_red + 1)
        better_l2_same_red (better_l2_same_red + worse_l2_same_red + 1);
    ) (bench_names csv_location)

let all_together csv_location = 
  List.iter (fun name ->
      let csv_name = name ^ "_all.csv" in
      File.with_file_out csv_name @@ fun f ->
      Printf.fprintf f "perf, L1 miss, L2 miss, L1 volume, L2 volume, scheme\n";
      let schemes, values = get_tiles_values csv_location name in
      let perfs = List.assoc "perf" values |> List.map (Option.get % Either.find_left) in
      let get_miss k= List.assoc k values |> List.map (Option.get % Either.find_right) in
      let l1_misses = get_miss "CACHE_MISS_L1"
      and l2_misses = get_miss "CACHE_MISS_L2" in
      let l12_volumes = List.map (fun (_, _, scheme) -> data_volumes scheme) schemes in
      let schemes_strings = List.map (fun (_, scheme_string, _) ->
          "\"" ^ String.replace_chars (function '\n' -> " " | c -> String.of_char c) scheme_string
          ^ "\"") schemes in
      let rec iter5 f l1 l2 l3 l4 l5 = match l1, l2, l3, l4, l5 with
        | h1::t1, h2::t2, h3::t3, h4::t4, h5::t5 ->
          f h1 h2 h3 h4 h5; iter5 f t1 t2 t3 t4 t5
        | [] ,[], [], [], [] -> ()
        | _ -> failwith "Iter5 : Unequal sized lists" in
      iter5 (fun perf l1_miss l2_miss (l1_vol, l2_vol) scheme ->
          Printf.fprintf f "%.1f,%d,%d,%d,%d,%s\n" perf l1_miss l2_miss l1_vol l2_vol scheme)
        perfs l1_misses l2_misses l12_volumes schemes_strings
    ) (bench_names csv_location)

let lambda_scheme = "[V f; U (2, f); ULambda y; T (32, c); Hoist_vars [c]; T (112, x); T (3, w);
  T (3, h); T (1, x);
  Lambda_apply y [((Iter 5), (Arg 8)); ((Iter 8), (Arg 9))]; T (1, c);
  T (1, x)]"

let lambda2 = "[V f; U (2, f); ULambda y; T (32, c); Hoist_vars [c]; T (112, x); T (3, w); T (3, h); T (1, x); Lambda_apply y [((Iter 5), (Arg 8)); ((Iter 8), (Arg 9))]; T (1, c); T (1, x)]"

let first_scheme =
  "[V f; U (2, f); U (8, y); T (32, c); Hoist_vars [c]; T (112, x); T (3, w); T (3, h); T (1, x); T (14, y); T (1, c); T (1, x)]"

let baba() = all_together "../random_search_avx2_XeonE52680/1/"

let flip() =
  let first_scheme_fp = parse_single_scheme 8 lambda_scheme
       |> Loop_levels.spec_fp_either in
(*   print_endline @@ Loop_levels.show_spec_fp_either first_scheme_fp ; *)
  let l1 = Loop_levels.data_volume_cache_level l1_size @@ Loop_levels.forget_spec first_scheme_fp in
  Printf.printf "%d\n" l1

let mean_stdv () =
  Parse_csv.parse_without_header "grid5k_ns.csv"
  |> List.map (fun l ->
      let li = List.map (Option.get % Either.find_right) l in
      let exp_len =  20 in
      assert (List.length li = exp_len);
      let sorted = List.sort Int.compare li in
      let sorted = List.drop 5 sorted |> List.take 10 in
      let mean = List.sum sorted / 10 in
      let square x = x * x in
      let std_dev =
        (List.fold_left (fun acc e -> acc +. float_of_int (square (e - mean))) 0. sorted)
        /. (float_of_int 10)
        |> Float.sqrt  in
      float_of_int mean /. 1000000., std_dev, 100. *. std_dev /. float_of_int mean )

let get_res () = Parse_csv.parse_csv_extended "grid5k.csv"
         |> List.concat_map (function
             | [Ident bench_name; Fl f1; Fl f2] ->
               [(bench_name ^ "_xlast", f1) ; (bench_name ^ "_flast", f2) ]
             | l -> print_endline @@ [%show: pline list] l; failwith "argh"
           )

let get_list_res () = Parse_csv.parse_csv_extended "grid5k.csv"
         |> List.map (function
             | [Ident bench_name; Fl f1; Fl f2] ->
               bench_name , f1,  f2
             | l -> print_endline @@ [%show: pline list] l; failwith "argh"
           )

let get_names ()= File.lines_of "conv_names" |> List.of_enum

let () = let lmean_std = mean_stdv ()
         |> List.ntake 2
         |> List.map
           (function [m1,_,x; m2,_,y] -> (m1, x, m2, y)
                   | _ -> assert false) in
  let names = get_names () in
       List.iter2 (fun (mean1, stddev1, mean2, stddev2) name ->
             Printf.printf "%s,%.2f,%.2f,%.2f,%.2f\n" name mean1 stddev1 mean2 stddev2
           ) lmean_std names

let bla () = let l_stdevs = mean_stdv ()
                        |> List.ntake 2
                        |> List.map
                          (function [_,_,x; _,_,y] -> (x, y)
                                  | l -> print_endline @@ [%show: (float * float * float) list] l; assert false)
  and l_bres = get_list_res () in
  print_endline "bench,perf_xlast,std_dev_xlast,perf_flast,std_dev_flast";
  List.iter2 (fun (name, p1, p2) (stddev1, stddev2) ->
      Printf.printf "%s,%.2f,%.2f,%.2f,%f\n" name p1 stddev1 p2 stddev2) l_bres l_stdevs

let scheme_yolo5 =
  "[V f; U (2, f); ULambda y; T (128, c); Hoist_vars [c]; T (1, x);  Lambda_apply y [((Iter 1), (Arg 8)); ((Iter 2), (Arg 13))]; T (2, f);  T (136, x); T (4, y)]"

let trac () =
  print_endline "Bloub";
  let first_scheme_fp = parse_single_scheme 16 scheme_yolo5
       |> Loop_levels.spec_fp_either in
(*   print_endline @@ Loop_levels.show_spec_fp_either first_scheme_fp ; *)
  let l1 = Loop_levels.data_volume_cache_level l1_size @@ Loop_levels.forget_spec first_scheme_fp in
  Printf.printf "L1 : %d\n" l1;
  let l2 = Loop_levels.data_volume_cache_level l2_size @@ Loop_levels.forget_spec first_scheme_fp in
  Printf.printf "L2 : %d\n" l2
