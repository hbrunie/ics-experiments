#!/usr/bin/env python3

## Perf against L1+L2 miss and volume with just one csv
##
##
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly
import plotly.express as px
cols = plotly.colors.DEFAULT_PLOTLY_COLORS


layout = go.Layout(yaxis={'tickformat':'.2e', 'rangemode': 'tozero',
           'ticks': 'outside'})



file2read = "./test-micro.csv"
df2 = pd.read_csv(file2read)
l1v = 1
l2v = 2
l1c = 0
l2c= 0
benchname_list = list(set(df2['benchname']))
bench_sizes = []

def genFigures(benchname_list):
    for benchname in benchname_list:
        df = df2.loc[df2['benchname'] == benchname]
        xlist = (l1c*df["L1miss"] + l2c*df["L2miss"]+ l1v*df["L1volume"]+l2v*df["L2volume"]) / df['computation']

        perfmicros = [max(x,y) for x,y in zip(df['perfmicro1'],df['perfmicro2'])]
        df2.loc[df2['benchname'] == benchname, 'perfmicros'] = perfmicros
        df = df2.loc[df2['benchname'] == benchname, ['perf','perfmicros','computation','loop1','L1volume','L2volume','L1miss','L2miss','scheme','lambda']].sort_values(by=['perfmicros'])
        xlist = (l1c*df["L1miss"] + l2c*df["L2miss"]+ l1v*df["L1volume"]+l2v*df["L2volume"])# / df['computation']
        if l1v > 0:
            title = f"({l1v}*L1volume+ {l2v}*L2volume) / Calc"
            s= "volume"
            l1 = l1v
            l2 = l2v
        else:
            l1 = l1c
            l2 = l2c
            s= "miss"
            title = f"({l1c}*L1miss+ {l2c}*L2miss)"
        title = f"{benchname}: Perf versus "+title
        fig = px.scatter(df,x=xlist, y='perf',  title=title, color='perfmicros',log_x=True,hover_data=['scheme'],symbol='lambda')

        fig.update_yaxes( tickformat='.2e')
        fig.update_layout(height=800, width=1600)
        
        fig.write_html(f'htmls/lmarker-perfmicroscolor/threshold-stride-PerfVsIA_{l1}-L1{s}_{l2}-L2{s}-{benchname}.html')#, auto_open=True)
        #fig.write_image(f'pngs/withCalc1/threshold-stride-PerfVsIA_{l1}-L1{s}_{l2}-L2{s}-{benchname}.png')#, auto_open=True)
        
genFigures(benchname_list)
