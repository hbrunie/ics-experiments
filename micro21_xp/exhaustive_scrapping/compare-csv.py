#!/usr/bin/env python3

## Perf against L1+L2 miss and volume with just one csv
##
##
import pandas as pd

file2read2cmp = "./test.csv"
df2 = pd.read_csv(file2read2cmp)

file2read_ref = "./ia-controlCoeff.csv"
df_ref = pd.read_csv(file2read_ref)
print("ref",file2read_ref)
print("Display absdiff")

for field in ['perf','L1volume','L2volume','L1miss','L2miss','computation']:
    diff = df_ref[field]-df2[field]
    print(field,sum([abs(x) for x in diff]))

file2read_ref = "./ia.csv"
print("ref",file2read_ref)
df_ref = pd.read_csv(file2read_ref)

for field in ['perf','L1volume','L2volume','L1miss','L2miss','computation']:
    diff = df_ref[field]-df2[field]
    print(field,sum([abs(x) for x in diff]))
