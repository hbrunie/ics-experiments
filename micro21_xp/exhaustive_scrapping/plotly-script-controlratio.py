#!/usr/bin/env python3

## Perf against L1+L2 miss and volume with just one csv
##
##
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly
import plotly.express as px
cols = plotly.colors.DEFAULT_PLOTLY_COLORS

layout = go.Layout(yaxis={'tickformat':'.2e', 'rangemode': 'tozero',
           'ticks': 'outside'})

file2read = "./test.csv"
df2 = pd.read_csv(file2read)
l1v = 1
l2v = 2
l1c = 0
l2c= 0
benchname_list = list(set(df2['benchname']))
bench_sizes = []

def genFigures(benchname_list):
    for benchname in benchname_list:
        df = df2.loc[df2['benchname'] == benchname]
        xlist = (l1c*df["L1miss"] + l2c*df["L2miss"]+ l1v*df["L1volume"]+l2v*df["L2volume"]) / df['computation']
        #sort list from best to worst IA
        xlist_sorted = sorted(xlist,reverse=False)
        border_ind = int(0.2 * len(xlist))
        threshold = xlist_sorted[border_ind]
        category = ['other' if x<threshold else 'best: 20% or 50 first' for x in xlist]
        df2.loc[df2['benchname'] == benchname, 'category'] = category
        df = df2.loc[df2['benchname'] == benchname, ['perf','computation','category','L1volume','L2volume','L1miss','L2miss']].sort_values(by=['category'])
        xlist = (l1c*df["L1miss"] + l2c*df["L2miss"]+ l1v*df["L1volume"]+l2v*df["L2volume"]) / df['computation']
        xlist = df["scheme"]
        ylist = df["abcoeff"]*df["loop1"]+(1.0-df["abcoeff"])*df["loop2"]
        if l1v > 0:
            title = f"({l1v}*L1volume+ {l2v}*L2volume) / Calc"
            s= "volume"
            l1 = l1v
            l2 = l2v
        else:
            l1 = l1c
            l2 = l2c
            s= "miss"
            title = f"({l1c}*L1miss+ {l2c}*L2miss)"
        title = f"{benchname}: Perf versus "+title
        fig = px.scatter(df,x=xlist, y=ylist,  title=title)

        fig.update_yaxes( tickformat='.2e')
        fig.update_layout(height=1600, width=1600)
        
        fig.write_image(f'pngs/controlratio/threshold-stride-PerfVsIA_{l1}-L1{s}_{l2}-L2{s}-{benchname}.png')#, auto_open=True)
        #fig.write_image(f'pngs/withCalc1/threshold-stride-PerfVsIA_{l1}-L1{s}_{l2}-L2{s}-{benchname}.png')#, auto_open=True)
        
genFigures(benchname_list)
