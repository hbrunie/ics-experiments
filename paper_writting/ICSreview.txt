Dear authors,

The International Conference on Supercomputing (ICS'21) program
committee is sorry to inform you that your submission #151 was rejected,
and will not appear in the conference.

       Title: TTile: Tensor Tiling Beyond Perfect
     Authors: Nicolas Tollenaere (Inria)
              Auguste Olivry (Inria)
              Guillaume Iooss (Inria)
              Hugo Brunie (Inria)
              Albert Cohen (Google)
              P. Sadayappan (University of Utah)
              Fabrice Rastello (Inria)
        Site: https://ics21.hotcrp.com/paper/151?cap=0151a7RCgdSJRRd4

39 papers were accepted out of 157 submissions.

Visit the submission site for reviews, comments, and related
information. Reviews and comments are also included below.

Please consult the *meta-review* added as a comment or within one of
the reviews for feedback from  the online discussion and/or PC
meeting.

Contact mueller@cs.ncsu.edu and yetsion@technion.ac.il with any
questions or concerns.

- ICS'21 Submissions

Review #151A
===========================================================================

Overall merit
-------------
2. Weak reject

Reviewer expertise
------------------
2. Some familiarity

Novelty
-------
3. New contribution

Paper summary
-------------
In this paper, the authors develop a new approach to optimising convolution kernels for convolutional neural networks by searching over loop permutations, tile sizes and choosing between different micro-kernels. The goal is to develop a method to search for highly efficient implementations in a much faster manner than traditional auto-tuning approaches. For this, the authors semi-formally introduce optimisation schemes, which are lists of specifiers that identify the loops that exist in the final optimised implementation. Each specifier is parameterized to express sizes, such as tile sizes. The authors then describe how they build a set of micro-kernels by benchmarking them independently and choosing those which perform best. They search for permutations of the loops outside of the micro-kernels that minimize data-movement between cache-levels and generate a search space over possible tile sizes based on predefined constraints. An experimentally determined metric is used to choose the most promising optimisation schemes, generate code and choose the best performing implementation. In the end this method is used to generate optimised code and compare the performance to a state-of-the-art implementation for 32 different benchmarks.

Strengths
---------
- Benchmarks show highly performant code that is found in a comparably short amount of time.

- The approach enables using multiple micro-kernels instead of one, thus offering more flexibility.

Weaknesses
----------
- One important step, determining loop permutations for loops outside of the micro-kernel is described in another paper which is currently being reviewed.

- In the beginning, it is not clear what exactly the paper tries to achieve. There is a lot of talk about tiling and loop permutations. However, I would say that finding (combinations of) micro-kernels is the real focus of the paper.

- There are quite a lot of spelling and grammatical mistakes in the paper. This makes it more difficult to read.

Comments for author
-------------------
- In the beginning it seems to me like the main focus of the paper lies in finding loop permutations and tile sizes for convolutions. However, later in the paper, it is explained that loop permutations outside of micro-kernels are found by a tool not described in the paper and that there are exactly four predetermined optimization schemes for micro-kernels between which to choose. It would help a lot in understanding the introduction and motivation of the paper if this were made clear from the beginning.
- In section 4.2 you describe how loop permutations are determined for single micro-kernels. It is not clear to me if and how this is done for combinations of micro-kernels. Also, how are the loop permutations chosen for combinations of kernels?
- A very important optimization in terms of search space reduction and therefore search time, seems to be the “pruning of the optimization space” described in Section 4.4. This optimization is based on the exhaustive exploration of the optimisation space for only a single benchmark. Are these single benchmark results comparable to the other benchmarks? Can some of the benchmark results in Figure 9, where the performance for TTile is significantly worse in comparison to oneDNN be attributed to throwing away good kernels based on this metric? It could be helpful to add further explanations to the performance results in Section 6.
- Figures 3 and 6 are mentioned only once and not explained any further. I don’t see what they are demonstrating exactly.

- In line 635, what is meant by: “each element d in the loop permutation”. Does “element” refer to a loop specifier?
- In general, I feel like this paper is not quite ready yet.


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


Review #151B
===========================================================================

Overall merit
-------------
2. Weak reject

Reviewer expertise
------------------
1. No familiarity

Novelty
-------
2. Incremental improvement

Paper summary
-------------
This paper proposes a method determining the tile configuration (tile sizes + tiling loops) which is a critical aspect of data locality in tensor computations.

Strengths
---------
- The authors are targeting a timely problem in this paper.

Weaknesses
----------
- The paper lacks clarity and is not an easy read.
-  Results do not provide in-debt  insights such as why in some cases TTile is not as performant as that in the rest of the cases.


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


Review #151C
===========================================================================

Overall merit
-------------
2. Weak reject

Reviewer expertise
------------------
2. Some familiarity

Novelty
-------
3. New contribution

Paper summary
-------------
 - Objective of this work is to optimize tensor computations that achieve both user productivity and performance.
 - The authors' approach is to find the best tiling loops and the best tile size by exploring all the possible permutation and generate optimized code via TTILE.
 - The evaluation results show that TTILE is comparable performance to OneDNN in single-threaded executions of 30 CNN benchmarks.

Strengths
---------
 - The paper is well-written and easy to follow the story.
 - The authors shows comprehensive evaluations results w.t.r. problem sizes

Weaknesses
----------
 - The authors target only Intel i9-7940X
 - TTILE only support a single-thread version as of now
 - There is no comparison with MOpt (Modeling-based Optimizer) proposed by Li et al.
 - There is no discussion on productivity

Comments for author
-------------------
The authors propose TTILE to optimize tensor computations that maximize its performance while maintaining certain productivity.
TTILE explore all the possible tiling loops and tile sizes to find the best configuration, and then generate the optimized code.
The evaluations show that performance of TTILE is comparable to OneDNN in single-threaded executions of 30 CNN benchmarks.

The paper is well-written and easy to follow.
Tensor computations are core kernels for CNN. The authors work on an important research topic.
In the evaluations, the authors evaluates TTILE by comparing to OneDNN with three CNN under many different problem sizes.
It is comprehensive w.r.t variety of problem sizes.

Although the authors evaluate TTILE with many different problem sizes, the authors target only Intel i9-7940X.
It is not clear TTILE gives the same results in different Intel architectures.
It is recommended to evaluate TTILE on other Intel architectures which has, e.g., different cache sizes.

Although TTILIE show comparable performance to OneDNN, TTILE is not compared with MOpt proposed by Li et al.
MOpt outperforms OneDNN in their paper. It is not clear that TTILI outperforms MOpt in the same evaluations in this paper.

Other comments
 - Since the authors only target a single-threaded execution while OneDNN can make use of Intel multi-core processors, significance of this work is limited accordingly
 - Unfortunately, TTILE is motivated by high productivity. However, there is not quantitative discussion on the productivity compared to existing libraries other than Figure 1.
 - a index --> an index


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


Review #151D
===========================================================================

Overall merit
-------------
3. Weak accept

Reviewer expertise
------------------
2. Some familiarity

Novelty
-------
3. New contribution

Paper summary
-------------
This paper present a systematic approach to find the tile configurations for
tensor code. The main components in this approach include (1) selecting micro-kernels,
(2) deciding the loop permutation above the micro-kernel level, (3) pruning the search space,
and (4) code generation. The evaluation shows that the technique is able to
compete with a vendor library, oneDNN.

Strengths
---------
1. The problem is interesting and timely.
2. The results are strong.

Weaknesses
----------
1. An important piece in the proposed techniques is not in this paper, making it hard to judge the novelty.

2. It is not clear where the described techniques are implemented.

Comments for author
-------------------
This paper present a systematic approach to find the tile configurations for
tensor code. The main components in this approach include (1) selecting micro-kernels,
(2) deciding the loop permutation above the micro-kernel level (expressing
data movement volume at different cache levels with analytical expressions of
loop extent and cache size, using a non-linear problem optimizer to find tile loop
sizes; detail given in another paper), (3) pruning the search space, and
(4) code generation. The evaluation shows that the technique is able to
compete with a vendor library, oneDNN.

The techniques traditionally fall in the general category of loop optimizations. The
proposed techniques customize for tensor computation loops. One of the key novelty
in the proposed method is allowing multiple micro-kernels to be used simultaneously
to cover more cases. This aspect, however, is not discussed in detail in this paper
(citing another paper under review) and not empirically investigated. As such,
it is hard to appreciate the technical contribution besides seeing a system that works
well.

It is not clear whether Section 4.4 only applies to the example in 4.3 or
is a scheme that works in general? Is only considering L1 cache sufficient?

In Section 5.3, "We consider the SSA graph of computation described inSection 3.1"
What SSA graph is referred to here.

It is not clear where the proposed techniques are implemented.

The evaluation section can be improved by having more discussions. In particular,
although TTILE out-performs oneDNN in 17 out of 32 cases, some of the cases oneDNN
performs better by a large margin. It would be interesting to see the reason of
that.

Comments
===========================================================================

Rebuttal Response by Guillaume Iooss <guillaume.iooss@inria.fr>
---------------------------------------------------------------------------
First, we would like to thank the reviewers for their feedback. We will first address the general concerns, then the specific points raised by each of them.

*** General comments ***

* Reviewer A notes that although introduction focuses on loop permutation, the real contribution described in the paper is actually the micro-kernels exploration.
We agree that our introduction is unclear and that we should have put more emphasis on micro-kernels from the beginning.
However, one key contribution is to provide an end-to-end process that provides an efficient implementation of convolution without any user intervention. As such, our contribution lies more in the integration of this permutation-finding tool in a more extensive toolchain than in the permutation generation itself.

* Speaking of which, reviewer C asks for a more thorough discussion on productivity. We stress that our tool provides an fully automated process that does not need any user intervention. This contrasts with MOpt which only provides a loop permutation, then manual implementation is needed in order to get a concrete implementation. While TVM also provides an automated workflow, their search is significantly longer than ours.

* Reviewer C points out that lack of multicore limits the significance of the contribution, and that we should have tried and discussed more than one architecture. We agree with these points and we are actively working on making up for them.

* Reviewer D regrets that we did not try to get some insights of why our implementation does perform well, or why there is a such a huge gap in cases where OneDNN beat us. We also agree on this and we will try to address these questions.

We will also fix all remarks concerning typos and grammatical mistakes.



*** Specific Questions ***

** Reviewer A :

> You describe how loop permutations are determined for single micro-kernels. It is not clear to me if and how this is done for combinations of micro-kernels. Also, how are the loop permutations chosen for combinations of kernels ?

We only generate combine micro-kernels that are "close" to each other - that is, that only differs along one dimension. As such, they have quite similar footprints and we assume that we can use the exact same permutation for both.

> Are these single benchmark results comparable to the other benchmarks?

We did not perform exhaustive exploration for the other benchmarks so we cannot really tell whether they are really different.
One difference we could imagine is that the channel dimension is sometimes much bigger, which means reduction size is much bigger too.

> Can some of the benchmark results in Figure 9, where the performance for TTile is significantly worse in comparison to oneDNN be attributed to throwing away good kernels based on this metric?

This is a possibility, this would mean that our metric is biased. Another possibility is that some layout reshaping is needed for this problem.

> What is meant by: “each element d in the loop permutation”. Does “element” refer to a loop specifier ?

In this case, we refer to a location in our permutation, so a given level in the loop nest that has been associated with a dimension and a divisor of the size of this dimension. This element is translated into a specifier in a straightforward way.

** Reviewer C :

> Although TTILE show comparable performance to OneDNN, TTILE is not compared with MOpt proposed by Li et al. MOpt outperforms OneDNN in their paper. It is not clear that TTILI outperforms MOpt in the same evaluations in this paper.

MOpt paper was still not published at the time of our submission. Nevertheless, we are aware of this work which is actually similar to the permutation-searching tool that we mention in section 4.2. In contrast to our work, MOpt provides only a loop permutation.  Manual implementation is then needed in order to get a concrete implementation.


** Reviewer D :

> It is not clear where the described techniques are implemented.

We do not understand this question, but the full pipeline (microkernels generation and evaluation, permutation search, scheme building and code generation) is implemented and automated. We will make it publicly available as soon as possible.

> It is not clear whether Section 4.4 only applies to the example in 4.3 or is a scheme that works in general? Is only considering L1 cache sufficient?

We agree that our paper does not provide much insight on why it does perform well. What we wanted to highlight is that a very simple metric can yield peak results in most cases. We are currently trying to use hardware counters to build a better understanding of these questions.

> In Section 5.3, "We consider the SSA graph of computation described in Section 3.1" What SSA graph is referred to here.

This refers the SSA graph describing the semantic specification of the kernel.

