
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

name = "motokoData"
size =16

### AVX512
#file2read = f"data_microkern_AVX512_nohw.txt"
#titlefig = "Micro-kernel in isolation - SkylakeX - AVX512 - no H/W"

#file2read = f"data_microkern_AVX512_h_now.txt"
#titlefig = "Micro-kernel in isolation - SkylakeX - AVX512 - H=3 - no W"

#file2read = f"data_microkern_AVX512_w_noh.txt"
#titlefig = "Micro-kernel in isolation - SkylakeX - AVX512 - W=3 - no H"

file2read = f"data_microkern_AVX512_h_w.txt"
titlefig = "Micro-kernel in isolation - SkylakeX - AVX512 - H=W=3"


### AVX2
#file2read = f"data_microkern_AVX2_nohw.txt"
#titlefig = "Micro-kernel in isolation - SkylakeX - AVX2 - no H/W"

#file2read = f"data_microkern_AVX2_h_now.txt"
#titlefig = "Micro-kernel in isolation - SkylakeX - AVX2 - H=3 - no W"

#file2read = f"data_microkern_AVX2_w_noh.txt"
#titlefig = "Micro-kernel in isolation - SkylakeX - AVX2 - W=3 - no H"

#file2read = f"data_microkern_AVX2_h_w.txt"
#titlefig = "Micro-kernel in isolation - SkylakeX - AVX2 - H=W=3"



# Extract the data from the csv
df = pd.read_csv(file2read)
F = [int(x) for x in list(df['f'])]
Y = [int(y) for y in list(df['y'])]
perf = list(df["perf"])


data=[]
datasquared=[]
for x in range(size-1):
    data.append([0 for x in range(size-1)])
    datasquared.append([0 for x in range(size-1)])
for f,y,p in zip(F,Y,perf):
    #print(y,f,p)
    data[y-1][f-1] = round(p, 1)
    datasquared[y-1][f-1] = data[y-1][f-1]**2


# Show (https://matplotlib.org/3.3.1/gallery/images_contours_and_fields/image_annotated_heatmap.html?highlight=heatmap )
fig, ax = plt.subplots()
im = ax.imshow(datasquared)

# We want to show all ticks...
ax.set_xticks(np.arange(size-1))
ax.set_yticks(np.arange(size-1))

ax.set_xlabel("F")
ax.set_ylabel("Y")

# ... and label them with the respective list entries
ax.set_xticklabels(range(1,size))
ax.set_yticklabels(range(1,size))

# Rotate the tick labels and set their alignment.
plt.setp(ax.get_xticklabels(), ha="center",
         rotation_mode="anchor")


# Loop over data dimensions and create text annotations.
for y in range(0,size-1):
    for f in range(0,size-1):
        if (y+f<size-1):
            text = ax.text(f, y, "{}".format(data[y][f]), ha="center", va="center", color="w")

fig.subplots_adjust(left=0.045, bottom=0.05, right=0.5, top=0.96, wspace=0.2, hspace=0.2)

#ax.set_title(titlefig)
fig.tight_layout()
plt.show()
#plt.savefig("test.svg", format="svg")

