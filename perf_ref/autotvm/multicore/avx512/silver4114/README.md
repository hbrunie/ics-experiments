Bench for Multi core with AVX512
=====

Files
-----
```
launch_execution.py                    Launch conv_NCHW.py with all inputs

conv_NCHW.py                           Launch AutoTVM and conv_NCHW_best.py in order to have the execution time

conv_NCHW_best.py                      Take one input and measure the time to execute with the best configuration in log/
```

Run
----
```sh
python3 launch_execution.py
```


If you want to run for some inputs you have to change the line 16 of launch_execution.py.

For example you can do:

```py
for key in ["MobilNet_1", "MobilNet_2"]:
```

Bench
----

I use AutoTVM for all configurations. Then I measure 20 times the execution time with conv_NCHW_best.py. 
Then I delete the 5 best times and the 5 worst times and I compute the mean and std of the 10 others. The result are in result/result.csv

I mesure the times with conv_NCHW_best.py because I observe some variation of time which is not visible by increasing the number of measure with time_evaluator. This method therefore makes it possible to recompile each time and thus be sure to have a good time.

Note that the number of configuration is write at the line 75 of the file conv_NCHW.py, len(task.config_space) means all espace but for conv2d is not very huge.


Please install:

```sh
pip3 install --user numpy decorator attrs tornado psutil xgboost cloudpickle pytest
```

For Grid5000
----

## Connect to grid5000

```sh

ssh <LOGIN>@access.grid5000.fr
ssh grenoble
oarsub -p "cluster='dahu'" -l host=1,walltime=<NB_HEURE> -I -t deploy
kadeploy3 -f $OAR_NODE_FILE -e debian10-x64-base -k
ssh root@dahu-<ID>
```
\<NB_HEURE\> is 4 for 4 hours or 4:30 for 4 hours and half.

Please note that if you want to add time you need to do:

```sh
oarwalltime <JOBID> +<NB_HOURS>
```

You can find your JOBID on https://intranet.grid5000.fr/oar/Grenoble/drawgantt-svg/

## Disable Turbo-boost
```sh
echo 1 | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo
```

## Install Dependency

```sh
apt install git
apt-get install -y python3 python3-dev python3-setuptools gcc libtinfo-dev zlib1g-dev build-essential cmake libedit-dev libxml2-dev
apt install lsb-release wget software-properties-common
bash -c "$(wget -O - https://apt.llvm.org/llvm.sh)"
apt install clang
apt install python3-pip
echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo
git clone --recursive git@github.com:S12P/tvm_ttile.git
export TVM_HOME=/root/tvm_ttile
export PYTHONPATH=$TVM_HOME/python:${PYTHONPATH}
pip3 install --user numpy decorator attrs tornado psutil xgboost cloudpickle pytest
```

Please note that, if you forgot to clone with --recursive you need to do:

```sh
git submodule init
git submodule update
```

## Install TVM

```
git checkout bench_multi_core
mkdir build
cp cmake/config.cmake build/
cd build/
cmake ..
make -j64
cd ../ttile/bench/bench_multi_core_avx512
```

Please be sure to export TVM_HOME and PYTHONPATH:

```
export TVM_HOME=/root/tvm_ttile
export PYTHONPATH=$TVM_HOME/python:${PYTHONPATH}
```
