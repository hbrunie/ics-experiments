#!/usr/bin/env python3

import argcomplete, argparse
import pandas as pd

parser = argparse.ArgumentParser(description='Generate 1 csv from one per bench. 300sec on my laptop (Hugo).')
parser.add_argument('csvfile', help="csv file to convert")
parser.add_argument('newcsvfile', help="csv file to dump new format good for weka study")
parser.add_argument('nbL1L2', help="Number of OI we want to compute (number of couple L1L2 used to generate input csv)", type=int)
argcomplete.autocomplete(parser)
args = parser.parse_args()
df0 = pd.read_csv(args.csvfile)

tmplist = []
for i in range(args.nbL1L2):
    tmplist += [f'L1size_{i}',f'L2size_{i}',f'L1vol_{i}',f'L2vol_{i}']

df = df0[['benchname','perf','computation','lambda','loop1','loop2','L1miss','L2miss']+tmplist]
for i in range(args.nbL1L2):
    df.insert(1,f'OI_{i}',[ x/(2*y+z) for x,y,z in zip(df[f'computation'].values, df[f'L1vol_{i}'].values, df[f'L2vol_{i}'].values)])

df.to_csv(args.newcsvfile,index=False)
