#!/usr/bin/env python3

## Perf against L1+L2 miss and volume with just one csv
##
##
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly
import plotly.express as px
import math
import numpy as np
import argcomplete, argparse
import os

# If autocompletion does not work:
#  activate-global-python-argcomplete --user
#  eval "$(register-python-argcomplete ./plotly-script.py)"


parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('--metriccolor', type=str, help='metric for color', choices=('lambdatop','incfilter', 'crossfilter', 'OI', 'OIbest', 'controlloop', 'controlkernel', 'controlbest', 'nolambda', 'lambdabest', 'L1volume', 'L2volume', 'L1miss' , 'L2miss', 'kernelperf', 'kernelroofline'), default='incfilter')
parser.add_argument('--outputformat', type=str, help='select one or more output formats', choices=('svg','html','png'), action='append')
parser.add_argument('--subdir', type=str, help='path for output default is ./')
parser.add_argument('--metricxaxis', type=str, help='metric for x axis', choices=('OI', 'controlloop', 'controlkernel', 'L1volume', 'L2volume', 'L1miss', 'L2miss', 'kernelperf'), default='OI')
parser.add_argument('--kernelcontrol', action='store_true')
parser.add_argument('--csv', type=str, default='./fulldata.csv')
parser.add_argument('--bandwidth', type=float, default=2, help='L1 bandwidth assuming L2 is 1')

argcomplete.autocomplete(parser)
args = parser.parse_args()

bandwidth={'L1':args.bandwidth, 'L2':1}

xaxis=args.metricxaxis 

outputformat=args.outputformat
subdir=args.subdir
if not outputformat and subdir:
    for f in ['png', 'svg', 'html']:
        if subdir.find(f)!=-1: outputformat=[f]
if not outputformat: outputformat=['svg']
if not subdir:
    for f in outputformat:
        if not os.path.exists(f'{f}s'): os.makedirs(f'{f}s')
elif not os.path.exists(subdir): os.makedirs(subdir)

def filename(outformat, name):
    if subdir: return f'{subdir}/{name}.{outformat}'
    else: return f'{outformat}s/{name}.{outformat}'
        
if args.kernelcontrol: control='microkernel'
else: control='loop1'
color=args.metriccolor

file2read = args.csv
df2 = pd.read_csv(file2read)


layout = go.Layout(yaxis={'tickformat':'.2e', 'rangemode': 'tozero',
           'ticks': 'outside'})

print('Fields: ',list(df2.columns), sep='')
def genFigures(benchname_list):
    def best(df, filters, verbose=True):
        tmpdf=df.assign(tmp=True)
        if verbose: print(f'filter {filters}',end='\t')
        for f in filters:
            field=f['field'];
            length=tmpdf.tmp.sum()
            if 'num' not in f and 'ratio' in f: num=int(length*f['ratio'])
            elif 'num' in f and 'ratio' not in f: num=f['num']
            elif 'num' in f and 'ratio' in f: num=max( f['num'], int(length*f['ratio']))
            else: num=int(length*0.2)
            threshold=int(min(tmpdf.nlargest(num, ['tmp', field])[field]))
            tmpdf.tmp=tmpdf.tmp & (tmpdf[field]>=threshold)
            if verbose: print('num:{}/{}'.format(tmpdf.tmp.sum(), len(df[field])),f'threshold:{threshold}', end='\t')
        return tmpdf.tmp
    perfref = [] 
    sizeref = []
    perfmetric = []
    sizemetric = []
    for benchname in benchname_list:
        for f in outputformat: print('\t', filename(f, f'threshold-{benchname}'), end=' ')

        df = df2.loc[df2['benchname'] == benchname].copy()
        perfref.append(max(list(df.perf)))
        sizeref.append(len(list(df.perf)))
        df['OI'] = df['computation']/ (df["L1volume"]/bandwidth['L1']+df["L2volume"]/bandwidth['L2'])
        if color in ['OIbest', 'crossfilter']:
            df['OIbest'] = best(df, [{'field':'OI', 'ratio':0.6, 'num':400}])


        df['abcoeff2']=df.abcoeff*df.acore/(df.abcoeff*df.acore+(1-df.abcoeff)*df.bcore)
        df['kernelperf']=df.perfmicro1*df.abcoeff2+df.perfmicro2*(1-df.abcoeff2)
        if max(df.kernelperf>100): print(df[df.abcoeff>1]['scheme'])
        df['kernelroofline']=df['kernelperf']<df['perf']
                                    
        
        df['controlloop']=df.loop1
        df['controlkernel']=df['control']=df.loop1*(df.acore*df.abcoeff+df.bcore*(1-df.abcoeff))
        if control=='loop1': df['control']=df.controlloop
        elif control=='microkernel': df['control']=df.controlkernel
        if color in ['controlbest','crossfilter']:
            df['controlbest']= best(df, [{'field':'control', 'ratio':0.6, 'num': 200}])

        df['nolambda']=df['lambda']==False
        if color in ['lambdabest','crossfilter']:
            df['lambdabest']=best(df, [{'field':'nolambda', 'num':500}])

        if color=='crossfilter':
            df[color]=df.nolambda*1+df.controlbest*1+df.OIbest*1

        if color=='incfilter':
            df[color]=best(df,  [{'field':'nolambda', 'num':1000}, {'field':'control', 'ratio':0.6, 'num': 200}, {'field':'OI', 'ratio': 0.6, 'num':200}])
        
        perfmetric.append(max(df.loc[df[color] == True].perf))
        sizemetric.append(len(list(df.loc[df[color] == True].perf)))
        df['L2ratio']=df['L2miss']/df['L2volume']

        #df=df.sort_values(ascending=False,by=[color])
        #title = f"{benchname}"
        #fig = px.scatter(df,x=xaxis, y='perf',  title=title, color=color, log_x=True, log_y=False, hover_data=['scheme'])

        #fig.update_yaxes( tickformat='.2e')
        #fig.update_layout(height=800, width=1200)
        #
        #if 'png' in outputformat:
        #    fig.write_image(filename('png', f'threshold-{benchname}'))
        #if 'svg' in outputformat:
        #    fig.write_image(filename('svg', f'threshold-{benchname}'))
        #if 'html' in outputformat:
        #    fig.write_html(filename('html', f'threshold-{benchname}'))


        print()
    return perfref,perfmetric,sizeref,sizemetric

benchs=list(set(df2['benchname']))
benchs.sort()
perfref,perfmetric,sizeref,sizemetric = genFigures(benchs)
print("Perf ref")
print(perfref)
print("Perf metric")
print(perfmetric)
print("Ratio")
print(["{:.2f}".format(x/y) for x,y in zip(sizemetric,sizeref)])
print("Size ref")
print(sizeref)
print("Size metric")
print(sizemetric)
print("Ratio")
print(["{:.2f}".format(x/y) for x,y in zip(sizemetric,sizeref)])
