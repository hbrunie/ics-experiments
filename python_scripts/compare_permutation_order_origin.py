#!/usr/bin/env python3
import pandas as pd
import numpy as np
import argcomplete, argparse
from reformat import merge_name

parser = argparse.ArgumentParser(description='Compare different performance obtained with permutations from IOOPT, IOOPT buggy and Default permutation.')
parser.add_argument('rootdir', help="Precise absolute path for root dir of ics-experiments.")
parser.add_argument('--debug', help="More verbose", action='store_true', default=False)
argcomplete.autocomplete(parser)
args = parser.parse_args()
debug=args.debug

datadir = args.rootdir + "/perf_schemes/avx512/INTELi9-7940X_motoko/"

buggydir = datadir + "volume_metric_buggy_ioopt/"
defaultdir = datadir + "volume_metric_default_perm/"
iooptdir = datadir + "volume_metric_ioopt/100_cands/"

namel=["skx_mobilNet_1.csv","skx_mobilNet_5.csv","skx_mobilNet_9.csv","skx_resNet18_1.csv","skx_resNet18_5.csv","skx_resNet18_9.csv","skx_yolo9000_18.csv","skx_yolo9000_5.csv","skx_mobilNet_2.csv","skx_mobilNet_6.csv","skx_resNet18_10.csv","skx_resNet18_2.csv","skx_resNet18_6.csv","skx_yolo9000_0.csv","skx_yolo9000_19.csv","skx_yolo9000_8.csv","skx_mobilNet_3.csv","skx_mobilNet_7.csv","skx_resNet18_11.csv","skx_resNet18_3.csv","skx_resNet18_7.csv","skx_yolo9000_12.csv","skx_yolo9000_2.csv","skx_yolo9000_9.csv","skx_mobilNet_4.csv","skx_mobilNet_8.csv","skx_resNet18_12.csv","skx_resNet18_4.csv","skx_resNet18_8.csv","skx_yolo9000_13.csv","skx_yolo9000_4.csv"]##skx_yolo9000_23.csv does not exist

dfioopt = merge_name(iooptdir,namel,sep=",",debug=debug)
dfbuggy = merge_name(buggydir,namel,sep=",",debug=debug)
dfdefault = merge_name(defaultdir,namel,sep=",",debug=debug)

dfioopt['version'] = "ioopt"
dfdefault['version'] = "default"
dfbuggy['version'] = "buggy"

dfmerge = dfioopt.append(dfdefault).append(dfbuggy)

lcomp = []
liom = []
ldm = []
lbm = []
for bench in list(set(dfmerge.benchname.values)):
    m = max(dfmerge.loc[dfmerge.benchname == bench,'perf'].values)
    iom = max(dfioopt.loc[dfioopt.benchname == bench,'perf'].values)
    liom.append(iom/m)
    dm = max(dfdefault.loc[dfdefault.benchname == bench,'perf'].values)
    ldm.append(dm/m)
    bm = max(dfbuggy.loc[dfbuggy.benchname == bench,'perf'].values)
    lbm.append(bm/m)
    lcomp.append((bench,m,"{:.2f}".format(iom/m),"{:.2f}".format(dm/m),"{:.2f}".format(bm/m)))

def stat(l):
    return ("{:.2f}".format(np.mean(l)),"{:.2f}".format(np.median(l)),"{:.2f}".format(np.std(l)),"{:.2f}".format(min(l)),"{:.2f}".format(max(l)))

print("mean,median,std,min,max")
print("Ioopt")
print(stat(liom))
print("Default")
print(stat(ldm))
print("Buggy")
print(stat(lbm))
