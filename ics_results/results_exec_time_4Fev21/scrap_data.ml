open Batteries


type batch = { perf : float;
               l1_min : float; l1_max : float;
               l2_min : float; l2_max : float;
               l3_min : float; l3_max : float;
               c_red_size : int;
             } [@@deriving show {with_path = false}]

let to_csv f {perf ;
               l1_min ; l1_max;
               l2_min ; l2_max ;
               l3_min ; l3_max ;
            c_red_size ;} =
  Printf.fprintf f "%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%d\n"
    perf l1_min l1_max l2_min l2_max l3_min l3_max c_red_size

let conv_name = Re.compile (Re.(seq [str "Res for "; group (rep wordc)]))

let fuse l =
  let rec loop acc current l = match current, l with
    | Some d, (`Text s) :: tail -> loop ((d,s)::acc) None tail
    | None,   (`Delim d) :: tail -> loop acc (Some d) tail
    | None, (`Text _) :: tail -> loop acc None tail
    | _, [] -> List.rev acc
    | _ -> failwith "Argh"
  in loop [] None l

let float_re = Re.(seq [rep1 digit; char '.'; rep1 digit;])
let perf_re = Re.(seq [str "perf "; group float_re; rep space;])
let firstlines_match = Re.(seq [perf_re; rep any; str "tile"; non_greedy (rep any);
                                str "T ("; group (rep1 digit); str ", c)"; rep any;
                                str "Cache occupation :";])

let lline lname = Re.(seq [str lname;
                           rep any; char '['; group float_re; rep space;
                           str "|"; rep space; group float_re; char ']'; rep any;])

let last_lines = Re.(seq [lline "l1"; rep any; lline "l2"; rep any; lline "l3";])

let all = Re.(compile @@ non_greedy @@ seq [firstlines_match; rep any; last_lines])

let to_batch s = 
  Re.all all s
  |> List.map (fun g ->
      let get gpos = float_of_string @@ Re.Group.get g gpos  in
      let get_int gpos = int_of_string @@ Re.Group.get g gpos  in
      let perf = get 1 in
      let c_red_size = get_int 2 in
      let l1_min = get 3 in
      let l1_max = get 4 in
      let l2_min = get 5 in
      let l2_max = get 6 in
      let l3_min = get 7 in
      let l3_max = get 8 in
      {perf; l1_min; l2_min; c_red_size; l3_min; l1_max; l2_max; l3_max;}
    )

let scrap file = File.with_file_in file
                   @@ fun f -> IO.read_all f
(*                              |> Re.matches all *)
                             |> Re.split_full conv_name
                             |> fuse
                             |> List.map ( fun (d, s) ->
                                 let name = Re.Group.get d 1 in
                               name, to_batch s)

let () = scrap "retry_mbnet_resnet_50.txt" |>
             List.iter (fun (name, batches) ->
             File.with_file_out (name ^ "_mb_resnet.csv") @@ fun f ->
             Printf.fprintf f "perf, l1_min, l1_max, l2_min, l2_max, l3_min, l3_max, c_red_size\n";
             List.iter (to_csv f) batches)
